//
//  PSPuzzleViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/9/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSPuzzleView.h"

@class PFObject, PFQuery;
@protocol PSPuzzleViewControllerDelegate

@optional
/**
 This method is called when the user wants to solve the next puzzle. Return nil
 if there is none left.
 */
- (PFObject *)nextPuzzle;

/**
 This method is called when the user wants to solve the next puzzle, but it has
 to be loaded from the server. This executes the query and handles the rest.
 */
- (PFQuery *)nextQuery;

@end

/**
 This struct is useful to tell the view controller what to do after user solves
 a puzzle.
 */
typedef enum : NSUInteger {
    PuzzleSourceFriend = 0,
    PuzzleSourceFeatured,
    PuzzleSourceSolved,
    PuzzleSourceAdditional
} PuzzleSource;

@class PSPuzzleView;
@interface PSPuzzleViewController : UIViewController
<PSPuzzleViewDelegate>

/// This determines where to send completion data after user solves the puzzle.
@property (nonatomic) PuzzleSource puzzleSource;

/// Puzzle object, assuming to have certain properties
@property (strong, nonatomic) PFObject *puzzle;

@property (weak, nonatomic) IBOutlet id <PSPuzzleViewControllerDelegate> delegate;

/// Loads the puzzle from the query
- (void)setQuery:(PFQuery *)query;

/// Sets the title
- (void)setTitle:(NSString *)title;

/// Reply button pressed
- (IBAction)reply:(id)sender;

@end
