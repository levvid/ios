//
//  CameraViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 2/14/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "CameraViewController.h"
#import "PSPuzzleView.h"
#import "PSContactViewController.h"
#import <Parse/Parse.h>
#import "AVCamPreviewView.h"
#import "PSFocusView.h"
#import "PSScrollViewController.h"

static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface CameraViewController ()
<PSScrollViewControllerProtocol>
{
    // Back button navigation bar
    UIBarButtonItem *galleryBarButton;
    UIBarButtonItem *timerBarButton;
    UIButton *timerBarButtonTitle;
    UIBarButtonItem *backBarButton;
    UILabel *titleView;
    
    // Preview view
    IBOutlet AVCamPreviewView *previewView;
    
    // Button to take picture
    IBOutlet UIButton *captureButton;
    
    // Button to change camera
    IBOutlet UIButton *changeCameraButton;
    
    // Flash button - hide when unavailable
    IBOutlet UIButton *flashButton;
    
    // Mask - for the time selection
    IBOutlet UIButton *timerBackgroundMask;
    
    // Focus control
    PSFocusView *focusView;
    
    // Current flash mode
    AVCaptureFlashMode flashMode;
    
    // Source of picture
    BOOL picFromCam;
}

// Session management.
// Communicate with the session and other session objects on this queue.
@property (nonatomic) dispatch_queue_t sessionQueue;
@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

// Utilities.
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) id runtimeErrorHandlingObserver;

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer;

@end

@implementation CameraViewController
@synthesize sessionQueue, session, videoDeviceInput, stillImageOutput,
backgroundRecordingID, deviceAuthorized, sessionRunningAndDeviceAuthorized,
runtimeErrorHandlingObserver, recipients;

- (BOOL)isSessionRunningAndDeviceAuthorized {
    return [[self session] isRunning] && [self isDeviceAuthorized];
}

+ (NSSet *)keyPathsForValuesAffectingSessionRunningAndDeviceAuthorized
{
    return [NSSet setWithObjects:@"session.running", @"deviceAuthorized", nil];
}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;
    
    [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
        if (granted) {
            //Granted access to mediaType
            [self setDeviceAuthorized:YES];
        } else {
            //Not granted access to mediaType
            dispatch_async(dispatch_get_main_queue(), ^{
                [[[PSAlertView alloc] initWithMessage:@"We can't seem to access the camera! Please change privacy settings to allow us to use it."
                                           delegate:self
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                [self setDeviceAuthorized:NO];
            });
        }
    }];
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position {
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];
    
    for (AVCaptureDevice *device in devices) {
        if ([device position] == position) {
            captureDevice = device;
            break;
        }
    }
    
    return captureDevice;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    state = 0;
    timerOpen = NO;
    timerOption = 45;
    
    // Setting up navigation buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    backBarButton = [[UIBarButtonItem alloc] initWithCustomView:back];
    
    UIView *gallery = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *galleryButton = [[UIButton alloc] initWithFrame:CGRectMake(4, 0, 40, 40)];
    [galleryButton setImage:[UIImage imageNamed:@"GalleryButton"] forState:UIControlStateNormal];
    [gallery addSubview:galleryButton];
    [galleryButton addTarget:self action:@selector(openGallery:) forControlEvents:UIControlEventTouchUpInside];
    galleryBarButton = [[UIBarButtonItem alloc] initWithCustomView:gallery];
    
    UIColor *green = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    timerBarButtonTitle = [[UIButton alloc] initWithFrame:CGRectMake(10, 6, 28, 28)];
    timerBarButtonTitle.layer.cornerRadius = 2.0;
    timerBarButtonTitle.backgroundColor = [UIColor whiteColor];
    timerBarButtonTitle.titleLabel.font = [UIFont fontWithName:@"VarelaRound-Regular" size:12];
    [timerBarButtonTitle setTitle:@"0" forState:UIControlStateNormal];
    [timerBarButtonTitle setTitleColor:green forState:UIControlStateNormal];
    [timerBarButtonTitle addTarget:self action:@selector(openTimer:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:timerBarButtonTitle];
    timerBarButton = [[UIBarButtonItem alloc] initWithCustomView:container];
    
    titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    [titleView setText:@"create puzzle"];
    [titleView setFont:[UIFont fontWithName:@"Montserrat-Regular" size:25]];
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    
    // Set up navigation if we are not nested
    if (![[self.parentViewController class] isSubclassOfClass:[PSScrollViewController class]]) {
        self.navigationItem.titleView = titleView;
        self.navigationItem.leftBarButtonItem = backBarButton;
        self.navigationItem.rightBarButtonItem = galleryBarButton;
    }
    
    // Setting up notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(adjustForKeyboard:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(retakePicture:) name:@"PicswitchSent" object:nil];
    
    // Setting up capture buttons
    captureButton.layer.borderColor = [UIColor whiteColor].CGColor;
    captureButton.layer.borderWidth = 3.0;
    captureButton.layer.cornerRadius = 10.0;
    
    // Setting up overlay
    [self cameraOverlay:3];
    
    // Setting up focus view
    focusView = [[PSFocusView alloc] initWithFrame:CGRectMake(0, 0, 72, 72)];
    [camOverlay addSubview:focusView];
    
    // Setting up tap gesture recognizer for focus
    UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(focusAndExposeTap:)];
    [previewView addGestureRecognizer:gr];
    [gr setNumberOfTapsRequired:1];
    [gr setNumberOfTouchesRequired:1];
    
    // Create the AVCaptureSession
    session = [[AVCaptureSession alloc] init];
    
    // Setup the preview view
    [previewView setSession:session];
    
    // Check for device authorization
    [self checkDeviceAuthorizationStatus];
    
    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch session setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).
    
    sessionQueue = dispatch_queue_create("session queue", DISPATCH_QUEUE_SERIAL);
    
    dispatch_async(sessionQueue, ^{
        backgroundRecordingID = UIBackgroundTaskInvalid;
        
        NSError *error = nil;
        
        AVCaptureDevice *videoDevice = [CameraViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
        AVCaptureDeviceInput *vi = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        
        if (error) {
            NSLog(@"%@", error);
        }
        
        flashMode = AVCaptureFlashModeAuto;
        
        BOOL success = [CameraViewController setFlashMode:flashMode forDevice:videoDevice];
        
        // Animate flash button change
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2 animations:^{
                [flashButton setAlpha:success];
            }];
        });
        
        if ([session canAddInput:vi]) {
            [session addInput:vi];
            videoDeviceInput = vi;
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for AVCamPreviewView and UIView can only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation changes on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                [(AVCaptureVideoPreviewLayer *)[previewView layer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
            });
        }
        
        AVCaptureStillImageOutput *io = [[AVCaptureStillImageOutput alloc] init];
        
        if ([session canAddOutput:io]) {
            [io setOutputSettings:@{AVVideoCodecKey : AVVideoCodecJPEG}];
            [session addOutput:io];
            stillImageOutput = io;
        }
    });
}

- (UINavigationController *)navigationController {
    if (![[self.parentViewController class] isSubclassOfClass:[PSScrollViewController class]]) {
        return super.navigationController;
    }
    
    return [self parentViewController].navigationController;
}

- (UINavigationItem *)navigationItem {
    if (![[self.parentViewController class] isSubclassOfClass:[PSScrollViewController class]]) {
        return super.navigationItem;
    }
    return [self parentViewController].navigationItem;
}

- (void)viewDidBecomeActive {
    [UIView animateWithDuration:0.15 animations:^{
        self.navigationItem.titleView.alpha = 0;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = titleView;
        [UIView animateWithDuration:0.15 animations:^{
            titleView.alpha = 1;
        }];
    }];
    
    [self.navigationItem setLeftBarButtonItem:backBarButton animated:YES];
    
    if (takePictureView.isHidden) {
        [self.navigationItem setRightBarButtonItem:timerBarButton animated:YES];
    } else {
        [self.navigationItem setRightBarButtonItem:galleryBarButton animated:YES];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    dispatch_async([self sessionQueue], ^{
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        
        [[self session] startRunning];
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    dispatch_async([self sessionQueue], ^{
        [[self session] stopRunning];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[self videoDeviceInput] device]];
        [[NSNotificationCenter defaultCenter] removeObserver:[self runtimeErrorHandlingObserver]];
    });
}

- (void)runStillImageCaptureAnimation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[previewImage layer] setOpacity:0.0];
        [UIView animateWithDuration:.2 animations:^{
            [[previewImage layer] setOpacity:1.0];
        }];
    });
}

- (IBAction)switchCamera:(id)sender {
    [changeCameraButton setEnabled:NO];
    [captureButton setEnabled:NO];
    
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *currentVideoDevice = [[self videoDeviceInput] device];
        AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
        AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
        
        switch (currentPosition) {
            case AVCaptureDevicePositionUnspecified:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
            case AVCaptureDevicePositionBack:
                preferredPosition = AVCaptureDevicePositionFront;
                break;
            case AVCaptureDevicePositionFront:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
        }
        
        AVCaptureDevice *videoDevice = [CameraViewController deviceWithMediaType:AVMediaTypeVideo preferringPosition:preferredPosition];
        AVCaptureDeviceInput *newVideoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];
        
        BOOL success = [CameraViewController setFlashMode:flashMode forDevice:videoDevice];
        
        // Animate flash button change
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2 animations:^{
                [flashButton setAlpha:success];
            }];
        });
        
        [[self session] beginConfiguration];
        
        [[self session] removeInput:[self videoDeviceInput]];
        if ([[self session] canAddInput:newVideoDeviceInput]) {
            [[NSNotificationCenter defaultCenter] removeObserver:self name:AVCaptureDeviceSubjectAreaDidChangeNotification object:currentVideoDevice];
            
            [CameraViewController setFlashMode:AVCaptureFlashModeAuto forDevice:videoDevice];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:videoDevice];
            
            [[self session] addInput:newVideoDeviceInput];
            [self setVideoDeviceInput:newVideoDeviceInput];
        } else {
            // Failed adding the new input. Going back to previous input
            [[self session] addInput:[self videoDeviceInput]];
        }
        
        [[self session] commitConfiguration];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [changeCameraButton setEnabled:YES];
            [captureButton setEnabled:YES];
        });
    });
}

- (IBAction)capturePic:(id)sender
{
    // Delay is for camera to focus!
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), [self sessionQueue], ^{
        // Update the orientation on the still image output video connection before capturing.
        [[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:AVCaptureVideoOrientationPortrait];
        
        // Capture a still image.
        [[self stillImageOutput] captureStillImageAsynchronouslyFromConnection:[[self stillImageOutput] connectionWithMediaType:AVMediaTypeVideo] completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
            
            if (imageDataSampleBuffer) {
                state = 1;
                
                NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                UIImage *image = [[UIImage alloc] initWithData:imageData];
                // Flip image if taken with front camera
                AVCaptureDevice *currentVideoDevice = [[self videoDeviceInput] device];
                AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
                if (currentPosition == AVCaptureDevicePositionFront)
                    image = [UIImage imageWithCGImage:image.CGImage scale:image.scale orientation:UIImageOrientationLeftMirrored];
                
                picFromCam = YES;
                [self pictureChosen:image];
            }
        }];
    });
}

- (IBAction)focusAndExposeTap:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint point = [gestureRecognizer locationInView:[gestureRecognizer view]];
    CGPoint devicePoint = [(AVCaptureVideoPreviewLayer *)[previewView layer] captureDevicePointOfInterestForPoint:point];
    [self focusWithMode:AVCaptureFocusModeAutoFocus exposeWithMode:AVCaptureExposureModeAutoExpose atDevicePoint:devicePoint monitorSubjectAreaChange:YES];
    [self displayFocusControl:point];
}

- (void)subjectAreaDidChange:(NSNotification *)notification {
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
    [self displayFocusControl:CGPointMake(camOverlay.bounds.size.width / 2, camOverlay.bounds.size.height / 2)];
}

// Flash has three states, stored by the tag. it always start in automatic.
// The three states are: Automatic, On, Off.
- (IBAction)toggleFlash:(UIButton *)sender {
    sender.tag = (sender.tag + 1) % 3;
    
    AVCaptureFlashMode mode = AVCaptureFlashModeOff;
    // Swap pic and change flash mode
    if (sender.tag == 0) {
        mode = AVCaptureFlashModeAuto;
        [flashButton setImage:[UIImage imageNamed:@"FlashAutoButton"] forState:UIControlStateNormal];
    } else if (sender.tag == 1) {
        mode = AVCaptureFlashModeOn;
        [flashButton setImage:[UIImage imageNamed:@"FlashOnButton"] forState:UIControlStateNormal];
    } else {
        [flashButton setImage:[UIImage imageNamed:@"FlashOffButton"] forState:UIControlStateNormal];
    }
    
    flashMode = mode;
    AVCaptureDevice *videoDevice = [[[[self session] inputs] firstObject] device];
    BOOL success = [CameraViewController setFlashMode:flashMode forDevice:videoDevice];
    
    // Animate flash button change
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.2 animations:^{
            [flashButton setAlpha:success];
        }];
    });
}

#pragma mark Device Configuration
- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange {
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [[self videoDeviceInput] device];
        NSError *error = nil;
        if ([device lockForConfiguration:&error]) {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode]) {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode]) {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        } else {
            NSLog(@"%@", error);
        }
    });
}

- (void)displayFocusControl:(CGPoint)loc {
    focusView.center = loc;
    [focusView showControl:YES];
}

+ (BOOL)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode]) {
        NSError *error = nil;
        if ([device lockForConfiguration:&error]) {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
            
            return YES;
        } else {
            NSLog(@"%@", error);
        }
    }
    
    return NO;
}


/**
 This shows how many grids we should over lay the camera by. Grids must be > 0.
 */
- (void)cameraOverlay:(int)grids {
    int g = grids > 0 ? grids : 1;
    
    // Creating horizontal rulers
    for (int i = 1; i < g; i++) {
        CGFloat width = camOverlay.bounds.size.width;
        CGFloat separation = i * camOverlay.bounds.size.height / g;
        CGRect rect = CGRectMake(0, separation, width, 1);
        
        UIView *v = [[UIView alloc] initWithFrame:rect];
        v.backgroundColor = [UIColor blackColor];
        [camOverlay addSubview:v];
    }
    
    // Creating vertical rulers
    for (int i = 1; i < g; i++) {
        CGFloat height = camOverlay.bounds.size.height;
        CGFloat separation = i * camOverlay.bounds.size.width / g;
        CGRect rect = CGRectMake(separation, 0, 1, height);
        
        UIView *v = [[UIView alloc] initWithFrame:rect];
        v.backgroundColor = [UIColor blackColor];
        [camOverlay addSubview:v];
    }
}

- (void)back:(id)sender {
    if (![[self.parentViewController class] isSubclassOfClass:[PSScrollViewController class]]) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [[self view] endEditing:YES];
        [(PSScrollViewController *)[self parentViewController] scrollLeft];
    }
}

- (IBAction)openTimer:(id)sender {
    // Pop up time selection view
    timerOpen = !timerOpen;
    
    [[self view] layoutIfNeeded];
    
    timerConstraint.constant = timerOpen ? 0 : -179;
    timerBackgroundMask.userInteractionEnabled = NO;
    [UIView animateWithDuration:0.2 animations:^{
        [[self view] layoutIfNeeded];
        timerBackgroundMask.alpha = timerOpen;
    } completion:^(BOOL finished) {
        timerBackgroundMask.userInteractionEnabled = YES;
    }];
}

- (void)pictureChosen:(UIImage *)image {
    // Treat picture
    previewImage.puzzleImage = [self squareImageWithImage:image scaledToSize:1000];
    [previewImage becomePreview];
    
    // Stopping inputs
    [self viewDidDisappear:NO];
    
    // Updating UI
    [self runStillImageCaptureAnimation];
    [self updateTimerButton:45];
    [sendButton setHidden:NO];
    [takePictureView setHidden:YES];
    [postPictureView setHidden:NO];
    [self adjustViewObstructHeight:0];
}

- (IBAction)retakePicture:(id)sender {
    state = 0;
    
    [self.navigationItem setRightBarButtonItem:galleryBarButton];
    
    [sendButton setHidden:YES];
    [takePictureView setHidden:NO];
    [postPictureView setHidden:YES];
    caption1.text = @"";
    caption2.text = @"";
    [self viewWillAppear:NO];
    [[self view] endEditing:YES];
}


- (IBAction)changeTimer:(id)sender {
    timerButton1.backgroundColor = [UIColor whiteColor];
    timerButton2.backgroundColor = [UIColor whiteColor];
    timerButton3.backgroundColor = [UIColor whiteColor];
    timerButton4.backgroundColor = [UIColor whiteColor];
    timerButton5.backgroundColor = [UIColor whiteColor];
    
    UIColor *green = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
    [timerButton1 setTitleColor:green forState:UIControlStateNormal];
    [timerButton2 setTitleColor:green forState:UIControlStateNormal];
    [timerButton3 setTitleColor:green forState:UIControlStateNormal];
    [timerButton4 setTitleColor:green forState:UIControlStateNormal];
    [timerButton5 setTitleColor:green forState:UIControlStateNormal];
    
    [self updateTimerButton:(int)[sender tag]];
    timerOption = (int)[sender tag];
    
    [self openTimer:sender];
}

- (void)updateTimerButton:(int)sec {
    [self.navigationItem setRightBarButtonItem:timerBarButton];
    [timerBarButtonTitle setTitle:sec == 0 ? @"∞" : [NSString stringWithFormat:@"%i", sec] forState:UIControlStateNormal];
    
    if (sec == 0) {
        timerButton5.backgroundColor = [UIColor clearColor];
        [timerButton5 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (sec == 15) {
        timerButton1.backgroundColor = [UIColor clearColor];
        [timerButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (sec == 30) {
        timerButton2.backgroundColor = [UIColor clearColor];
        [timerButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (sec == 45) {
        timerButton3.backgroundColor = [UIColor clearColor];
        [timerButton3 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    } else if (sec == 60) {
        timerButton4.backgroundColor = [UIColor clearColor];
        [timerButton4 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

- (UIImage *)squareImageWithImage:(UIImage *)image scaledToSize:(CGFloat)s {
    CGFloat ratio = 1;
    CGFloat dx = 0;
    CGFloat dy = 0;
    
    // First resize image then take square
    if (image.size.width > image.size.height) {
        ratio = image.size.height / s;
        dx = (image.size.width / ratio - image.size.height / ratio) / 2;
    } else {
        ratio = image.size.width / s;
        dy = (image.size.height / ratio - image.size.width / ratio) / 2;
    }
    
    CGSize newSize = CGSizeMake(image.size.width / ratio, image.size.height / ratio);
    CGRect newRect = CGRectMake(0, 0, newSize.width, newSize.height);
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0.0);
    [image drawInRect:newRect];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Crop image
    CGFloat scale = [newImage scale];
    CGRect clipRect = CGRectInset(newRect, dx, dy);
    clipRect = CGRectMake(clipRect.origin.x * scale, clipRect.origin.y * scale, clipRect.size.width * scale, clipRect.size.height *scale);

    CGImageRef cgimg = CGImageCreateWithImageInRect(newImage.CGImage, clipRect);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    
    return img;
}

// Let user choose a picture from their album
- (void)openGallery:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    [picker setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [picker setAllowsEditing:YES];
    [picker setDelegate:self];
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)adjustForKeyboard:(NSNotification *)note {
    NSDictionary *dict = [note userInfo];
    CGFloat currentOriginY = [[dict objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
    CGFloat screenSize = [UIScreen mainScreen].bounds.size.height;
    [self adjustViewObstructHeight:screenSize - currentOriginY];
}

- (void)adjustViewObstructHeight:(float)height {
    // Begin moving the view up
    [[self view] layoutIfNeeded];
    
    [UIView animateWithDuration:0.2 animations:^{
        scrollConstraint.constant = MAX(68, height);
        [[self view] layoutIfNeeded];
    }];
}

# pragma mark - UITextfield Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [[self view] endEditing:YES];
    [self adjustViewObstructHeight:68];
    isEditingCaption2 = NO;
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    // We are editing the bottom one - alert for extra scrolling
    isEditingCaption2 = textField == caption2;
    
    return YES;
}

- (IBAction)chooseContacts:(id)sender {
    PSContactViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ContactsVC"];
    
    // Build the picswitch and pass to the contact vc
    PFObject *picswitch = [PFObject objectWithClassName:@"Messages"];
    picswitch[@"fileType"] = @"image";
    picswitch[@"firstCaption"] = caption1.text;
    picswitch[@"secondCaption"] = caption2.text;
    picswitch[@"timeLimit"] = [NSNumber numberWithInt:timerOption];
    picswitch[@"previewIndex"] = [NSNumber numberWithInt:[previewImage selectedGrid]];
    // THis is default for the moment.
    picswitch[@"puzzleSize"] = [NSNumber numberWithInt:3];
    
    NSData *imageData = UIImageJPEGRepresentation(previewImage.puzzleImage, 0.8);
    PFFile *imageFile = [PFFile fileWithName:@"image.jpg" data:imageData];
    
    
    // Save picture to album
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"shouldSaveOutgoing"])
        UIImageWriteToSavedPhotosAlbum(previewImage.puzzleImage, nil, nil, nil);
    
    picswitch[@"photo"] = imageFile;
    
    [viewController assignPicswitch:picswitch];
    [viewController setMode:ContactViewControllerModeSend];
    
    // Set preselected if there are any
    if (recipients) {
        [viewController setRecipients:recipients];
        [viewController isReply];
    }
    
    [[self navigationController] pushViewController:viewController animated:YES];
}

#pragma mark - UIImagePickerControllerDelegate
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

// Chose picture, go to next step!
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if ([[self.parentViewController class] isSubclassOfClass:[PSScrollViewController class]])
        [(PSScrollViewController *)self.parentViewController jumpToPage:2];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    state = 1;
    picFromCam = NO;
    [self pictureChosen:image];
}

@end
