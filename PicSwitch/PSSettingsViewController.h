//
//  PSSettingsViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/24/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSSettingsViewController : UITableViewController

- (void)configureNavigation;

@end
