//
//  PSPuzzle.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/17/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 This class stores a completed puzzle. They are stored separately from server to
 conserve server space, as completed puzzles get deleted from the server.
 
 A note about sender info: sender name is a string of the user's name at the
 time of completion - this is to be used as placeholder only. The actual name
 might have changed so grabbing it entails a server communication.
 */

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PSPuzzle : NSManagedObject

@property (nonatomic, retain) NSString * firstCaption;
@property (nonatomic, retain) NSString * secondCaption;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSData * thumbnail;
@property (nonatomic, retain) NSString * sender;
@property (nonatomic, retain) NSString * senderFirstName;
@property (nonatomic, retain) NSString * senderLastName;
@property (nonatomic, retain) NSString * puzzleId;
@property (nonatomic, retain) NSNumber * completeTime;
@property (nonatomic, retain) NSNumber * completeSwitches;
@property (nonatomic, retain) NSNumber * puzzleSize;
@property (nonatomic, retain) NSDate * completeDate;
@property (nonatomic) BOOL disableRetry;

@end
