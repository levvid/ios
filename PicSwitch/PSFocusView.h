//
//  PSFocusView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/21/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 This class draws a focus UI element which can be manipulated to show, dismiss
 with animation.
 */
#import <UIKit/UIKit.h>

@interface PSFocusView : UIView

/**
 This, if animated, shows the control zooming and fading in, before fading out.
 */
- (void)showControl:(BOOL)animated;

@end
