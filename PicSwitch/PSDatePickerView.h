//
//  PSDatePickerView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/29/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Delegate methods for implementer to respond to various events.
 */
@class PSDatePickerView;
@protocol PSDatePickerViewDelegate <NSObject>

- (void)nextPressed:(PSDatePickerView *)pickerView;
- (void)donePressed:(PSDatePickerView *)pickerView;

@end


/**
 A date picker with a tool bar with "next" and "done" button.
 */
@interface PSDatePickerView : UIView
{
    IBOutlet NSLayoutConstraint *bottomConstraint;
}

@property (nonatomic, strong, readonly) UIPickerView *picker;
@property (nonatomic, weak) id <PSDatePickerViewDelegate> delegate;

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated;
- (int)year;

@end
