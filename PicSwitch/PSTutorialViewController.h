//
//  PSTutorialViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 7/21/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSTutorialViewController : UIViewController

/**
 This sets up the tutorial view with the images to display. Pass in an array of
 UIImage. Make sure to pass in at least one image!
 */
- (void)setupImages:(NSArray *)imageArray;

@end
