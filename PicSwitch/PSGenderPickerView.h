//
//  PSGenderPickerView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/30/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSGenderPickerView;
@protocol PSGenderPickerViewDelegate <NSObject>

- (void)donePressedGender:(PSGenderPickerView *)picker;

@end

@interface PSGenderPickerView : UIView
<UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet NSLayoutConstraint *bottomConstraint;
    int gender;
    
}

@property (nonatomic, strong, readonly) UIPickerView *picker;
@property (nonatomic, weak) id <PSGenderPickerViewDelegate> delegate;

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated;
- (NSString *)gender;
- (int)genderIndex;

@end
