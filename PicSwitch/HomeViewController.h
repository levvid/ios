//
//  HomeViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 2/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSwipeView.h"
#import "PSRefreshControl.h"
#import "PSPuzzleViewController.h"
#import "PSPuzzleCell.h"
#import "PSScrollViewController.h"

@class PSRefreshControl;
@interface HomeViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, PSSwipeViewDelegate,
UIScrollViewDelegate, PSRefreshControlDelegate, PSPuzzleViewControllerDelegate,
PSPuzzleCellDelegate, PSScrollViewControllerProtocol>
{
    IBOutlet UIButton *dropMenuButton;
    IBOutlet UIButton *friendButton;
    IBOutlet UIButton *lastMenuButton;
    IBOutlet NSLayoutConstraint *dropMenuButtonConstraint;
    IBOutlet NSLayoutConstraint *dropMenuHeightConstraint;
    IBOutlet UIView *mask;
    IBOutlet UITableView *contentTable;
    
    NSArray *newPuzzles;
    NSArray *featuredPuzzles;
    NSArray *solvedPuzzles;
    
    NSIndexPath *lastPuzzle;
    
    PSRefreshControl *refreshControl;
}

- (IBAction)dropMenuInteract:(id)sender;

- (IBAction)openContacts:(id)sender;

- (void)refreshContent;

@end
