//
//  PSRefreshControl.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/10/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 A refresh controller that looks like its been opened up behind the table!
 */

#import <UIKit/UIKit.h>

@class PSRefreshControl;
@protocol PSRefreshControlDelegate

/**
 This is sent when the refresh event is triggered.
 */
- (void)refreshControlTriggered:(PSRefreshControl *)control;

@end

@interface PSRefreshControl : UIControl

@property (weak, nonatomic) id <PSRefreshControlDelegate> delegate;

/**
 This is overriden to init with a frame
 */
- (id)init;

/**
 Override the implementation of scrollViewDidScroll: in UIScrollViewDelegate and 
 call this function inside of it. This lets the pong refresh control know to 
 update its subviews as the user scrolls.
 */
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;

/**
 Override the implementation of scrollViewDidEndDragging:willDecelerate: in 
 UIScrollViewDelegate and call this function inside of it. This lets the pong 
 refresh control know the user let go of the scroll view and causes it to check 
 if a refresh should be triggered.
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView;

/**
 This resets the refresh control with optional sliding animation.
 */
- (void)resetRefreshControlAnimated:(BOOL)isAnimated scrollView:(UIScrollView *)scrollView;

/**
 This resets the refresh control with optional block after animation completion
 */
- (void)resetRefreshControlWithCompletionBlock:(void (^)(BOOL))block scrollView:(UIScrollView *)scrollView;

@end

