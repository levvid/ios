//
//  PSNotificationView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/17/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSNotificationView.h"

@interface PSNotificationView () {
    UILabel *textLabel;
    
    float verticalPosition;
    
    BOOL isShowing;
}

@end

@implementation PSNotificationView
@synthesize isAutoTimed, displayTime;

- (id)initWithFrame:(CGRect)frame {
    return [self initWithMessage:@""];
}

- (id)initWithMessage:(NSString *)msg {
    self = [super initWithFrame:CGRectZero];
    
    if (self) {
        verticalPosition = 0.8;
        isAutoTimed = YES;
        isShowing = NO;
        
        // Init text label with maximum width, some height, then resize.
        float w = [UIScreen mainScreen].bounds.size.width * 0.8;
        float h = [UIScreen mainScreen].bounds.size.height * 0.2;
        textLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, w, h)];
        textLabel.text = msg;
        textLabel.numberOfLines = 0;
        textLabel.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
        textLabel.textAlignment = NSTextAlignmentCenter;
        textLabel.font = [UIFont fontWithName:@"Quicksand-Italic" size:14];
        textLabel.textColor = [UIColor blackColor];
        [textLabel sizeToFit];
        [self addSubview:textLabel];
        
        [self calculateFrame];
        
        // Setting up looks
        self.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
        self.layer.cornerRadius = 8;
        self.alpha = 0.0;
    }
    
    return self;
}

- (void)setMessage:(NSString *)msg {
    if (!isShowing) {
        textLabel.text = msg;
        [textLabel sizeToFit];
        [self calculateFrame];
    }
}

- (void)setVerticalAlignment:(float)position {
    if (!isShowing) {
        verticalPosition = position;
        [self calculateFrame];
    }
}

// Calculating the default frame
- (void)calculateFrame {
    self.frame = CGRectInset(textLabel.frame, -8, -5);
    
    float w = [UIScreen mainScreen].bounds.size.width / 2;
    float h = [UIScreen mainScreen].bounds.size.height * verticalPosition;
    self.center = CGPointMake(w, h);
}

- (void)show {
    isShowing = YES;
    
    // Add to top view
    [[[UIApplication sharedApplication] keyWindow] addSubview:self];
    
    // Animate in and out
    
    float waitTime = [self calculateWaitTime];
    
    // Fade in
    self.frame = CGRectOffset(self.frame, 0, 10);
    
    [UIView animateWithDuration:0.2 animations:^{
        self.alpha = 1.0;
        self.frame = CGRectOffset(self.frame, 0, -10);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 delay:waitTime options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 0.0;
            self.frame = CGRectOffset(self.frame, 0, -10);
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }];
}

- (float)calculateWaitTime {
    if (isAutoTimed) {
        return textLabel.text.length * 0.06 + 1;
    } else {
        return displayTime;
    }
}

@end
