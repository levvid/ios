//
//  PSTextField.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSTextField.h"

@implementation PSTextField

- (void)awakeFromNib {
    [self updatePlaceholder];
}

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 8, 5);
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 8, 5);
}

- (void)updatePlaceholder {
    // Change this to set different colors
    UIColor *color = [UIColor lightGrayColor];
    UIFont *font = [UIFont fontWithName:@"Quicksand-Italic" size:14];
    
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName:color, NSFontAttributeName:font}];
    
    // Workaround for iOS 7 placeholder font bug
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        [self setFont:font];
    }
}

@end
