//
//  LoginViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "MainViewController.h"
#import "PSDocViewController.h"
#import <Parse/Parse.h>
#import "PSScrollViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Login straight away if token exists
    if ([PFUser currentUser]) {
        PSScrollViewController *viewController = [[PSScrollViewController alloc] init];
        [[self navigationController] pushViewController:viewController animated:NO];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (IBAction)showTerms:(id)sender {
    NSString *urlStr = @"http://www.picswitchapp.com/terms";
    NSURL *myURL = [NSURL URLWithString:urlStr];
    
    [[UIApplication sharedApplication] openURL:myURL];
}

- (IBAction)showPrivacy:(id)sender {
    NSString *urlStr = @"http://www.picswitchapp.com/privacy";
    NSURL *myURL = [NSURL URLWithString:urlStr];
    
    [[UIApplication sharedApplication] openURL:myURL];
}

@end
