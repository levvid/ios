//
//  PSEditProfileViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 6/16/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <Parse/Parse.h>
#import "PSEditProfileViewController.h"
#import "PSAlertView.h"
#import "PSNotificationView.h"
#import "PSLoadingView.h"
#import "ServiceManager.h"

@interface PSEditProfileViewController ()
<PSAlertViewDelegate>
{
    IBOutlet UITextField *firstName;
    IBOutlet UITextField *lastName;
}
@end

@implementation PSEditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"edit profile"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Set up buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    // Set up fields
    firstName.text = [PFUser currentUser][@"firstName"];
    lastName.text = [PFUser currentUser][@"lastName"];
    
    [[PFUser currentUser] fetchInBackground];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self nextField:textField];
    
    return YES;
}

// iOS 7 workaround
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        UIFont *fontMain = [UIFont fontWithName:@"Montserrat-Bold" size:14];
        UIFont *fontPh = [UIFont fontWithName:@"Quicksand-Italic" size:14];
        
        if ([[[textField text] stringByReplacingCharactersInRange:range withString:string] isEqualToString:@""]) {
            [textField setFont:fontPh];
        } else {
            [textField setFont:fontMain];
        }
    }
    
    return YES;
}

- (void)nextField:(id)sender {
    if (sender == firstName) {
        [lastName becomeFirstResponder];
    } else if (sender == lastName) {
        [self confirmChange:self];
    }
}

- (void)confirmChange:(id)sender {
    NSString *message;
    
    if (firstName.text.length == 0) {
        message = @"First name is too short!";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if (lastName.text.length == 0) {
        message = @"Last name is too short!";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if ([[PFUser currentUser][@"nameChangeCount"] intValue] > 0) {
        message = @"You have already changed your name! If you want to change your name, please email names@picswitchapp.com with legal documentation.";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        message = @"Are you sure you want to change your name? You will only be able to change here once. ";
        [[[PSAlertView alloc] initWithMessage:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil] show];
    }
}

- (IBAction)dismissKeyboard:(id)sender {
    [[self view] endEditing:YES];
}

/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)alertView:(PSAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    if (index == 1) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PSLoadingView *loadingView = [[PSLoadingView alloc] init];
            [loadingView show:YES];
            
            [[ServiceManager sharedManager] changeUserFirstName:firstName.text lastName:lastName.text callback:^(BOOL success, NSError *err) {
                [loadingView dismiss:YES completion:^{
                    
                    if (success) {
                        [self.navigationController popViewControllerAnimated:YES];
                        [[[PSNotificationView alloc] initWithMessage:@"Name changed successfully!"] show];
                    } else {
                        NSString *message = @"Network error! Please try again.";
                        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                }];
            }];
        });
    }
}


@end
