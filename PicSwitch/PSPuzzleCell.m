//
//  PSPuzzleCell.m
//  PicSwitch
//
//  Created by Jiacong Xu on 2/5/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import <Parse/Parse.h>
#import "PSPuzzleCell.h"

@interface PSPuzzleCell ()
{
    IBOutlet UIView *barBackground;
    IBOutlet UIView *textBackground;
    IBOutlet UILabel *senderLabel;
    IBOutlet UILabel *messageLabel;
    IBOutlet UILabel *dateLabel;
    IBOutlet UIImageView *star;
    
    PFImageView *picture;
    
    UIButton *deleteButton;
    UIButton *cancelButton;
}
@end

@implementation PSPuzzleCell
@synthesize isDeleting, delegate, type, puzzleId;

- (void)awakeFromNib {
    // Initialization code
    CGRect rect = CGRectMake(230, 0, 80, 80);
    picture = [[PFImageView alloc] initWithFrame:rect];
    [[self contentView] addSubview:picture];
    [picture setImage:[UIImage imageNamed:@"PlaceholderImageSmall"]];
    
    // Adding support for deletion
    deleteButton = [[UIButton alloc] initWithFrame:rect];
    [deleteButton setImage:[UIImage imageNamed:@"TrashCanButton"] forState:UIControlStateNormal];
    deleteButton.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
    deleteButton.alpha = 0;
    deleteButton.clipsToBounds = YES;
    [deleteButton addTarget:self action:@selector(delete:) forControlEvents:UIControlEventTouchUpInside];
    
    CGRect rect2 = CGRectMake(10, 0, 220, 80);
    cancelButton = [[UIButton alloc] initWithFrame:rect2];
    cancelButton.alpha = 0;
    cancelButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.08];
    [cancelButton addTarget:self action:@selector(cancelDelete:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:cancelButton];
    [self addSubview:deleteButton];
    
    UILongPressGestureRecognizer *gr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [gr setNumberOfTouchesRequired:1];
    [gr setMinimumPressDuration:0.5];
    [self addGestureRecognizer:gr];
}

- (void)prepareForReuse {
    [self dontDelete:NO];
    [picture setImage:[UIImage imageNamed:@"LoginIcon"]];
}

- (void)setColorStyle:(int)style {
    switch (style) {
        case 0:
            [barBackground setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1]];
            [textBackground setBackgroundColor:[UIColor colorWithWhite:0.25 alpha:1.0]];
            break;
        case 1:
            [barBackground setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.6 brightness:0.6 alpha:1]];
            [textBackground setBackgroundColor:[UIColor clearColor]];
        default:
            break;
    }
}

- (void)setPicture:(UIImage *)image {
    [picture setImage:image];
}

- (void)setFile:(PFFile *)file {
    [picture setFile:file];
    [picture setImage:[UIImage imageNamed:@"LoginIcon"]];
    
    [picture loadInBackground:^(UIImage *img, NSError *error){
        if (error) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }];
}

- (void)setMessage:(NSString *)msg answer:(NSString *)ans {
    UIFont *msgFont = [UIFont fontWithName:@"Montserrat-Regular" size:12];
    NSDictionary *msgStyle = @{NSFontAttributeName : msgFont};
    UIFont *ansFont = [UIFont fontWithName:@"Quicksand-Italic" size:12];
    NSDictionary *ansStyle = @{NSFontAttributeName : ansFont};
    
    NSString *ansSpaced = [NSString stringWithFormat:@" %@", ans];
    NSMutableAttributedString *msgAtt = [[NSMutableAttributedString alloc] initWithString:msg attributes:msgStyle];
    NSMutableAttributedString *ansAtt = [[NSMutableAttributedString alloc] initWithString:ansSpaced attributes:ansStyle];
    
    [msgAtt appendAttributedString:ansAtt];
    
    [messageLabel setAttributedText:msgAtt];
}

- (void)setSender:(NSString *)sender {
    senderLabel.text = sender;
}

- (void)setTime:(NSString *)t {
    [dateLabel setText:t];
}

- (void)setStarred:(BOOL)flag {
    [star setHidden:!flag];
}

- (void)longPressed:(id)sender {
    if (!isDeleting) {
        [self showDelete:YES];
    }
}

- (BOOL)isDeleting {
    return isDeleting;
}

- (void)showDelete:(BOOL)show {
    if (type != PuzzleCellTypeFeatured) {
        isDeleting = YES;
        
        [UIView animateWithDuration:0.15 animations:^{
            deleteButton.alpha = show;
            cancelButton.alpha = show;
        }];
    }
}

- (void)cancelDelete:(id)sender {
    [self dontDelete:YES];
}

- (void)dontDelete:(BOOL)animated {
    if (type != PuzzleCellTypeFeatured && isDeleting) {
        isDeleting = NO;
        
        [UIView animateWithDuration:animated? 0.15 : 0 animations:^{
            deleteButton.alpha = 0;
            cancelButton.alpha = 0;
        }];
    }
}

- (void)delete:(id)sender {
    // User pressed delete button. Notify delegate
    [delegate puzzleCellShouldDelete:self];
}

@end
