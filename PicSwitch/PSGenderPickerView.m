//
//  PSGenderPickerView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/30/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSGenderPickerView.h"

#define MyGenderPickerHeight 162
#define MyGenderPickerToolbarHeight 44
#define MyConstantsElementAnimationLength 0.3

@interface PSGenderPickerView ()

@property (nonatomic, strong, readwrite) UIPickerView *picker;

@end

@implementation PSGenderPickerView

@synthesize picker, delegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (int)genderIndex {
    return gender + 1;
}

- (void)setup {
    // Set up a visual blur if we got iOS 8 and above. A semitransparent
    // background otherwise
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIVisualEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *ev = [[UIVisualEffectView alloc] initWithEffect:effect];
        [ev setFrame:self.bounds];
        [self addSubview:ev];
    } else {
        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.95];
    }
    
    CGFloat width = self.bounds.size.width;
    UIPickerView *gp = [[UIPickerView alloc] initWithFrame:CGRectMake(0, MyGenderPickerToolbarHeight, width, MyGenderPickerHeight)];
    gp.delegate = self;
    gp.dataSource = self;
    [self addSubview:gp];
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, width, MyGenderPickerToolbarHeight)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    toolbar.items = @[space, doneButton];
    [self addSubview:toolbar];
    
    self.picker = gp;
}

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated {
    [[self superview] layoutIfNeeded];
    bottomConstraint.constant = hidden ? -MyGenderPickerHeight - MyGenderPickerHeight : 0;
    [UIView animateWithDuration:animated ? MyConstantsElementAnimationLength : 0 animations:^{
        [[self superview] layoutIfNeeded];
    }];
}

- (void)donePressed:(id)sender {
    gender = (int)[picker selectedRowInComponent:0];
    [delegate donePressedGender:self];
}

- (NSString *)gender {
    switch (gender) {
        case 0:
            return @"Male";
            break;
        case 1:
            return @"Female";
            break;
        case 2:
            return @"Undisclosed";
            break;
        default:
            return @"";
    }
}

- (NSString *)genderForIndex:(int)index {
    switch (index) {
        case 0:
            return @"Male";
            break;
        case 1:
            return @"Female";
            break;
        case 2:
            return @"Undisclosed";
            break;
        default:
            return @"";
    }
}

#pragma mark - UIPickerViewDataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return 3;
}

#pragma mark - UIPickerViewDelegate
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self genderForIndex:(int)row];
}

@end
