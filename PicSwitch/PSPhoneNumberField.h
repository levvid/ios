//
//  PSPhoneNumberField.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/3/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSPhoneNumberField;
@protocol PSPhoneNumberFieldDelegate

- (void)nextPressedPhoneNumberField:(PSPhoneNumberField *)field;
- (void)donePressedPhoneNumberField:(PSPhoneNumberField *)field;

@end

/**
 This class handles phone number inputs. It uses a formatter, RMPhoneFormat, to
 format the phone number. This is done in a two stage process - user input is
 recorded and the actual text displayed is the formatted text, and when user
 tries to move to a digit, we adjust the position of the curser to the closest
 left digit. This should work exactly like the phone number field for iOS
 Contacts app.
 
 The delegate is responsible to pass on some methods back to this instance, if
 an external delegate is required. Otherwise, set its delegate to itself. The
 methods that need to be forwarded are:
 
 - textViewDidChangeSelection:
 
 - textView:shouldChangeTextInRange:replacementText:
 */
@interface PSPhoneNumberField : UITextView
<UITextViewDelegate>

@property (weak, nonatomic) id <PSPhoneNumberFieldDelegate> controlDelegate;

/**
 Whether the phone number entered is valid, digit-wise.
 */
- (BOOL)isValid;


/**
 The phone number contained in the field, without all format.
 */
- (NSString *)phoneNumber;

@end
