//
//  LoginViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/27/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *email;
    IBOutlet UITextField *password;
}

/**
 Attempts a login
 */
- (IBAction)login:(id)sender;

/**
 Dismisses the keyboard.
 */
- (IBAction)dismissKeyboard:(id)sender;

/**
 Going back to login.
 */
- (IBAction)backButtonPressed:(id)sender;

@end
