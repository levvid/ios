//
//  PSTransparentView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/11/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSTransparentView.h"

@implementation PSTransparentView

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    
    // If the hitView is THIS view, return nil and allow hitTest:withEvent: to
    // continue traversing the hierarchy to find the underlying view.
    if (hitView == self) {
        return nil;
    }
    
    // Else return the hitView (as it could be one of this view's buttons):
    return hitView;
}

@end
