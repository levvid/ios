//
//  PSDatePickerView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/29/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSDatePickerView.h"

#define MyDateTimePickerHeight 216
#define MyDateTimePickerToolbarHeight 44
#define MyConstantsElementAnimationLength 0.3

@interface PSDatePickerView()
<UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSArray *years;
}
@property (nonatomic, strong, readwrite) UIPickerView *picker;

@end

@implementation PSDatePickerView

@synthesize picker, delegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (void)setup {
    // Set up a visual blur if we got iOS 8 and above. A semitransparent
    // background otherwise
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        UIVisualEffect *effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *ev = [[UIVisualEffectView alloc] initWithEffect:effect];
        [ev setFrame:self.bounds];
        [self addSubview:ev];
    } else {
        self.backgroundColor = [UIColor colorWithWhite:0.9 alpha:0.95];
    }
    
    CGFloat width = self.bounds.size.width;
    
    UIPickerView *dp = [[UIPickerView alloc] initWithFrame:CGRectMake(0, MyDateTimePickerToolbarHeight, width, MyDateTimePickerHeight)];
    [dp setDelegate:self];
    
    //Get Current Year into i2
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2 = [[formatter stringFromDate:[NSDate date]] intValue];
    
    //Create Years Array from 1900 to This year
    years = @[];
    
    for (int i=1900; i<=i2; i++) {
        years = [years arrayByAddingObject:[NSString stringWithFormat:@"%d",i]];
    }
    
    [self addSubview:dp];
    [dp selectRow:[years count] - 15 inComponent:0 animated:NO];
    
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, width, MyDateTimePickerToolbarHeight)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *nextButton = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextPressed:)];
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
    toolbar.items = @[doneButton, space, nextButton];
    [self addSubview:toolbar];
    
    self.picker = dp;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView*)thePickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return [years count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView
             titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [years objectAtIndex:row];
}

- (void)setHidden:(BOOL)hidden animated:(BOOL)animated {
    [[self superview] layoutIfNeeded];
    bottomConstraint.constant = hidden ? - MyDateTimePickerHeight - MyDateTimePickerToolbarHeight : 0;
    [UIView animateWithDuration:animated ? MyConstantsElementAnimationLength : 0 animations:^{
        [[self superview] layoutIfNeeded];
    }];
}

/**
 Relaying the delegate method from bar button.
 */
- (void)nextPressed:(id)sender {
    [delegate nextPressed:self];
}

/**
 Relaying the delegate method from bar button.
 */
- (void)donePressed:(id)sender {
    [delegate donePressed:self];
}

- (int)year {
    return [[years objectAtIndex:[picker selectedRowInComponent:0]] intValue];
}

@end
