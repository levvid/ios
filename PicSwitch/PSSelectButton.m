//
//  PSSelectButton.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSSelectButton.h"

@implementation PSSelectButton

@synthesize delegate;

- (void)awakeFromNib {
    // Creating border
    border = [[UIView alloc] initWithFrame:CGRectInset(self.bounds, 5, 5)];
    
    border.layer.borderColor = [UIColor whiteColor].CGColor;
    border.layer.borderWidth = 1;
    border.layer.cornerRadius = 3;
    [self addSubview:border];
    
    self.clipsToBounds = YES;
    
    // Creating filling view
    gleam = [[UIView alloc] initWithFrame:CGRectMake(0, 0, border.frame.size.width, 5)];
    gleam.layer.cornerRadius = 5.0;
    gleam.backgroundColor = [UIColor colorWithHue:0.5 saturation:0.4 brightness:0.6 alpha:1.0];
    
    fill = [[UIView alloc] initWithFrame:CGRectMake(0, border.frame.size.height, border.frame.size.width, 0)];
    fill.layer.cornerRadius = 5.0;
    fill.backgroundColor = [UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1.0];
    fill.clipsToBounds = YES;
    [fill addSubview:gleam];
    [border addSubview:fill];
    
    // Creating foreground image holder;
    imageView = [[UIImageView alloc] initWithFrame:CGRectInset(self.bounds, 5, 5)];
    [self addSubview:imageView];
    
    // Creating button
    UIButton *button = [[UIButton alloc] initWithFrame:self.bounds];
    [button addTarget:self action:@selector(pressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:button];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self awakeFromNib];
    }
    
    return self;
}

- (void)pressed:(id)sender {
    [self setSelectionState:!selectionState animated:YES];
    if (delegate) {
        [delegate selectButton:self stateToggled:selectionState];
    }
}

- (void)setSelectionState:(BOOL)isSelected animated:(BOOL)shouldAnimate {
    selectionState = isSelected;
    
    [UIView animateWithDuration:shouldAnimate ? 0.15 : 0 animations:^{
        [fill setFrame:CGRectMake(0, !isSelected * border.frame.size.height, border.frame.size.width, isSelected * border.frame.size.height)];
    }];
}

- (BOOL)getSelectionState {
    return selectionState;
}

- (void)setImage:(UIImage *)image {
    imageView.image = image;
}

- (void)showBorder:(BOOL)shouldShow {
    border.alpha = shouldShow;
}

@end
