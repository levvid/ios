//
//  PSDiscreteProgressControl.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/22/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSDiscreteProgressControl.h"

@interface PSDiscreteProgressControl ()
{
    NSMutableArray *viewArray;
    NSMutableArray *fillArray;
}
@end

@implementation PSDiscreteProgressControl
@synthesize progress, totalNumber, lineColor, fillColor, highlightColor,
circleSize, circleSpacing;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (void)setup {
    progress = 0;
    totalNumber = 4;
    lineColor = [UIColor whiteColor];
    fillColor = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
    highlightColor = [UIColor colorWithHue:0.5 saturation:0.4 brightness:0.6 alpha:1.0];
    circleSpacing = 5.0;
    circleSize = 15.0;
    
    viewArray = [NSMutableArray array];
    [self layout];
}

- (void)layout {
    // Removing previous
    for (UIView *view in viewArray) {
        [view removeFromSuperview];
    }
    
    viewArray = [NSMutableArray array];
    fillArray = [NSMutableArray array];
    
    // Calculating frame
    CGFloat width = totalNumber * circleSize + (totalNumber - 1) * circleSpacing;
    self.frame = CGRectInset(self.frame, (self.frame.size.width - width) / 2, (self.frame.size.height - circleSize) / 2);
    
    // Creating circles
    for (int i = 0; i < totalNumber; i++) {
        // Creating container
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake((circleSize + circleSpacing) * i, 0, circleSize, circleSize)];
        container.layer.borderColor = lineColor.CGColor;
        container.layer.borderWidth = circleSize / 15.0;
        container.layer.cornerRadius = circleSize / 4;
        container.clipsToBounds = YES;
        
        // Creating filler
        UIView *fill = [[UIView alloc] initWithFrame:CGRectMake(0, circleSize, circleSize, circleSize)];
        fill.backgroundColor = fillColor;
        fill.layer.cornerRadius = circleSize / 3;
        [container addSubview:fill];
        UIView *highlight = [[UIView alloc] initWithFrame:CGRectMake(0, 0, circleSize, circleSize / 3)];
        highlight.backgroundColor = highlightColor;
        highlight.layer.cornerRadius = circleSize / 4;
        [fill addSubview:highlight];
        [fillArray addObject:fill];
        
        [viewArray addObject:container];
        [self addSubview:container];
    }
}

- (void)setProgress:(NSInteger)p {
    [self setProgress:p animated:NO];
}

- (void)setProgress:(NSInteger)p animated:(BOOL)isAnimated {
    progress = p;
    
    if (p < 0 || p > totalNumber) {
        NSLog(@"Invalid progress number!");
        return;
    }
    
    // Animating the change
    [UIView animateWithDuration:isAnimated ? 0.2 : 0 animations:^{
        for (int i = 0; i < totalNumber; i++) {
            UIView *fill = [fillArray objectAtIndex:i];
            
            if (i < progress) {
                [fill setFrame:[fill superview].bounds];
            } else {
                [fill setFrame:CGRectMake(0, circleSize, circleSize, circleSize)];
            }
        }
    }];
}


@end
