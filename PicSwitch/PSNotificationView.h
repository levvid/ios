//
//  PSNotificationView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/17/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSTransparentView.h"

/**
 This class displays a small, unintrusive message that serves as a notification.
 
 Simply set up the message and invoke display. Additionally set up y-axis before
 showing to adjust for vertical positioning. Look in the header for a list of
 configurable properties.
 */
@interface PSNotificationView : PSTransparentView

/**
 When YES, message display interval is auto-timed by message length. Default is
 YES.
 */
@property (nonatomic) BOOL isAutoTimed;

/**
 When isAutoTimed is NO, this is the amount of time message will be displayed.
 */
@property (nonatomic) float displayTime;

/**
 The default initializer. This initializes a new notification with the given
 message and default values for other properties.
 */
- (id)initWithMessage:(NSString *)msg;

/**
 This sets the text to display. This is only effective before showing the text.
 */
- (void)setMessage:(NSString *)msg;

/**
 This sets the vertical center of the message, relative to the screen size.
 Position takes values between 0 and 1, corresponding to the percentage of the
 screen the message should be centered at. This is only effective if called
 before showing the message. The default value is 0.8.
 */
- (void)setVerticalAlignment:(float)position;

/**
 This shows the message for the given interval. Afterwards the message will be
 faded out and dismissed automatically.
 */
- (void)show;

@end
