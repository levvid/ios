//
//  PSPhoneBookCell。h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSelectButton.h"

@class PSPhoneBookCell;
@protocol PSPhoneBookCellDelegate
// This is called when user selected a number to invite from this class.
- (void)cell:(PSPhoneBookCell *)cell didSelectNumber:(NSString *)number;

// This is called when user unselected a number
- (void)cell:(PSPhoneBookCell *)cell didUnselectNumber:(NSString *)number;

@end

@interface PSPhoneBookCell : UITableViewCell
<PSSelectButtonDelegate>
{
    IBOutlet UIView *bgView;
    IBOutlet UIView *barView;
    
    IBOutlet UILabel *nameLabel;
    
    NSArray *onlineArray;
    NSArray *friendsArray;
    NSArray *numbersArray;
    NSMutableArray *numViewArray;
    
    NSArray *inviteButtons;
    
    BOOL isExpanded;
    
    BOOL createdUI;
    
    NSLayoutConstraint *heightConstraint;
}

@property (strong, nonatomic) id <PSPhoneBookCellDelegate> delegate;

/**
 This sets the cell to one of the predefined color styles. The only valid values
 for style are 0 (dark) and 1 (light).
 */
- (void)setColorStyleForBackground:(int)background bar:(int)bar;

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName;

/**
 Based on the numbers for the contact, we expand the cell showing all those
 numbers and allow users to tap for them.
 */
- (void)shouldExpand:(BOOL)expand animated:(BOOL)animated;

/**
 Set a list of numbers for this cell to display on expansion.
 */
- (void)setNumbers:(NSArray *)numbers;

/**
 Set a pointer to a list of selected numbers. If one number is in this cell, we
 select it. Set this after we set numbers.
 */
- (void)setSelectedNumbers:(NSSet *)selected;

/**
 Set the meta data for the numbers. This allows the numbers to have icons
 indicating whether number is already on picswitch/friends with user. The arrays 
 should contain a subset of numbers. Set this after we set numbers.
 */
- (void)setOnPicswitch:(NSArray *)onPicswitch friend:(NSArray *)friendsArray;

/**
 Returns whether cell is expanded.
 */
- (BOOL)isExpanded;

/**
 Returns number of phone numbers contained in the cell
 */
- (NSInteger)numbersCount;

@end