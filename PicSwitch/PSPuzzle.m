//
//  PSPuzzle.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/17/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSPuzzle.h"


@implementation PSPuzzle

@dynamic firstCaption;
@dynamic secondCaption;
@dynamic photo;
@dynamic thumbnail;
@dynamic sender;
@dynamic senderFirstName;
@dynamic senderLastName;
@dynamic puzzleId;
@dynamic completeTime;
@dynamic completeSwitches;
@dynamic completeDate;
@dynamic puzzleSize;
@dynamic disableRetry;

@end
