//
//  PSTextField.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Extended with the ability to specify placeholder text color and text inset.
 */

@interface PSTextField : UITextField

- (void)updatePlaceholder;

@end
