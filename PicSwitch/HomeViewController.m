//
//  HomeViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 2/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSExtraPuzzleViewController.h"
#import "HomeViewController.h"
#import "PSPuzzleCell.h"
#import "CameraViewController.h"
#import "PSPuzzleViewController.h"
#import "ServiceManager.h"
#import "PSRefreshControl.h"
#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "PSPuzzle.h"
#import "PSSettingsViewController.h"
#import "PSContactViewController.h"
#import "PSDocViewController.h"
#import "PSTutorialViewController.h"

@interface HomeViewController ()
{
    // A thumbnail dictionary storing pointers to puzzle previews
    NSMutableDictionary *thumbnails;
    
    // A thumbnail dictionary storing pointers to solved puzzle previews
    NSMutableDictionary *solvedThumbnails;
    
    // Current number of rows to show for new messages
    int rowsForNew;
    
    // Current number of rows to show for solved
    int rowsForSolved;
    
    // Title buttons
    UIButton *titleButton;
    UIBarButtonItem *left;
    UIBarButtonItem *right;
    
    // Animation
    NSArray *dropMenuButtonAnimationImages;
    
    // Whether menu is open
    BOOL isMenuOpen;
}
@end

@implementation HomeViewController
#pragma mark - Constants
int const MAX_ROW_NUMBERS = 3;


#pragma mark - Body

- (void)viewWillAppear:(BOOL)animated {
    // Refresh content
    [self refreshContent];
}

- (void)viewDidBecomeActive {
    // Setting up the navigation controller
    // Fade out the current title and add the new one
    [UIView animateWithDuration:0.15 animations:^{
        self.navigationItem.titleView.alpha = 0;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = titleButton;
        [UIView animateWithDuration:0.15 animations:^{
            titleButton.alpha = 1;
        }];
    }];
    
    [self.navigationItem setLeftBarButtonItem:left animated:YES];
    [self.navigationItem setRightBarButtonItem:right animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setting up buttons
    titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    [titleButton setTitle:@"picswitch" forState:UIControlStateNormal];
    [[titleButton titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Regular" size:27]];
    [titleButton addTarget:self action:@selector(dropMenuInteract:) forControlEvents:UIControlEventTouchUpInside];
    
    UIView *leftContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [leftButton setImage:[UIImage imageNamed:@"MoreButton"] forState:UIControlStateNormal];
    [leftContainer addSubview:leftButton];
    [leftButton addTarget:self action:@selector(openAdditionalPuzzles:) forControlEvents:UIControlEventTouchUpInside];
    left = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
    
    UIView *rightContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [rightButton setImage:[UIImage imageNamed:@"CameraButton"] forState:UIControlStateNormal];
    [rightContainer addSubview:rightButton];
    [rightButton addTarget:self action:@selector(openCreate:) forControlEvents:UIControlEventTouchUpInside];
    right = [[UIBarButtonItem alloc] initWithCustomView:rightContainer];
    
    
    // Setting up the table view
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, -1000, 320, 1000)];
    bg.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
    [contentTable addSubview:bg];
    contentTable.sectionFooterHeight = 0;
    
    featuredPuzzles = nil;
    newPuzzles = nil;
    
    // Setting up menu button
    dropMenuButtonAnimationImages = @[[UIImage imageNamed:@"DownButton"], [UIImage imageNamed:@"DownButtonFlash"]];
    dropMenuButton.imageView.animationDuration = 1.5;
    
    // Initializing default max rows
    rowsForNew = MAX_ROW_NUMBERS;
    rowsForSolved = 2 * MAX_ROW_NUMBERS;
    
    // Setting up refresh control
    refreshControl = [[PSRefreshControl alloc] init];
    refreshControl.delegate = self;
    [contentTable addSubview:refreshControl];
    
    // Initializing stuff
    thumbnails = [NSMutableDictionary dictionary];
    solvedThumbnails = [NSMutableDictionary dictionary];
    isMenuOpen = NO;
    
    // Registering notification
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receivedNotification:) name:@"ReceivedNotification" object:nil];
}

- (void)receivedNotification:(NSNotification *)note {
    // Later - we need to check whether the notification is a new user request!
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"showMenuAlert"];
    [self updateFriendRequestCount:YES];
}

- (void)updateFriendRequestCount:(BOOL)showAnimation {
    [[[PFUser currentUser] relationForKey:@"friendRequests"].query countObjectsInBackgroundWithBlock:^(int count, NSError *err) {
        if (!err) {
            if (count > 0) {
                [friendButton setTitle:[NSString stringWithFormat:@"Friends (%i)", count] forState:UIControlStateNormal];
                // Finally animate the button - if the menu isn't open to begin with!
                if ((!isMenuOpen && showAnimation)){
                    dropMenuButton.imageView.animationImages = dropMenuButtonAnimationImages;
                    [dropMenuButton.imageView startAnimating];
                }
            } else
                [friendButton setTitle:@"Friends" forState:UIControlStateNormal];
            
        }
    }];
}

- (UINavigationController *)navigationController {
    return [self parentViewController].navigationController;
}

- (UINavigationItem *)navigationItem {
    return [self parentViewController].navigationItem;
}

NSInteger sort_solved(PFObject *obj1, PFObject *obj2, void *context) {
    // Sort using date created
    NSDictionary *solvedData = (__bridge NSDictionary *)(context);
    
    NSDate *date1 = [[solvedData objectForKey:obj2.objectId] objectAtIndex:2];
    NSDate *date2 = [[solvedData objectForKey:obj1.objectId] objectAtIndex:2];
    
    if (!date1) {
        return NSOrderedDescending;
    } else if (!date2) {
        return NSOrderedAscending;
    } else {
        return [date1 compare:date2];
    }
    
}

- (void)updateRecentContacts:(NSArray *)msgs {
    // Add recipients to the recent list
    
    // Grab recent user ids from local settings
    NSMutableDictionary *recent = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"RecentContacts"]];
    NSMutableArray *recentContactsForUser = [NSMutableArray arrayWithArray:[recent objectForKey:[PFUser currentUser].objectId]];
    
    for (PFObject *msg in msgs) {
        // If message is sent after update time, we ignore this one
        if ([msg.createdAt compare:[[NSUserDefaults standardUserDefaults] objectForKey:@"RecentContactsCheck"]] == NSOrderedAscending) {
            continue;
        }
        
        // Grab the user
        PFUser *user = msg[@"sender"];
        if (user == [PFUser currentUser])
            continue;
        
        NSMutableIndexSet *set = [NSMutableIndexSet indexSet];
        for (int i = 0; i < recentContactsForUser.count; i++) {
            NSString *userId = [recentContactsForUser objectAtIndex:i];
            
            if ([user.objectId isEqualToString:userId]) {
                [set addIndex:i];
            }
        }
        
        // Basically bubbles the given user up the list
        [recentContactsForUser removeObjectsAtIndexes:set];
        [recentContactsForUser insertObject:user.objectId atIndex:0];
    }
    
    // Trim the list
    if (recentContactsForUser.count > 5)
        [recentContactsForUser removeObjectsInRange:NSMakeRange(5, recentContactsForUser.count - 5)];
    
    // Save the list
    [recent setObject:recentContactsForUser forKey:[PFUser currentUser].objectId];
    [[NSUserDefaults standardUserDefaults] setObject:recent forKey:@"RecentContacts"];
    
    // Finally, update the date
    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"RecentContactsCheck"];
}

- (void)refreshContent {
    [[PFUser currentUser] fetchInBackgroundWithBlock:^(id obj, NSError *err){
        if (!err) {
            // Refresh friend count
            [self updateFriendRequestCount:[[NSUserDefaults standardUserDefaults] boolForKey:@"showMenuAlert"]];
            
            // Grabbing new
            [[ServiceManager sharedManager] fetchNewMessages:^(NSArray *msgs, NSError *err){
                [refreshControl resetRefreshControlWithCompletionBlock:^(BOOL finished){
                    if (err) {
                        NSLog(@"Something went wrong with message fetching! Desc: %@", [err localizedDescription]);
                    } else {
                        // Here, we update recent contacts
                        [self updateRecentContacts:msgs];
                        
                        newPuzzles = msgs;
                        [contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                } scrollView:contentTable];
            }];
            
            // Grabbing featured
            [[ServiceManager sharedManager] fetchFeaturedPuzzles:^(NSArray *msgs, NSError *err){
                [refreshControl resetRefreshControlWithCompletionBlock:^(BOOL finished){
                    if (err) {
                        NSLog(@"Something went wrong with message fetching! Desc: %@", [err localizedDescription]);
                    } else {
                        NSMutableArray *temp = [NSMutableArray array];
                        
                        for (PFObject *featured in msgs) {
                            if ([[NSDate date] compare:featured[@"startDate"]] == NSOrderedDescending &&
                                [[NSDate date] compare:featured[@"endDate"]] == NSOrderedAscending) {
                                [temp addObject:featured];
                            }
                        }
                        
                        featuredPuzzles = temp;
                        [contentTable reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
                    }
                } scrollView:contentTable];
            }];
            
            // Grabbing solved - first normal puzzles then featured puzzles
            NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"userCompleted"];
            NSArray *solvedIds = [dict objectForKey:[PFUser currentUser].objectId];
            NSDictionary *context = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SolveStats"] objectForKey:[PFUser currentUser].objectId];
            if (solvedIds > 0) {
                PFQuery *query = [PFQuery queryWithClassName:@"Messages"];
                [query whereKey:@"objectId" containedIn:solvedIds];
                [query fromLocalDatastore];
                
                [query findObjectsInBackgroundWithBlock:^(NSArray *normal, NSError *error) {
                    if (error) {
                        NSLog(@"Failed fetching solved puzzles! Error: %@", [error userInfo][@"error"]);
                    } else {
                        NSDictionary *dict2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"featuredCompleted"];
                        NSArray *solvedIds2 = [dict2 objectForKey:[PFUser currentUser].objectId];
                        if (solvedIds2 > 0) {
                            PFQuery *query = [PFQuery queryWithClassName:@"FeaturedPuzzles"];
                            [query whereKey:@"objectId" containedIn:solvedIds2];
                            [query fromLocalDatastore];
                            
                            [query findObjectsInBackgroundWithBlock:^(NSArray *featured, NSError *error) {
                                if (error) {
                                    NSLog(@"Failed fetching solved puzzles! Error: %@", [error userInfo][@"error"]);
                                } else {
                                    // Combine the two arrays and sort
                                    NSMutableArray *combined = [NSMutableArray arrayWithArray:normal];
                                    [combined addObjectsFromArray:featured];
                                    
                                    @try {
                                        [combined sortUsingFunction:sort_solved context:(__bridge void *)(context)];
                                    } @catch (NSException *exception) {}
                                    
                                    solvedPuzzles = combined;
                                    [contentTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
                                }
                            }];
                        } else {
                            // No featured!
                            @try {
                                solvedPuzzles = [normal sortedArrayUsingFunction:sort_solved context:(__bridge void *)(context)];
                            } @catch (NSException *exception) {
                                solvedPuzzles = normal;
                            }
                            [contentTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                    }
                }];
            } else {
                // Just try fetch featured instead.
                NSDictionary *dict2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"featuredCompleted"];
                NSArray *solvedIds2 = [dict2 objectForKey:[PFUser currentUser].objectId];
                if (solvedIds2 > 0) {
                    PFQuery *query = [PFQuery queryWithClassName:@"FeaturedPuzzles"];
                    [query whereKey:@"objectId" containedIn:solvedIds2];
                    [query fromLocalDatastore];
                    
                    [query findObjectsInBackgroundWithBlock:^(NSArray *featured, NSError *error) {
                        if (error) {
                            NSLog(@"Failed fetching solved puzzles! Error: %@", [error userInfo][@"error"]);
                        } else {
                            @try {
                                solvedPuzzles = [featured sortedArrayUsingFunction:sort_solved context:(__bridge void *)(context)];
                            } @catch (NSException *exception) {
                                solvedPuzzles = normal;
                            }
                            
                            [contentTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
                        }
                    }];
                }
            }
        }
    }];
}

- (void)openAdditionalPuzzles:(id)sender {
    [(PSScrollViewController *)self.parentViewController scrollLeft];
}

- (void)openCreate:(id)sender {
    [(PSScrollViewController *)self.parentViewController scrollRight];
}

- (IBAction)openAddFriend:(id)sender {
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PhoneBookVC"];
    
    // Retract menu
    [self dropMenuInteract:nil];
    
    // Push new vc
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)openContacts:(id)sender {
    PSContactViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ContactsVC"];
    
    [viewController setMode:ContactViewControllerModeView];
    
    // Retract menu
    [self dropMenuInteract:nil];
    
    // Push new vc
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)openStats:(id)sender {
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"StatsVC"];
    
    // Retract menu
    [self dropMenuInteract:nil];
    
    // Push new vc
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)openSettings:(id)sender {
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingsVC"];
    
    // Retract menu
    [self dropMenuInteract:nil];
    
    // Push new vc
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)dropMenuInteract:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"showMenuAlert"];
    dropMenuButton.imageView.animationImages = @[];
    [dropMenuButton.imageView stopAnimating];
    
    [[self view] layoutIfNeeded];
    
    isMenuOpen = !isMenuOpen;
    
    // Modifying arrow direction
    if (isMenuOpen) {
        dropMenuButtonConstraint.constant = -8;
        dropMenuHeightConstraint.constant = 162;
        
        [dropMenuButton setImage:[UIImage imageNamed:@"UpButton"] forState:UIControlStateNormal];
    } else {
        dropMenuButtonConstraint.constant = 0;
        dropMenuHeightConstraint.constant = 3;
        // To keep smoothness we change the button image back after a small window.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.05 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [dropMenuButton setImage:[UIImage imageNamed:@"DownButton"] forState:UIControlStateNormal];
        });
    }
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.75 initialSpringVelocity:0 options:UIViewAnimationOptionCurveLinear animations:^{
        mask.alpha = isMenuOpen;
        [[self view] layoutIfNeeded];
    } completion:nil];
    
    // Disabling settings menu because users can accidentally go straight to
    // settings even when the menu is retracted.
    [lastMenuButton setUserInteractionEnabled:isMenuOpen];
    
    [sender setTag:!isMenuOpen];
}

- (void)rulesButtonPressed:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FeaturedRules" ofType:@"txt"];
    NSString *file = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSString *string = [NSString stringWithFormat:@"Contest Rules\n%@", file];
    [[[PSAlertView alloc] initWithMessage:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (NSArray *)newPuzzles {
    return newPuzzles;
}

- (NSArray *)featuredPuzzles {
    return featuredPuzzles;
}

// User tapped on the expand button
- (void)expandNew:(id)sender {
    rowsForNew += MAX_ROW_NUMBERS;
    [contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

// User tapped on the expand button
- (void)expandSolved:(id)sender {
    rowsForSolved += 2 * MAX_ROW_NUMBERS;
    [contentTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return MIN(rowsForNew, [[self newPuzzles] count]);
            break;
        case 1:
            return MAX(1, [[self featuredPuzzles] count]);
        case 2:
            return MIN(rowsForSolved, [solvedPuzzles count]);
            break;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // If we got no featured, return the default cell straigt away
    if (indexPath.section == 1 && [[self featuredPuzzles] count] == 0) {
        return [tableView dequeueReusableCellWithIdentifier:@"NoFeaturedCellReuseIdentifier"];
    }
    
    PSPuzzleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PSPuzzleCellReuseIdentifier"];
    
    // Set cell color
    [cell setColorStyle:[indexPath row] % 2];
    
    // Set up delete status
    [cell setType:indexPath.section];
    [cell setDelegate:self];
    
    if (indexPath.section == 0) {
        PFObject *obj = [newPuzzles objectAtIndex:indexPath.row];
        
        // Set up identifier
        [cell setPuzzleId:obj.objectId];
        
        // Set up text
        [cell setMessage:obj[@"firstCaption"] answer:@""];
        
        // Set up image - only if we don't already have it!
        UIImage *thumbnail = [thumbnails objectForKey:obj.objectId];
        
        if (thumbnail) {
            [cell setPicture:thumbnail];
        } else {
            PFFile *imageFile = obj[@"photo"];
            int grids = [obj[@"puzzleSize"] intValue];
            int previewIndex = [obj[@"previewIndex"] intValue];
            
            [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage *treated = [self fetchPreviewForImage:image grid:previewIndex gridNumber:grids];
                    [cell setPicture:treated];
                    [thumbnails setObject:treated forKey:obj.objectId];
                }
            }];
        }
        
        // Set up time
        NSDate *createDate = obj.createdAt;
        NSString *timeString = [self dateDiff:createDate isFeatured:NO];
        [cell setTime:timeString];
        
        // Set up sender name
        PFUser *sender = obj[@"sender"];
        [cell setSender:[NSString stringWithFormat:@"%@ %@", sender[@"firstName"], sender[@"lastName"]]];
    } else if (indexPath.section == 1) {
        PFObject *obj = [featuredPuzzles objectAtIndex:indexPath.row];
        
        // Set up text
        NSString *caption1 = obj[@"firstCaption"];
        [cell setMessage:caption1 answer:@""];
        
        // Set up image - only if we don't already have it!
        UIImage *thumbnail = [thumbnails objectForKey:obj.objectId];
        
        if (thumbnail) {
            [cell setPicture:thumbnail];
        } else {
            PFFile *imageFile = obj[@"photo"];
            int grids = [obj[@"puzzleSize"] intValue];
            int previewIndex = [obj[@"previewIndex"] intValue];
            
            [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage *treated = [self fetchPreviewForImage:image grid:previewIndex gridNumber:grids];
                    [cell setPicture:treated];
                    [thumbnails setObject:treated forKey:obj.objectId];
                }
            }];
        }
        
        // Set up time
        NSDate *endDate = obj[@"endDate"];
        NSString *timeString = [self dateDiff:endDate isFeatured:YES];
        [cell setTime:timeString];
        
        // Set up sender name - this is for later
        /*
         PFUser *sender = obj[@"sender"];
         
         [sender fetchIfNeededInBackgroundWithBlock:^(PFObject *sender, NSError *err) {
         [cell setSender:[NSString stringWithFormat:@"%@ %@", sender[@"firstName"], sender[@"lastName"]]];
         }];
         */
        NSString *nameString = obj[@"companyName"];
        
        [cell setSender:nameString];
    } else {
        // Solved section - we need to handle two types here!
        PFObject *obj = [solvedPuzzles objectAtIndex:indexPath.row];
        
        // Set up identifier
        [cell setPuzzleId:obj.objectId];
        
        // Set up text
        [cell setMessage:obj[@"firstCaption"] answer:obj[@"secondCaption"]];
        
        // Set up image - only if we don't already have it! - CHANGE LATER
        UIImage *thumbnail = [solvedThumbnails objectForKey:obj.objectId];
        
        if (thumbnail) {
            [cell setPicture:thumbnail];
        } else {
            PFFile *imageFile = obj[@"photo"];
            [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
                if (!error) {
                    UIImage *image = [UIImage imageWithData:imageData];
                    UIImage *treated = [self fetchPreviewForImage:image grid:0 gridNumber:1];
                    [solvedThumbnails setObject:treated forKey:obj.objectId];
                    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }];
        }
        
        // Set up time
        NSDate *solveDate = [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"SolveStats"] objectForKey:[PFUser currentUser].objectId] objectForKey:obj.objectId] objectAtIndex:2];
        NSString *timeString = [self dateDiff:solveDate isFeatured:NO];
        [cell setTime:timeString];
        
        // HERE WE HAVE DIFFERENCE BETWEEN MESSAGE AND FEATURED
        if ([obj.parseClassName isEqualToString:@"FeaturedPuzzles"]) {
            NSString *nameString = obj[@"companyName"];
            [cell setSender:nameString];
        } else {
            // Set up sender name
            PFUser *sender = obj[@"sender"];
            
            // Fetch from server
            [sender fetchIfNeededInBackgroundWithBlock:^(PFObject *sender, NSError *err) {
                if (!err) {
                    [cell setSender:[NSString stringWithFormat:@"%@ %@", sender[@"firstName"], sender[@"lastName"]]];
                }
            }];
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
        case 1:
            return 40;
            break;
        case 2:
            return solvedPuzzles.count > 0 ? 40 : 0;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return newPuzzles.count > rowsForNew ? 30 : 0;
            break;
        case 1:
            return 0;
            break;
        case 2:
            return solvedPuzzles.count > rowsForSolved ? 30 : 0;
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView numberOfRowsInSection:section] == 0 && section == 2)
        return nil;
    
    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, height - 10)];
    UIFont *font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
    UIFont *fontSmall = [UIFont fontWithName:@"Montserrat-Regular" size:12];
    
    switch (section) {
        case 0:
            label.text = [NSString stringWithFormat:@"new (%i)", (int)[[self newPuzzles] count]];
            break;
        case 1: {
            label.text = @"featured";
            
            // Add a little question mark to display terms!
            UIButton *rulesButton = [UIButton buttonWithType:UIButtonTypeSystem];
            [rulesButton setFrame:CGRectMake(78, 0, height, height)];
            [rulesButton setTitle:@"(?)" forState:UIControlStateNormal];
            [[rulesButton titleLabel] setFont:fontSmall];
            [rulesButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [rulesButton addTarget:self action:@selector(rulesButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            [container addSubview:rulesButton];
            break;
        } case 2:
            label.text = @"solved";
            break;
    }
    
    label.font = font;
    label.textColor = [UIColor whiteColor];
    
    [container addSubview:label];
    
    return container;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    UIButton *expandButton = [[UIButton alloc] initWithFrame:CGRectMake(135, 0, 50, 30)];
    expandButton.layer.cornerRadius = 15;
    expandButton.layer.borderWidth = 4;
    expandButton.layer.borderColor = [UIColor colorWithWhite:0.2 alpha:1].CGColor;
    expandButton.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    expandButton.clipsToBounds = YES;
    expandButton.alpha = 0.75;
    
    [expandButton setImage:[UIImage imageNamed:@"ExpandButton"] forState:UIControlStateNormal];
    [footer addSubview:expandButton];
    
    if (section == 0) {
        [expandButton addTarget:self action:@selector(expandNew:) forControlEvents:UIControlEventTouchUpInside];
    } else if (section == 2) {
        [expandButton addTarget:self action:@selector(expandSolved:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return footer;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && featuredPuzzles.count == 0)
        return;
    
    PSPuzzleViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PVC"];
    viewController.delegate = self;
    
    viewController.puzzleSource = (PuzzleSource)indexPath.section;
    
    // Fetching puzzle
    PFObject *obj = [self puzzleForIndexPath:indexPath];
    
    [viewController setPuzzle:obj];
    lastPuzzle = indexPath;
    
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (PFObject *)puzzleForIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return indexPath.row < newPuzzles.count ? [newPuzzles objectAtIndex:indexPath.row] : nil;
    } else if (indexPath.section == 1) {
        return indexPath.row < featuredPuzzles.count ? [featuredPuzzles objectAtIndex:indexPath.row] : nil;
    } else {
        return indexPath.row < solvedPuzzles.count ? [solvedPuzzles objectAtIndex:indexPath.row] : nil;
    }
}

#pragma mark - PSSwipeViewDelegate Methods
- (void)swipeComplete:(PSSwipeView *)swipeView {
    [self openAdditionalPuzzles:swipeView];
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

/**
 This converts a date to a string of relative time
 */
- (NSString *)dateDiff:(NSDate *)date isFeatured:(BOOL)isFeatured
{
    NSDate *todayDate = [NSDate date];
    double ti = [date timeIntervalSinceDate:todayDate];
    
    if (isFeatured) {
        if (ti < 0) {
            return @"Promotion has ended!";
        } else if (ti < 120) {
            return @"One minute left";
        } else if (ti < 3600) {
            int diff = (int)(ti / 60);
            return [NSString stringWithFormat:@"%d minutes left", diff];
        } else if (ti < 86400) {
            int diff = (int)(ti / 3600);
            if (diff == 1) {
                return [NSString stringWithFormat:@"an hour left"];
            } else {
                return [NSString stringWithFormat:@"%d hours left", diff];
            }
        } else if (ti < 604800) {
            int diff = (int)(ti / 86400);
            if (diff == 1) {
                return [NSString stringWithFormat:@"a day left"];
            } else {
                return [NSString stringWithFormat:@"%d days left", diff];
            }
        } else if (ti < 2419200) {
            int diff = (int)(ti / 604800);
            if (diff == 1) {
                return [NSString stringWithFormat:@"a week left"];
            } else {
                return [NSString stringWithFormat:@"%d weeks left", diff];
            }
        } else {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"MMM yyyy"];
            return [NSString stringWithFormat:@"Ends in %@", [df stringFromDate:date]];
        }
    } else {
        ti = ti * -1;
        
        if (ti < -5) {
            return @"Your system time is messed up!";
        } else if (ti < 120) {
            return @"a minute ago";
        } else if (ti < 3600) {
            int diff = (int)(ti / 60);
            return [NSString stringWithFormat:@"%d minutes ago", diff];
        } else if (ti < 86400) {
            int diff = (int)(ti / 3600);
            if (diff == 1) {
                return [NSString stringWithFormat:@"an hour ago"];
            } else {
                return [NSString stringWithFormat:@"%d hours ago", diff];
            }
        } else if (ti < 604800) {
            int diff = (int)(ti / 86400);
            if (diff == 1) {
                return [NSString stringWithFormat:@"a day ago"];
            } else {
                return [NSString stringWithFormat:@"%d days ago", diff];
            }
        } else if (ti < 2419200) {
            int diff = (int)(ti / 604800);
            if (diff == 1) {
                return [NSString stringWithFormat:@"a week ago"];
            } else {
                return [NSString stringWithFormat:@"%d weeks ago", diff];
            }
        } else {
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"MMM yyyy"];
            return [df stringFromDate:date];
        }
    }
}

// This will take a square picture, dissect it into 9 grids, and take the passed
// in grid as an index number (going from left to right, then up to down) to
// return the image in that grid.
- (UIImage *)fetchPreviewForImage:(UIImage *)image grid:(int)whichGrid gridNumber:(int)gridNumber {
    // 0 is default grid numbers = 3
    int g = gridNumber == 0? 3 : gridNumber;
    
    // The default index is 4
    int gr = whichGrid;
    
    // This is the image's size! Default to 80
    CGFloat previewHeight = 80;
    
    CGImageRef pi = image.CGImage;
    
    // Cropping image
    int i = gr % 3;
    int j = gr / 3;
    
    CGFloat unitX = image.scale * image.size.width / g;
    CGFloat unitY = image.scale * image.size.height / g;
    CGRect clipRect = CGRectMake(unitX * i, unitY * j, unitX, unitY);
    CGImageRef cgimg = CGImageCreateWithImageInRect(pi, clipRect);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGImageRelease(cgimg);
    
    return [self imageWithImage:img scaledToSize:CGSizeMake(previewHeight, previewHeight)];
}

// Rescaling function
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark - UIScrollViewDelegate
// These help refresh control by letting it know when the view is scrolled
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [refreshControl scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [refreshControl scrollViewDidEndDragging:scrollView];
}

#pragma mark - PSRefreshControlDelegate
- (void)refreshControlTriggered:(PSRefreshControl *)control {
    [self refreshContent];
}

#pragma mark - PSPuzzleViewControllerDelegate
- (PFObject *)nextPuzzle {
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForRow:lastPuzzle.row + 1 inSection:lastPuzzle.section];
    lastPuzzle = nextIndexPath;
    
    return [self puzzleForIndexPath:nextIndexPath];
}

#pragma mark - PSPuzzleCellDelegate
- (void)puzzleCellShouldDelete:(PSPuzzleCell *)cell {
    NSIndexPath *ip = [contentTable indexPathForCell:cell];
    if (cell.type == PuzzleCellTypeMessage) {
        // Delete from database, treat as complete but don't send stats
        rowsForNew--;
        PFObject *obj = [newPuzzles objectAtIndex:ip.row];
        [[ServiceManager sharedManager] userPuzzleDeleted:obj];
        
        // Temp reload - actually still waiting for sync to take place!
        NSMutableArray *temp = [NSMutableArray arrayWithArray:newPuzzles];
        [temp removeObjectAtIndex:ip.row];
        newPuzzles = temp;
        [thumbnails removeObjectForKey:obj.objectId];
        [contentTable reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (cell.type == PuzzleCellTypeSolved) {
        // Delete from local database. AKA, unpin from local database.
        rowsForSolved--;
        PFObject *obj = [solvedPuzzles objectAtIndex:ip.row];
        [obj unpinInBackground];
        NSMutableArray *temp = [NSMutableArray arrayWithArray:solvedPuzzles];
        [temp removeObjectAtIndex:ip.row];
        solvedPuzzles = temp;
        [solvedThumbnails removeObjectForKey:obj.objectId];
        [contentTable reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}

@end
