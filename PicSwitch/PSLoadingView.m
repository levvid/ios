//
//  PSAlertView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSLoadingView.h"

@interface PSLoadingView ()
<UIViewControllerTransitioningDelegate,
UIViewControllerAnimatedTransitioning>
{
    IBOutlet UILabel *messageLabel;
    IBOutlet UIActivityIndicatorView *temp;
    IBOutlet UIView *alertView;
    
    NSString *message;
}

@end

@implementation PSLoadingView

- (id)init {
    return [self initWithTitle:@"LOADING"];
}

- (id)initWithTitle:(NSString *)title {
    self = [self initWithNibName:nil bundle:nil];
    
    if (self) {
        // Localization comes here
        message = title;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Customize appearance
    alertView.layer.cornerRadius = 10.0;
    
    // Setting text
    messageLabel.text = message;
    
    // Add fancy parallax effect
    // Set vertical effect
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-10);
    verticalMotionEffect.maximumRelativeValue = @(10);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-10);
    horizontalMotionEffect.maximumRelativeValue = @(10);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to your view
    [alertView addMotionEffect:group];
    
    // Adding fancy blur effect if it's iOS 8
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // Blur
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView.frame = self.view.bounds;
        blurView.alpha = .5;
        
        [self.view insertSubview:blurView atIndex:0];
    }
}

#pragma mark - UIViewControllerAnimatedTransitioning protocol
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return self;
}

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)context {
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)context {
    UIViewController *vc1 = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *vc2 = [context viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *container = [context containerView];
    UIView *v1 = vc1.view;
    UIView *v2 = vc2.view;
    
    if (vc2 == self) {
        // presenting
        [container addSubview:v2];
        v2.frame = v1.frame;
        alertView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        v2.alpha = 0;
        v1.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
        [UIView animateWithDuration:[self transitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            v2.alpha = 1;
            alertView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [context completeTransition:YES];
        }];
    } else {
        // dismissing
        [UIView animateWithDuration:[self transitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            v1.alpha = 0;
            alertView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:^(BOOL finished) {
            v2.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            [context completeTransition:YES];
        }];
    }
}

- (void)dismiss:(BOOL)animated {
    // Play dismiss animation
    [self dismissViewControllerAnimated:animated completion:nil];
}

- (void)dismiss:(BOOL)animated completion:(void (^)(void))completion {
    [self dismissViewControllerAnimated:animated completion:completion];
}

- (void)show:(BOOL)animated {
    [[PSLoadingView topMostController] presentViewController:self animated:animated completion:nil];
}

+ (UIViewController*)topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
