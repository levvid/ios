//
//  PSScrollViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 6/1/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSScrollViewController.h"
#import "PSExtraPuzzleViewController.h"
#import "HomeViewController.h"
#import "CameraViewController.h"

@interface PSScrollViewController ()
<UIScrollViewDelegate>
{
    UIViewController <PSScrollViewControllerProtocol> *currentController;
}

@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation PSScrollViewController
@synthesize scrollView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set up background
    float width = self.view.bounds.size.width;
    float height = self.view.bounds.size.height - 64;
    
    UIView *green = [[UIView alloc] initWithFrame:CGRectMake(-width, 0, width * 5, 10)];
    green.backgroundColor = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
    [self.view addSubview:green];
    
    UIView *white = [[UIView alloc] initWithFrame:CGRectMake(-width, 10, width * 5, 3)];
    white.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:white];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1];
    
    // Set up default navigation
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self.navigationController setNavigationBarHidden:NO];
    
    UIButton *titleButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    [titleButton setTitle:@"picswitch" forState:UIControlStateNormal];
    [[titleButton titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Regular" size:27]];
    self.navigationItem.titleView = titleButton;
    
    UIView *leftContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [leftButton setImage:[UIImage imageNamed:@"MoreButton"] forState:UIControlStateNormal];
    [leftContainer addSubview:leftButton];
    [leftButton addTarget:self action:@selector(scrollLeft) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:leftContainer];
    self.navigationItem.leftBarButtonItem = left;
    
    UIView *rightContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [rightButton setImage:[UIImage imageNamed:@"CameraButton"] forState:UIControlStateNormal];
    [rightContainer addSubview:rightButton];
    [rightButton addTarget:self action:@selector(scrollRight) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:rightContainer];
    self.navigationItem.rightBarButtonItem = right;
    
    // Set up scrollview
    scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.contentSize = CGSizeMake(3 * width, height);
    scrollView.contentOffset = CGPointMake(width, 0);
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    [self.view addSubview:scrollView];
    
    PSExtraPuzzleViewController *viewController1 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"EVC"];
    [self addChildViewController:viewController1];
    [viewController1.view setFrame:CGRectMake(0, 0, width, height)];
    
    HomeViewController *viewController2 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"HVC"];
    [self addChildViewController:viewController2];
    [viewController2.view setFrame:CGRectMake(width, 0, width, height)];
    
    CameraViewController *viewController3 = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CVC"];
    [self addChildViewController:viewController3];
    [viewController3.view setFrame:CGRectMake(2 * width, 0, width, height)];
    
    [scrollView addSubview:viewController1.view];
    [scrollView addSubview:viewController2.view];
    [scrollView addSubview:viewController3.view];
    
    [viewController2 refreshContent];
    
    currentController = viewController2;
    
    [titleButton addTarget:viewController2 action:@selector(dropMenuInteract:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sv {
    // Calculate paging and set up navigation correctly
    int page = [self currentPage];
    UIViewController <PSScrollViewControllerProtocol> *cv = [self.childViewControllers objectAtIndex:page];
    
    if (cv != currentController) {
        [cv viewWillAppear:YES];
        if ([cv respondsToSelector:@selector(viewDidBecomeActive)])
            [cv viewDidBecomeActive];
        [currentController viewWillDisappear:YES];
        if ([currentController respondsToSelector:@selector(viewDidBecomeInactive)])
            [currentController viewDidBecomeActive];
        
        currentController = cv;
    }
}

- (int)currentPage {
    return roundf((scrollView.contentOffset.x) / self.view.bounds.size.width);
}

- (void)scrollLeft {
    int newPage = MIN(2, MAX(0, [self currentPage] - 1));
    [UIView animateWithDuration:0.2 animations:^{
        [scrollView setContentOffset:CGPointMake(newPage * self.view.bounds.size.width, 0)];
    } completion:^(BOOL finished) {
        [self scrollViewDidEndDecelerating:scrollView];
    }];
}

- (void)scrollRight {
    int newPage = MIN(2, MAX(0, [self currentPage] + 1));
    [UIView animateWithDuration:0.2 animations:^{
        [scrollView setContentOffset:CGPointMake(newPage * self.view.bounds.size.width, 0)];
    } completion:^(BOOL finished) {
        [self scrollViewDidEndDecelerating:scrollView];
    }];
}

// Jump to the given page
- (void)jumpToPage:(int)page {
    [scrollView setContentOffset:CGPointMake(page * self.view.bounds.size.width, 0)];
    [self scrollViewDidEndDecelerating:scrollView];
}

- (void)viewWillAppear:(BOOL)animated {
    // Allow setup for appropriate page
    [self forceNavigationReset];
}

- (void)forceNavigationReset {
    int page = [self currentPage];
    UIViewController <PSScrollViewControllerProtocol> *cv = [self.childViewControllers objectAtIndex:page];
    if ([cv respondsToSelector:@selector(viewDidBecomeActive)])
        [cv viewDidBecomeActive];
}

- (void)dealloc {
    
}

@end
