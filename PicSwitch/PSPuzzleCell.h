//
//  PSPuzzleCell.h
//  PicSwitch
//
//  Created by Jiacong Xu on 2/5/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    PuzzleCellTypeMessage,
    PuzzleCellTypeFeatured,
    PuzzleCellTypeSolved
} PuzzleCellType;


@class PSPuzzleCell;
@protocol PSPuzzleCellDelegate

- (void)puzzleCellShouldDelete:(PSPuzzleCell *)cell;

@end

@class PFFile;
@interface PSPuzzleCell : UITableViewCell

/// This indicates whether the cell is deleting - don't go into puzzle from this state!
@property (nonatomic, readonly) BOOL isDeleting;

@property (nonatomic, weak) id <PSPuzzleCellDelegate> delegate;

@property (nonatomic) PuzzleCellType type;

/// This is the puzzle's identifier, used to delete it later on.
@property (nonatomic, strong) NSString *puzzleId;

/**
 This sets the cell to one of the predefined color styles. The only valid values
 for style are 0 (dark) and 1 (light).
 */
- (void)setColorStyle:(int)style;

/**
 This sets the picture of the cell. Use file instead!
 */
- (void)setPicture:(UIImage *)picture;

/**
 This sets the picture of the cell. Use file instead!
 */
- (void)setFile:(PFFile *)file;

/**
 This sets the main message associated with picture. Leave answer nil if it
 shouldn't be displayed.
 */
- (void)setMessage:(NSString *)msg answer:(NSString *)ans;

/**
 This sets the one line time at the bottom.
 */
- (void)setTime:(NSString *)t;

/**
 This sets the sender name.
 */
- (void)setSender:(NSString *)sender;

/**
 This sets the visibility of the star. Default is hidden.
 */
- (void)setStarred:(BOOL)flag;

/**
 Stops the deletion and resets the button look. If button isn't in delete state,
 do nothing.
 */
- (void)dontDelete:(BOOL)animated;

@end
