//
//  PSFocusView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/21/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSFocusView.h"

@implementation PSFocusView

CGFloat const CORNER_RATIO = 0.3;
CGFloat const FOCUS_ANIMATION_DURATION = 0.2;
CGFloat const FOCUS_ANIMATION_INTERVAL = 1.0;
CGFloat const FOCUS_ANIMATION_MAX_SCALE = 1.5;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    self.opaque = false;
    self.backgroundColor = [UIColor clearColor];
    self.contentMode = UIViewContentModeRedraw;
    
    // Initially hidden
    self.alpha = 0;
}

- (void)drawRect:(CGRect)rect {
    // Draws four corners
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat width = self.bounds.size.width;
    CGFloat height = self.bounds.size.height;
    CGFloat w = self.bounds.size.width * CORNER_RATIO;
    CGFloat h = self.bounds.size.height * CORNER_RATIO;
    
    // Top left corner
    CGContextMoveToPoint(context, 0, h);
    CGContextAddLineToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, w, 0);
    
    // Top right corner
    CGContextMoveToPoint(context, width - w, 0);
    CGContextAddLineToPoint(context, width, 0);
    CGContextAddLineToPoint(context, width, h);
    
    // Bottom right corner
    CGContextMoveToPoint(context, width, height - h);
    CGContextAddLineToPoint(context, width, height);
    CGContextAddLineToPoint(context, width - w, height);
    
    // Bottom left corner
    CGContextMoveToPoint(context, w, height);
    CGContextAddLineToPoint(context, 0, height);
    CGContextAddLineToPoint(context, 0, height - h);
    
    // Configure line
    CGContextSetLineWidth(context, 2.0);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    [[UIColor whiteColor] setStroke];
    CGContextStrokePath(context);
}

- (void)showControl:(BOOL)animated {
    self.transform = CGAffineTransformMakeScale(FOCUS_ANIMATION_MAX_SCALE, FOCUS_ANIMATION_MAX_SCALE);
    
    [UIView animateWithDuration:FOCUS_ANIMATION_DURATION animations:^{
        self.alpha = 1.0;
        self.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:FOCUS_ANIMATION_DURATION delay:FOCUS_ANIMATION_INTERVAL options:UIViewAnimationOptionCurveLinear animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished) {
        }];
    }];
}

@end
