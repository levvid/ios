//
//  PSContact.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/3/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 This class represents a contact a user has, whether it is a PicSwitch contact
 or just imported from the phone's address book.
 */

#import <Foundation/Foundation.h>

@interface PSContact : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (readonly, strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSArray *phoneNumbers;
@property (strong, nonatomic) NSArray *isFriend;
@property (strong, nonatomic) NSArray *onPicswitch;


@end
