//
//  PSPuzzleView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/1/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSPuzzleView.h"
#import "PSTransparentView.h"

typedef enum : NSUInteger {
    PSPuzzleViewStateLoading,
    PSPuzzleViewStateActive,
    PSPuzzleViewStateSolved,
    PSPuzzleViewStatePreview
} PSPuzzleViewState;

@interface PSPuzzleView()
{
    // This contains the gridded look for the view.
    PSTransparentView *gridView;
    
    // This is the selection outline
    PSTransparentView *selectionBorder;
    
    /*
     We use an array to keep all the programmatically created UIImageViews, then
     another to store all images. During a swap we reassign the swapped images
     to their image views. After each swap, we check the order of images to 
     determine whether we solved the puzzle. Note: imageViews are actually
     UIButtons. The order of the array images is not changed, only the order
     array.
     */
    NSArray *imageViews;
    NSArray *images;
    NSMutableArray *order;
    
    int gridNumber;
    
    // This is the selected grid index, in imageViews. If none is selected, this
    // takes the value -1.
    int selectedGrid;
    
    PSPuzzleViewState state;
    
    CGRect defaultSelectionFrame;
}

@end

@implementation PSPuzzleView
#pragma mark - Constants
CGFloat const SELECTED_BORDER_RATIO = 0.02;
CGFloat const ANIMATION_INTERVAL = 0.15;
CGFloat const ANIMATION_SCALE = 0.93;

#pragma mark - Body


@synthesize puzzleImage, delegate;

- (void)awakeFromNib {
    // Default grid number is 3 for now
    gridNumber = 3;
    
    // Setting up touch events
    selectedGrid = -1;
    
    // Initializing arrays
    images = [NSArray array];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self createGrid];
    });
}

- (void)createGrid {
    // Creating grids
    gridView = [[PSTransparentView alloc] initWithFrame:self.bounds];
    [self addSubview:gridView];
    [self createGrid:gridNumber];
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1.0;
    
    // Setting up selection border
    CGFloat width = self.bounds.size.width / gridNumber;
    CGFloat height = self.bounds.size.height / gridNumber;
    CGFloat borderWidth = width * SELECTED_BORDER_RATIO;
    CGRect square = CGRectMake(0, 0, width, height);
    CGRect inset = CGRectInset(square, borderWidth, borderWidth);
    
    PSTransparentView *white = [[PSTransparentView alloc] initWithFrame:inset];
    white.layer.borderWidth = borderWidth;
    white.layer.borderColor = [UIColor whiteColor].CGColor;
    
    selectionBorder = [[PSTransparentView alloc] initWithFrame:square];
    selectionBorder.layer.borderWidth = borderWidth;
    selectionBorder.layer.borderColor = [UIColor blackColor].CGColor;
    [selectionBorder addSubview:white];
    [self addSubview:selectionBorder];
    [selectionBorder setAlpha:0];
}

- (void)drawRect:(CGRect)rect {
    // placeholder!
    switch (state) {
        case PSPuzzleViewStateLoading:
        case PSPuzzleViewStateActive:
            [[UIImage imageNamed:@"PlaceholderImage"] drawInRect:self.bounds];
            break;
        case PSPuzzleViewStateSolved:
        case PSPuzzleViewStatePreview:
            if (!puzzleImage)
                [[UIImage imageNamed:@"PlaceholderImage"] drawInRect:self.bounds];
            else
                [puzzleImage drawInRect:self.bounds];
            break;
    }
}

- (void)setPuzzleImage:(UIImage *)p {
    // Do a resizing
    UIGraphicsBeginImageContextWithOptions(self.bounds.size, YES, 0.0);
    [p drawInRect:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    puzzleImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    [self setNeedsDisplay];
}

- (void)createGrid:(int)grids {
    int g = grids > 0 ? grids : 1;
    
    // Creating horizontal rulers
    for (int i = 1; i < g; i++) {
        CGFloat width = gridView.bounds.size.width;
        CGFloat separation = i * gridView.bounds.size.height / g;
        CGRect rect = CGRectMake(0, separation, width, 1);
        
        UIView *v = [[UIView alloc] initWithFrame:rect];
        v.backgroundColor = [UIColor blackColor];
        [gridView addSubview:v];
    }
        
    // Creating vertical rulers
    for (int i = 1; i < g; i++) {
        CGFloat height = gridView.bounds.size.height;
        CGFloat separation = i * gridView.bounds.size.width / g;
        CGRect rect = CGRectMake(separation, 0, 1, height);
        
        UIView *v = [[UIView alloc] initWithFrame:rect];
        v.backgroundColor = [UIColor blackColor];
        [gridView addSubview:v];
    }
}

- (void)toggleGrid:(BOOL)isShowing animated:(BOOL)animated {
    [UIView animateWithDuration:animated ? 0.2 : 0 animations:^{
        [gridView setAlpha:isShowing];
    }];
}

- (NSArray *)createImageViews:(int)grids {
    NSMutableArray *array = [NSMutableArray array];
    
    int g = grids > 0 ? grids : 1;
    
    int count = 0;
    
    for (int i = 0; i < g; i++) {
        for (int j = 0; j < g; j++) {
            CGFloat unitX = self.bounds.size.width / g;
            CGFloat unitY = self.bounds.size.height / g;
            
            CGRect rect = CGRectMake(unitX * j, unitY * i, unitX, unitY);
            UIButton *button = [[UIButton alloc] initWithFrame:rect];
            button.tag = count;
            [button addTarget:self action:@selector(userTapped:)
             forControlEvents:UIControlEventTouchUpInside];
            button.adjustsImageWhenHighlighted = NO;
            [self addSubview:button];
            [array addObject:button];
            
            count++;
            
            // Save selection frame!
            if (i == j && (int)ceilf((float)g / (i + 1)) == 2) {
                defaultSelectionFrame = rect;
            }
        }
    }
    
    return [NSArray arrayWithArray:array];
}

- (NSArray *)dissectImage:(int)grids {
    NSArray *array = [NSArray array];
    
    int g = grids > 0 ? grids : 1;
    
    CGImageRef pi = puzzleImage.CGImage;
    
    for (int i = 0; i < g; i++) {
        for (int j = 0; j < g; j++) {
            // Cropping image
            CGFloat unitX = puzzleImage.scale * self.bounds.size.width / g;
            CGFloat unitY = puzzleImage.scale * self.bounds.size.height / g;
            CGRect clipRect = CGRectMake(unitX * j, unitY * i, unitX, unitY);
            CGImageRef cgimg = CGImageCreateWithImageInRect(pi, clipRect);
            UIImage *img = [UIImage imageWithCGImage:cgimg];
            array = [array arrayByAddingObject:img];
            
            CGImageRelease(cgimg);
        }
    }
    
    return array;
}

// This constructs the order array.
- (void)shufflePuzzle {
    // Can only be called if we have images!
    if (images.count <= 0)
        return;
    
    NSInteger count = images.count;
    order = [NSMutableArray array];
    
    // Initializing temp array
    NSMutableArray *tempOrder = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        [tempOrder addObject:[NSNumber numberWithInt:i]];
    }
    
    for (int i = 0; i < count; i++) {
        int index = arc4random() % tempOrder.count;
        [order addObject:[tempOrder objectAtIndex:index]];
        [tempOrder removeObjectAtIndex:index];
    }
}

// This shuffles a puzzle according to a random sequence defined by sequence.txt
- (void)shufflePuzzleWithSequence {
    if (images.count <= 0)
        return;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"sequence" ofType:@"txt"];
    NSArray *content = [[NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL] componentsSeparatedByString:@"\n"];
    NSString *orderStr = [content objectAtIndex:arc4random() % content.count];
    order = [NSMutableArray array];
    for (int i = 0; i < [orderStr length]; i++) {
        NSString *ch = [orderStr substringWithRange:NSMakeRange(i, 1)];
        if ([ch isEqualToString:@"\r"])
            continue;
        [order addObject:[NSNumber numberWithInt:[ch intValue] - 1]];
    }
}

// Swap the two images, updating the image views as well as the array itself.
- (void)swapImage:(int)posA withImage:(int)posB {
    [order exchangeObjectAtIndex:posA withObjectAtIndex:posB];
    [self updateAllImageViews];
}

// This forces all image views to take on images from images array.
- (void)updateAllImageViews {
    if (images.count <= 0)
        return;
    
    NSInteger count = imageViews.count;
    for (int i = 0; i < count; i++) {
        int index = [[order objectAtIndex:i] intValue];
        UIImage *image = [images objectAtIndex:index];
        [[imageViews objectAtIndex:i] setBackgroundImage:image forState:UIControlStateNormal];
    }
}

// Checks whether we finished the puzzle!
- (BOOL)checkCompleted {
    // Checks each entry of order in order, and returns true only if everything
    // is in place.
    for (int i = 0; i < order.count; i++) {
        if ([[order objectAtIndex:i] intValue] != i) {
            return NO;
        }
    }
    
    return YES;
}

// Tidying up after user solved the puzzle!
- (void)puzzleCompleted {
    // Animation
    [self toggleGrid:NO animated:YES];
    [UIView animateWithDuration:ANIMATION_INTERVAL / 2 animations:^{
        self.transform = CGAffineTransformScale(self.transform, ANIMATION_SCALE, ANIMATION_SCALE);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:ANIMATION_INTERVAL animations:^{
            self.transform = CGAffineTransformIdentity;
        }];
    }];
    
    state = PSPuzzleViewStateSolved;
    [self setNeedsDisplay];
    [delegate userCompleted:self];
}

- (void)startSolvingAnimated:(BOOL)animated {
    // We must have an image to get started!
    if (!puzzleImage)
        return;
    
    // Create image view containers and initialize all the arrays
    imageViews = [self createImageViews:gridNumber];
    
    images = [self dissectImage:gridNumber];
    
    // Make grid view top layer
    [self bringSubviewToFront:gridView];
    [self bringSubviewToFront:selectionBorder];
    
    // Then pose the puzzle - mix up the image and the order array!
    [self shufflePuzzleWithSequence];
    [self updateAllImageViews];
    state = PSPuzzleViewStateActive;
    [self setNeedsDisplay];
    
    // Fade in if animated
    if (animated) {
        for (UIControl *view in imageViews) {
            view.alpha = 0;
        }
        
        [UIView animateWithDuration:0.2 animations:^{
            for (UIControl *view in imageViews) {
                view.alpha = 1;
            }
        }];
    }
    
    [delegate puzzleDidStart:self];
}

- (void)forceFinishAnimated:(BOOL)animated {
    state = PSPuzzleViewStateSolved;
    [self setNeedsDisplay];
}

- (void)userTapped:(UIButton *)sender {
    if (state == PSPuzzleViewStateActive) {
        if (selectedGrid == -1) {
            // User wants to select this grid!
            selectedGrid = (int)sender.tag;
            
            // Changing appearance of the grid
            [UIView animateWithDuration:ANIMATION_INTERVAL animations:^{
                [selectionBorder setAlpha:1];
            }];
            
            selectionBorder.frame = sender.frame;
        } else {
            // User wants to 1. switch 2. deselect
            UIButton *selected = [imageViews objectAtIndex:selectedGrid];
            
            if (selected != sender) {
                // Only swap if we aren't swapping what we already have
                [self swapImage:(int)selected.tag withImage:(int)sender.tag];
                [delegate userSwitched:self];
                
                if ([self checkCompleted]) {
                    // User has finished the puzzle! Play animation and notify
                    // delegate!
                    [self puzzleCompleted];
                }
            }
            
            // Changing appearance of the grid
            [UIView animateWithDuration:ANIMATION_INTERVAL animations:^{
                [selectionBorder setAlpha:0];
            }];
            
            selectedGrid = -1;
        }
    } else if (state == PSPuzzleViewStatePreview) {
        selectedGrid = (int)sender.tag;
        
        // Changing appearance of the grid
        [UIView animateWithDuration:ANIMATION_INTERVAL animations:^{
            [selectionBorder setAlpha:1];
        }];
        
        selectionBorder.frame = sender.frame;
    }
}

- (void)becomePreview {
    // Needs an image before we can preview!
    if (!puzzleImage)
        return;
    
    // Create image view containers and initialize all the arrays
    imageViews = [self createImageViews:gridNumber];
    
    images = [self dissectImage:gridNumber];
    
    order = [NSMutableArray array];
    for (int i = 0; i < images.count; i++) {
        [order addObject:[NSNumber numberWithInt:i]];
    }
    
    [self updateAllImageViews];
    
    // Make grid view top layer
    [self bringSubviewToFront:gridView];
    [self bringSubviewToFront:selectionBorder];
    
    state = PSPuzzleViewStatePreview;
    selectionBorder.alpha = 1;
    selectedGrid = [[order objectAtIndex:4] intValue];
    selectionBorder.frame = defaultSelectionFrame;
}

- (void)restartPuzzle {
    // What's the same: puzzleImage.
    [self toggleGrid:YES animated:YES];
    [self startSolvingAnimated:YES];
}

- (int)selectedGrid {
    return selectedGrid;
}

- (BOOL)isComplete {
    return state == PSPuzzleViewStateSolved;
}

@end
