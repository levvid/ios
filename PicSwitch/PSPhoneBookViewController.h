//
//  PSPhoneBookViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/25/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSPhoneBookCell.h"

@interface PSPhoneBookViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, PSPhoneBookCellDelegate,
UISearchBarDelegate>

@end
