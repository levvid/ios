//
//  PSAddFriendViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/7/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSAddFriendViewController.h"
#import <Parse/Parse.h>

@interface PSAddFriendViewController ()

@end

@implementation PSAddFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set up navigation
    [self.navigationItem setTitle:@"add friend"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"BackButton"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
    [self.navigationItem setLeftBarButtonItem:left];
    
    friendIdField.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:0.2].CGColor;
    friendIdField.layer.borderWidth = 2.0;
    friendIdField.layer.cornerRadius = 5.0;
}

- (void)back:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)addFriend:(id)sender {
    NSString *idString = friendIdField.text;
    
    // Empty string, return
    if ([idString isEqualToString:@""])
        return;
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"email" equalTo:idString];
    [query findObjectsInBackgroundWithBlock:^(NSArray *block, NSError *err){
        if (err) {
            // Handle error!
        } else {
            if ([block count] == 0) {
                [[[UIAlertView alloc] initWithTitle:@"Oops" message:@"We couldn't find anyone with that email!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            } else {
                PFRelation *relation = [[PFUser currentUser] relationForKey:@"friends"];
                [relation addObject:[block firstObject]];
                [[PFUser currentUser] saveEventually];
                [[[UIAlertView alloc] initWithTitle:@"Success!" message:@"Friend added!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    }];
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [[self view] endEditing:YES];
    return YES;
}
@end
