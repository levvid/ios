//
//  LoginViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/27/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <Parse/Parse.h>
#import "LoginViewController.h"
#import "PSScrollViewController.h"
#import "ServiceManager.h"
#import "PSAlertView.h"
#import "PSLoadingView.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    email.text = (NSString *)[[NSUserDefaults standardUserDefaults] objectForKey:@"LoginUsername"];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

/**
 Goes to the next field.
 */
- (void)nextField:(id)sender {
    if (sender == email) {
        [password becomeFirstResponder];
    } else if (sender == password) {
        [self login:sender];
    }
}

- (IBAction)login:(id)sender {
    NSString *usr = [email text];
    NSString *psd = [password text];
    
    // Checks for complete fields
    if (usr.length == 0 || psd.length == 0) {
        NSString *errMsg = @"Please enter both user name and password!";
        [[[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        [[ServiceManager sharedManager] login:usr password:psd completion:^(BOOL success, NSError *err) {
            if (!err) {
                // Do stuff after successful login.
                [[NSUserDefaults standardUserDefaults] setObject:usr forKey:@"LoginUsername"];
                // Pin user
                [[PFUser currentUser] pinInBackground];
                
                // Reset local data
                [[NSUserDefaults standardUserDefaults] setObject:@{} forKey:@"SkippedPuzzles"];
                [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"userFriends"];
                [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"incomingRequests"];
                
                [self displayHomeView];
            } else {
                NSString *errMsg = [NSString stringWithFormat:@"Incorrect log-in credentials. Please try again."];
                [[[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }];
    }
}

/**
 This is called when authentication is successful and we want to move the user
 to their home view.
 */
- (void)displayHomeView {
    PSScrollViewController *viewController = [[PSScrollViewController alloc] init];
    [[self navigationController] pushViewController:viewController animated:YES];
}


- (IBAction)dismissKeyboard:(id)sender {
    [[self view] endEditing:YES];
}


- (IBAction)backButtonPressed:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (IBAction)forgotPasswordPressed:(id)sender {
    // Display a dialogue!
    if (email.text.length > 0) {
        PSLoadingView *loadingView = [[PSLoadingView alloc] initWithTitle:@"Looking for emails..."];
        [loadingView show:YES];
        
        [PFUser requestPasswordResetForEmailInBackground:email.text block:^(BOOL succeeded, NSError *err) {
            [loadingView dismiss:YES completion:^{
                NSString *str;
                if (succeeded) {
                    str = @"Recovery success! Please check your inbox for the link to recover email.";
                } else {
                    str = @"Oops! Unable to find that email! Please check your email and try again!";
                }
                
                [[[PSAlertView alloc] initWithMessage:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }];
        }];
    } else {
        NSString *str = @"Please enter your email above, and we will send a password reset link to your email!";
        [[[PSAlertView alloc] initWithMessage:str delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self nextField:textField];
    
    return YES;
}

// iOS 7 workaround
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        UIFont *fontMain = [UIFont fontWithName:@"Montserrat-Bold" size:14];
        UIFont *fontPh = [UIFont fontWithName:@"Quicksand-Italic" size:14];
        
        if ([[[textField text] stringByReplacingCharactersInRange:range withString:string] isEqualToString:@""]) {
            [textField setFont:fontPh];
        } else {
            [textField setFont:fontMain];
        }
    }
    
    return YES;
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
