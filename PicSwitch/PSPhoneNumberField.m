//
//  PSPhoneNumberField.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/3/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSPhoneNumberField.h"
#import "RMPhoneFormat.h"

@interface PSPhoneNumberField ()
{
    // This is the digits the user actually typed
    NSString *digitList;
    
    // The number formatter
    RMPhoneFormat *formatter;
    
    // Used to break infinite loop caused by changing selected range within
    // delegate method.
    BOOL bypass;
    
    // Is number regex
    NSRegularExpression *isNumber;
}

@end

@implementation PSPhoneNumberField
@synthesize controlDelegate;

- (id)init {
    self = [super init];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)awakeFromNib {
    [self setup];
}

- (void)setup {
    NSLog(@"Setting up");
    formatter = [[RMPhoneFormat alloc] init];
    
    /*
    // Generating default country code for user
    digitList = [formatter defaultCallingCode];
     */
    // Enforcing US numbers
    digitList = @"1";
    
    [self updatePhoneNumber];
    
    bypass = false;
    
    isNumber = [NSRegularExpression
                regularExpressionWithPattern:@"[0-9]"
                options:NSRegularExpressionCaseInsensitive
                error:nil];
    self.textContainerInset = UIEdgeInsetsMake(10, 5, 10, 5);
    
    // Creating toolbar
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem *next = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextPressed:)];
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(donePressed:)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    toolbar.items = @[next, space, done];
    
    self.inputAccessoryView = toolbar;
}

- (void)nextPressed:(id)sender {
    [controlDelegate nextPressedPhoneNumberField:self];
}

- (void)donePressed:(id)sender {
    [controlDelegate donePressedPhoneNumberField:self];
}

// Uses current digit list to generate a formatted string and output it to text.
- (void)updatePhoneNumber {
    // Add a + in front
    NSString *appended = [NSString stringWithFormat:@"+%@", digitList];
    NSString *formatted = [formatter format:appended];
    self.text = formatted;
}

// Find the corresponding range for user input
- (NSRange)rangeOfDigitListForDisplayRange:(NSRange)range {
    // We look at the first position and count the number of digits that comes
    // before the string to know where we are
    NSInteger id1 = [isNumber numberOfMatchesInString:self.text options:0 range:NSMakeRange(0, range.location)];
    NSInteger id2 = [isNumber numberOfMatchesInString:self.text options:0 range:range];

    return NSMakeRange(id1, id2);
}

// Find the corresponding display range for digit list
- (NSRange)rangeOfDisplayForDigitListRange:(NSRange)range {
    // We count the number of digits from the start of the text until we reach
    // the correct number
    
    // Precondition: there must be same number of digits in self.text and
    // digitList.
    int index = 0;
    for (int i = 0; i < range.location; index++) {
        // Check whether ith character is a number
        BOOL isNum = [isNumber numberOfMatchesInString:self.text options:0 range:NSMakeRange(index, 1)];
        
        if (isNum) {
            i++;
        }
    }
    
    index = MAX(1, index);
    
    // Now work out the length.
    int length = 0;
    for (NSInteger i = range.location; i < range.location + range.length; length++) {
        // Check whether ith character is a number
        BOOL isNum = [isNumber numberOfMatchesInString:self.text options:0 range:NSMakeRange(length, 1)];
        
        if (isNum) {
            i++;
        }
    }
    
    return NSMakeRange(index, length);
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChangeSelection:(UITextView *)textView {
    if (bypass)
        return;
    
    // Override selection if range is 0
    if (textView.selectedRange.length == 0) {
        NSInteger position = 1;
        // Find the first number on the left of this carat and set that as the
        // selection point
        for (NSInteger i = textView.selectedRange.location - 1; i > 0; i--) {
            NSInteger isDigit = [isNumber numberOfMatchesInString:textView.text options:0 range:NSMakeRange(i, 1)];
            
            if (isDigit) {
                // We are done!
                position = MAX(1, i + 1);
                break;
            }
        }
        
        // Stopping infinite loop
        bypass = YES;
        self.selectedRange = NSMakeRange(position, 0);
        bypass = NO;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    // Determine where should we insert the digit into our digit list
    NSRange listRange = [self rangeOfDigitListForDisplayRange:self.selectedRange];
    
    // This is the range for digit list after the text operation
    NSRange digitRange;
    
    // Checking whether user has pressed backspace, and isn't selecting anything
    // and we need to delete something
    BOOL deleteWithBackspace = [text isEqualToString:@""] && listRange.length == 0 && listRange.location > 0;
    
    if (deleteWithBackspace) {
        digitList = [NSString stringWithFormat:@"%@%@", [digitList substringToIndex:listRange.location - 1], [digitList substringFromIndex:listRange.location]];
        digitRange = NSMakeRange(listRange.location - 1, 0);
    } else {
        // Filter the replacement text to only digits
        NSString *filteredText = [[text componentsSeparatedByCharactersInSet:
                                   [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                  componentsJoinedByString:@""];
        
        // Modify the digits
        digitList = [digitList stringByReplacingCharactersInRange:listRange withString:filteredText];
        
        NSInteger loc = MIN(22, listRange.location + filteredText.length);
        
        digitRange = NSMakeRange(loc, 0);
    }
    
    // Trimming the digit list to only hold at most 22 digits
    digitList = [digitList substringToIndex:MIN(22, digitList.length)];
    
    // Update display
    [self updatePhoneNumber];
    
    // Update selection
    self.selectedRange = [self rangeOfDisplayForDigitListRange:digitRange];
    
    return NO;
}

- (BOOL)isValid {
    return [formatter isPhoneNumberValid:[NSString stringWithFormat:@"+%@", digitList]];
}

- (NSString *)phoneNumber {
    return digitList;
}

@end
