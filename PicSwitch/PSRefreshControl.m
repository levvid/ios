//
//  PSRefreshControl.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/10/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSRefreshControl.h"

typedef enum {
    PSRefreshControlStateIdle,
    PSRefreshControlStateRefreshing,
    PSRefreshControlStateResetting
} PSRefreshControlState;

@interface PSRefreshControl()
{
    PSRefreshControlState state;
    
    // TEMP!
    UILabel *textLabel;
}


@end

@implementation PSRefreshControl
@synthesize delegate;

#pragma mark - Constants
float const ANIMATION_DURATION = 0.2;
CGFloat const HEIGHT = 70.0;

#pragma mark - Body
- (id)init {
    self = [super init];
    
    if (self) {
        CGFloat w = [UIScreen mainScreen].bounds.size.width;
        CGFloat h = HEIGHT;
        self.frame = CGRectMake(0, -HEIGHT, w, h);
        
        [self setup];
    }
    
    return self;
}

// This set up the appearance of the control
- (void)setup {
    self.clipsToBounds = YES;
    UIView *botShadow = [[UIView alloc] initWithFrame:CGRectMake(-10, HEIGHT, self.bounds.size.width + 20, 10)];
    botShadow.backgroundColor = [UIColor whiteColor];
    botShadow.layer.shadowOffset = CGSizeMake(0, -3);
    botShadow.layer.shadowColor = [UIColor blackColor].CGColor;
    botShadow.layer.shadowOpacity = 0.3;
    botShadow.layer.shadowRadius = 2.0;
    [self addSubview:botShadow];
    
    // TEMP!
    textLabel = [[UILabel alloc] initWithFrame:self.bounds];
    textLabel.text = @"keep pulling to refresh...";
    textLabel.textColor = [UIColor whiteColor];
    textLabel.font = [UIFont fontWithName:@"Quicksand-Italic" size:14];
    textLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:textLabel];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (state == PSRefreshControlStateIdle) {
        if ([self didUserScrollFarEnoughToTriggerRefresh:scrollView]) {
            // TEMP!
            textLabel.text = @"- release to refresh -";
        } else {
            textLabel.text = @"keep pulling to refresh...";
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView {
    if (state == PSRefreshControlStateIdle &&
        [self didUserScrollFarEnoughToTriggerRefresh:scrollView]) {
        [delegate refreshControlTriggered:self];
        [self beginLoadingAnimated:YES scrollView:scrollView];
    }
}

- (void)beginLoadingAnimated:(BOOL)animated scrollView:(UIScrollView *)scrollView {
    if (state !=PSRefreshControlStateRefreshing) {
        state = PSRefreshControlStateRefreshing;
        
        // TEMP!
        textLabel.text = @"refreshing...";
        
        [self scrollRefreshControlToVisibleAnimated:animated scrollView:scrollView];
    }
}

- (BOOL)didUserScrollFarEnoughToTriggerRefresh:(UIScrollView *)scrollView {
    return (-[self distanceScrolled:scrollView] > HEIGHT);
}

- (CGFloat)distanceScrolled:(UIScrollView *)scrollView {
    return scrollView.contentOffset.y + scrollView.contentInset.top;
}

// Scrolls the refresh control to visible area and fix the new inset
- (void)scrollRefreshControlToVisibleAnimated:(BOOL)animated
                                   scrollView:(UIScrollView *)scrollView {
    CGFloat animationDuration = animated? ANIMATION_DURATION : 0.0f;
    
    // This works with iOS 8...
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        UIEdgeInsets newInsets = scrollView.contentInset;
        newInsets.top = HEIGHT;
        CGPoint contentOffset = scrollView.contentOffset;
        
        [UIView animateWithDuration:animationDuration animations:^(void) {
            scrollView.contentInset = newInsets;
            scrollView.contentOffset = contentOffset;
        }];
    } else {
        UIEdgeInsets newInsets = scrollView.contentInset;
        newInsets.top = HEIGHT;
        CGPoint contentOffset = scrollView.contentOffset;
        
        [UIView animateWithDuration:animationDuration animations:^(void) {
            scrollView.contentInset = newInsets;
        }];
        
        CGFloat h = [UIScreen mainScreen].bounds.size.height;
        CGFloat w = [UIScreen mainScreen].bounds.size.width;
        
        CGRect rect = CGRectMake(contentOffset.x, contentOffset.y, w, h);
        
        // Content offset isn't animatable by UIView in iOS 7.
        [scrollView scrollRectToVisible:rect animated:YES];
    }
}

- (void)resetRefreshControlAnimated:(BOOL)isAnimated
                         scrollView:(UIScrollView *)scrollView {
    if (state != PSRefreshControlStateRefreshing) {
        return;
    }
    
    state = PSRefreshControlStateResetting;
    UIEdgeInsets newInsets = scrollView.contentInset;
    newInsets.top = 0;
    
    [UIView animateWithDuration:isAnimated? ANIMATION_DURATION : 0 animations:^(void){
        scrollView.contentInset = newInsets;
    } completion:^(BOOL finished){
         state = PSRefreshControlStateIdle;
    }];
}

- (void)resetRefreshControlWithCompletionBlock:(void (^)(BOOL))block
                                    scrollView:(UIScrollView *)scrollView {
    if (state != PSRefreshControlStateRefreshing) {
        block(NO);
        
        return;
    }
    
    state = PSRefreshControlStateResetting;
    UIEdgeInsets newInsets = scrollView.contentInset;
    newInsets.top = 0;
    
    [UIView animateWithDuration:ANIMATION_DURATION animations:^(void){
        scrollView.contentInset = newInsets;
    } completion:^(BOOL finished){
        state = PSRefreshControlStateIdle;
        block(finished);
    }];
}

@end
