//
//  PSExtraPuzzleViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/11/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSExtraPuzzleViewController.h"
#import "PSPuzzleViewController.h"
#import "PSRefreshControl.h"
#import "ServiceManager.h"
#import "PSPuzzleCell.h"
#import <Parse/Parse.h>
#import "PSScrollViewController.h"

@interface PSExtraPuzzleViewController ()
<UITableViewDataSource, UITableViewDelegate, PSScrollViewControllerProtocol,
UIScrollViewDelegate, PSRefreshControlDelegate, PSPuzzleViewControllerDelegate>
{
    IBOutlet UITableView *contentTable;
    
    NSIndexPath *lastPuzzle;
    
    PSRefreshControl *refreshControl;
    
    NSArray *categories;
    
    // Navigation buttons
    UIBarButtonItem *right;
    
    UILabel *titleView;
}
@end

@implementation PSExtraPuzzleViewController

- (void)viewDidBecomeActive {
    [UIView animateWithDuration:0.15 animations:^{
        self.navigationItem.titleView.alpha = 0;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = titleView;
        [UIView animateWithDuration:0.15 animations:^{
            titleView.alpha = 1;
        }];
    }];
    
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    [self.navigationItem setRightBarButtonItem:right animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setting up the navigation controller
    titleView = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 160, 44)];
    [titleView setText:@"additional puzzles"];
    [titleView setFont:[UIFont fontWithName:@"Montserrat-Regular" size:21]];
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    
    UIView *forward = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *forwardButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, 40, 40)];
    [forwardButton setImage:[UIImage imageNamed:@"ForwardButton"] forState:UIControlStateNormal];
    [forward addSubview:forwardButton];
    [forwardButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    right = [[UIBarButtonItem alloc] initWithCustomView:forward];
    
    // Setting up the table view
    UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, -1000, 320, 1000)];
    bg.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
    [contentTable addSubview:bg];
    
    // Setting up refresh control
    refreshControl = [[PSRefreshControl alloc] init];
    refreshControl.delegate = self;
    [contentTable addSubview:refreshControl];
    
    // Getting data
    [self refreshContent];
}

- (UINavigationController *)navigationController {
    return [self parentViewController].navigationController;
}

- (UINavigationItem *)navigationItem {
    return [self parentViewController].navigationItem;
}

- (void)back:(id)sender {
    [(PSScrollViewController *)[self parentViewController] scrollRight];
}

- (void)refreshContent {
    // Fetch all categories
    [[ServiceManager sharedManager] fetchExtraCategoriesWithCallback:^(NSArray *array, NSError *err) {
        [refreshControl resetRefreshControlWithCompletionBlock:^(BOOL finished){
            if (!err) {
                categories = array;
                [contentTable reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, [self numberOfSectionsInTableView:contentTable])] withRowAnimation:UITableViewRowAnimationAutomatic];
            } else {
                NSLog(@"Fetching categories failed: %@", [err localizedDescription]);
            }
        } scrollView:contentTable];
    }];
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [categories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPuzzleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PSPuzzleCellReuseIdentifier"];
    
    // Just prevent them from deleting it...
    [cell setType:PuzzleCellTypeFeatured];
    
    // Set cell color
    [cell setColorStyle:indexPath.row % 2];
    
    PFObject *cat = [categories objectAtIndex:indexPath.row];
    
    // Set up text
    NSString *firstCaption = cat[@"categoryDescription"];
    
    if (!firstCaption) {
        firstCaption = @"";
    }
    
    [cell setMessage:firstCaption answer:@""];
    
    // Set up image
    PFFile *imageFile = cat[@"photo"];
    [cell setFile:imageFile];
    
    // Set up time
    [cell setTime:@""];
    // [cell setTime:[NSString stringWithFormat:@"Completed %d/%d in series 1", 1, 100]];
    
    // Set up sender name
    [cell setSender:cat[@"categoryName"]];
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPuzzleViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PVC"];
    viewController.delegate = self;
    viewController.puzzleSource = PuzzleSourceAdditional;
    PFObject *cat = [categories objectAtIndex:indexPath.row];
    NSString *name = cat[@"categoryName"];
    [viewController setTitle:name];
    
    // Fetching puzzle
    PFQuery *query = [self queryForIndexPath:indexPath];
    [viewController setQuery:query];
    lastPuzzle = indexPath;
    
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, height - 10)];
    label.text = @"choose a category";
    
    label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
    label.textColor = [UIColor whiteColor];
    
    [container addSubview:label];
    
    return container;
}

#pragma mark - PSPuzzleViewControllerDelegate
- (PFQuery *)queryForIndexPath:(NSIndexPath *)indexPath {
    PFObject *cat = [categories objectAtIndex:indexPath.row];
    NSString *name = cat[@"puzzleClass"];
    
    PFQuery *query = [PFQuery queryWithClassName:name];
    NSArray *completed = [NSArray arrayWithArray:[PFUser currentUser][@"puzzlesSolved"]];
    NSArray *skipped = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SkippedPuzzles"] objectForKey:name];
    [query whereKey:@"objectId" notContainedIn:[completed arrayByAddingObjectsFromArray:skipped]];
    [query addAscendingOrder:@"createdAt"];
    
    return query;
}


#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


/**
 This converts a date to a string of relative time
 */
- (NSString *)dateDiff:(NSDate *)date
{
    NSDate *todayDate = [NSDate date];
    double ti = [date timeIntervalSinceDate:todayDate];
    ti = ti * -1;
    
    if (ti < 0) {
        return @"Your system time is messed up!";
    } else if (ti < 120) {
        return @"Added a minute ago";
    } else if (ti < 3600) {
        int diff = (int)(ti / 60);
        return [NSString stringWithFormat:@"Added %d minutes ago", diff];
    } else if (ti < 86400) {
        int diff = (int)(ti / 3600);
        if (diff == 1) {
            return [NSString stringWithFormat:@"Added an hour ago"];
        } else {
            return [NSString stringWithFormat:@"Added %d hours ago", diff];
        }
    } else if (ti < 604800) {
        int diff = (int)(ti / 86400);
        if (diff == 1) {
            return [NSString stringWithFormat:@"Added a day ago"];
        } else {
            return [NSString stringWithFormat:@"Added %d days ago", diff];
        }
    } else if (ti < 2419200) {
        int diff = (int)(ti / 604800);
        if (diff == 1) {
            return [NSString stringWithFormat:@"Added a week ago"];
        } else {
            return [NSString stringWithFormat:@"Added %d weeks ago", diff];
        }
    } else {
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"MMM yyyy"];
        return [NSString stringWithFormat:@"Added %@", [df stringFromDate:date]];
    }
}

#pragma mark - UIScrollViewDelegate
// These help refresh control by letting it know when the view is scrolled
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [refreshControl scrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [refreshControl scrollViewDidEndDragging:scrollView];
}

#pragma mark - PSRefreshControlDelegate
- (void)refreshControlTriggered:(PSRefreshControl *)control {
    [self refreshContent];
}

#pragma mark - PSPuzzleViewControllerDelegate
- (PFQuery *)nextQuery {
    NSIndexPath *nextIndexPath = lastPuzzle;
    lastPuzzle = nextIndexPath;
    
    return [self queryForIndexPath:nextIndexPath];
}

@end
