//
//  AppDelegate.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "RMPhoneFormat.h"
#import "PSNotificationView.h"
#import <AudioToolbox/AudioToolbox.h>


@interface AppDelegate ()
{
    SystemSoundID alertSound;
}
@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Parse set up
    // [Optional] Power your app with Local Datastore. For more info, go to
    // https://parse.com/docs/ios_guide#localdatastore/iOS
    [Parse enableLocalDatastore];
    
    // Initialize Parse.
    [Parse setApplicationId:@"T0mfW1Uz1wCR2l4xZBipnt36S4svUDrMfunBHcDA"
                  clientKey:@"zTz4VHrKcOAjtPbVhLCpy7bif6IZSF295FCe9UMu"];
    [PFImageView class];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Push notifications
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [application registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        [application registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    // Initializing
    NSDictionary *dict = @{@"SkippedPuzzles" : @{}, @"userFriends" : @[],
                           @"incomingRequests" : @[], @"userCompleted" : @{},
                           @"shouldMuteSound" : @NO,
                           @"RecentContacts" : @{},
                           @"SolveStats" : @{},
                           @"showMenuAlert" : @YES,
                           @"RecentContactsCheck" : [NSDate date]};
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:dict];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[ @"global" ];
    [currentInstallation saveEventually];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // Handles notification in app
    NSString *message;
    NSDictionary *dict = [userInfo objectForKey:@"aps"];
    
    id alert = [dict objectForKey:@"alert"];
    
    if ([alert isKindOfClass:[NSString class]]) {
        message = alert;
    } else {
        message = [dict objectForKey:@"body"];
    }
    
    [[[PSNotificationView alloc] initWithMessage:message] show];
    
    // Play sound
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"shouldMuteSound"] boolValue]) {
        NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"solved" ofType:@"wav"];
        NSURL *soundURL = [NSURL fileURLWithPath:soundPath];
        AudioServicesCreateSystemSoundID((__bridge CFURLRef)soundURL, &alertSound);
        AudioServicesPlaySystemSound(alertSound);
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationReceived" object:self];
}


@end
