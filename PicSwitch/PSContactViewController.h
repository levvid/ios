//
//  PSContactViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSContactCell.h"
#import "PSAlertView.h"

typedef enum : NSUInteger {
    // Use this when we want to send a picswitch
    ContactViewControllerModeSend,
    // Use this when we want to dsplay someone's contacts.
    ContactViewControllerModeView,
} ContactViewControllerMode;

@class PFObject;
@interface PSContactViewController : UIViewController
<UITableViewDataSource, UITableViewDelegate, PSContactCellDelegate, PSAlertViewDelegate>
{

}

- (IBAction)sendPicswitch:(id)sender;
- (IBAction)reportUser:(id)sender;
- (IBAction)acceptRequest:(id)sender;
- (IBAction)declineRequest:(id)sender;
- (IBAction)newMessage:(id)sender;
- (IBAction)deleteFriend:(id)sender;

- (void)assignPicswitch:(PFObject *)picswitch;

- (void)setMode:(ContactViewControllerMode)mode;

// This lets the controller know that when we pop, we need to pop two screens.
- (void)isReply;

@property (strong, nonatomic) NSArray *recipients;

@end
