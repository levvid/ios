//
//  PSPuzzleViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/9/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSPuzzleViewController.h"
#import "PSPuzzleView.h"
#import <Parse/Parse.h>
#import "ServiceManager.h"
#import "CameraViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface PSPuzzleViewController ()
<PSAlertViewDelegate>
{
    IBOutlet UILabel *firstCaptionLabel;
    IBOutlet UILabel *secondCaptionLabel;
    IBOutlet UIButton *nextButton;
    IBOutlet UILabel *switchesLabel;
    IBOutlet UILabel *secondsLabel;
    IBOutlet NSLayoutConstraint *statConstraint;
    IBOutlet NSLayoutConstraint *replyConstraint;
    IBOutlet PSPuzzleView *puzzleView;
    IBOutlet UIImageView *solvedPreviewImageView;
    
    BOOL shouldLoadPuzzleAfterViewLoaded;
    
    UIBarButtonItem *timerBarButton;
    UIButton *timerButton;
    
    UIBarButtonItem *restartBarButton;
    UIBarButtonItem *moreInfoBarButton;
    UILabel *titleLabel;
    
    NSTimer *puzzleTimer;
    NSDate *startTime;
    NSDate *endTime;
    BOOL isCountdown;
    int timeLimit;
    
    NSString *navTitle;
    
    int switchCount;
}

@end

@implementation PSPuzzleViewController
@synthesize puzzleSource, puzzle, delegate;

- (void)setPuzzleSource:(PuzzleSource)ps {
    puzzleSource = ps;
}

- (void)setTitle:(NSString *)title {
    titleLabel.text = title;
    navTitle = title;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Set up navigation
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 140, 50)];
    titleLabel.numberOfLines = 2;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"Montserrat-Regular" size:22];
    titleLabel.adjustsFontSizeToFitWidth = YES;
    titleLabel.minimumScaleFactor = 0.8;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = navTitle;
    self.navigationItem.titleView = titleLabel;
    
    // Set up bar buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    UIColor *green = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    timerButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 6, 28, 28)];
    timerButton.layer.cornerRadius = 2.0;
    timerButton.backgroundColor = [UIColor whiteColor];
    timerButton.titleLabel.font = [UIFont fontWithName:@"VarelaRound-Regular" size:12];
    timerButton.titleLabel.minimumScaleFactor = 0.5;
    timerButton.adjustsImageWhenHighlighted = NO;
    [timerButton setTitle:[NSString stringWithFormat:@"%i", 0] forState:UIControlStateNormal];
    [timerButton setTitleColor:green forState:UIControlStateNormal];
    [container addSubview:timerButton];
    timerBarButton = [[UIBarButtonItem alloc] initWithCustomView:container];
    
    [self.navigationItem setRightBarButtonItem:timerBarButton];
    
    UIView *restartView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *restartButton = [[UIButton alloc] initWithFrame:CGRectMake(2, 2, 36, 36)];
    [restartButton setImage:[UIImage imageNamed:@"RestartButton"] forState:UIControlStateNormal];
    [restartView addSubview:restartButton];
    [restartButton addTarget:self action:@selector(restartPuzzle) forControlEvents:UIControlEventTouchUpInside];
    restartBarButton = [[UIBarButtonItem alloc] initWithCustomView:restartView];
    
    UIView *infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *infoButton = [[UIButton alloc] initWithFrame:CGRectMake(10, 6, 28, 28)];
    infoButton.layer.cornerRadius = 2.0;
    infoButton.backgroundColor = [UIColor whiteColor];
    [infoButton setTitle:@"i" forState:UIControlStateNormal];
    [[infoButton titleLabel] setFont:[UIFont fontWithName:@"HoeflerText-BlackItalic" size:16]];
    [infoButton setTitleColor:green forState:UIControlStateNormal];
    [infoView addSubview:infoButton];
    [infoButton addTarget:self action:@selector(moreInfo) forControlEvents:UIControlEventTouchUpInside];
    moreInfoBarButton = [[UIBarButtonItem alloc] initWithCustomView:infoView];
    
    // Set up puzzle view
    if (shouldLoadPuzzleAfterViewLoaded) {
        [self loadGUI];
    }
    
    puzzleView.delegate = self;
    
    // Set up reply buttons
    statConstraint.constant = -110;
    replyConstraint.constant = 0;
    
    if ([puzzle.parseClassName isEqualToString:@"Messages"]) {
        statConstraint.constant = -190;
        replyConstraint.constant = 80;
    }
    
    [nextButton setTitle:@"SKIP THIS PICSWITCH" forState:UIControlStateNormal];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [puzzleTimer invalidate];
}

- (void)setPuzzle:(PFObject *)p {
    puzzle = p;
    timeLimit = [puzzle[@"timeLimit"] intValue];
    isCountdown = timeLimit != 0;
    
    if (!firstCaptionLabel) {
        // The view just got loaded! We wait until view finishes loading...
        shouldLoadPuzzleAfterViewLoaded = YES;
    } else {
        [self loadGUI];
    }
    
    // Setting title if we are solving another featured or new message.
    // If we are solving additional, then the name should have been set already
    // as category name isn't in the data and should only need to be set once.
    NSString *baseName;
    
    if (puzzleSource != PuzzleSourceAdditional) {
        if ([p.parseClassName isEqualToString:@"FeaturedPuzzles"]) {
            baseName = p[@"companyName"];
        } else {
            baseName = p[@"sender"][@"firstName"];
        }
        
        NSString *title = [NSString stringWithFormat:@"%@", baseName];
        [self setTitle:title];
    }
}

- (void)setQuery:(PFQuery *)q {
    // This executes the query and loads everything after the result becomes
    // available
    [q getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            [self setPuzzle:object];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
            [[[PSAlertView alloc] initWithMessage:@"You finished all the puzzles in this category! Check back for later - we update weekly!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

// This loads the puzzle into the current view.
- (void)loadGUI {
    firstCaptionLabel.text = puzzle[@"firstCaption"];
    secondCaptionLabel.text = puzzle[@"secondCaption"];
    NSString *title = [NSString stringWithFormat:@"%i", timeLimit];
    [timerButton setTitle:title forState:UIControlStateNormal];
    
    
    PFFile *imageFile = puzzle[@"photo"];
    [imageFile getDataInBackgroundWithBlock:^(NSData *imageData, NSError *error) {
        if (!error) {
            UIImage *image = [UIImage imageWithData:imageData];
            [puzzleView setPuzzleImage:image];
            
            // We only want to start solving if we aren't in solved section
            if (puzzleSource != PuzzleSourceSolved) {
                [puzzleView startSolvingAnimated:YES];
            } else {
                // We cover the puzzle source with the destination image.
                solvedPreviewImageView.image = image;
                [UIView animateWithDuration:0.2 animations:^{
                    solvedPreviewImageView.alpha = 1;
                }];
                
                // We also hide the timer button and replace with more info if
                // its featured
                
                if ([puzzle.parseClassName isEqualToString:@"FeaturedPuzzles"]) {
                    self.navigationItem.rightBarButtonItem = moreInfoBarButton;
                } else {
                    self.navigationItem.rightBarButtonItem = nil;
                }
                
                // Finally We modify the bottom bar. We show the already solved
                // stats
                secondCaptionLabel.alpha = 1;
                [self showSolveStats:YES animated:NO];
                
                NSDictionary *dict1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"SolveStats"];
                
                if (dict1) {
                    NSDictionary *dict2 = [dict1 objectForKey:[PFUser currentUser].objectId];
                    
                    if (dict2) {
                        NSArray *stats = [dict2 objectForKey:puzzle.objectId];
                        
                        if (stats) {
                            switchesLabel.text = [NSString stringWithFormat:@"%i SWITCHES", [[stats objectAtIndex:0] intValue]];
                            secondsLabel.text = [NSString stringWithFormat:@"%.02f SECONDS", [[stats objectAtIndex:1] floatValue]];
                        }
                    }
                }
            }
        } else {
            NSString *errString = [NSString stringWithFormat:@"Unable to grab the image of this picswitch! Please try again! %@", [error localizedDescription]];
            [[[PSAlertView alloc] initWithMessage:errString delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
    
    [[self view] layoutIfNeeded];
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateTime {
    int secs = (int)round(-[startTime timeIntervalSinceNow]);
    
    if (isCountdown) {
        int timeLeft = timeLimit - secs;
        NSString *str = [NSString stringWithFormat:@"%i", timeLeft];
        [timerButton setTitle:str forState:UIControlStateNormal];
        
        if (timeLeft <= 0) {
            // Display alert!
            [self playSound:@"timeout" fileType:@"mp3"];
            [[[PSAlertView alloc] initWithMessage:@"Time's up!" delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:@"Skip", nil] show];
            [puzzleTimer invalidate];
        }
    } else {
        NSString *str = [NSString stringWithFormat:@"%i", secs];
        [timerButton setTitle:str forState:UIControlStateNormal];
    }
}

- (void)restartPuzzle {
    NSString *title = [NSString stringWithFormat:@"%i", timeLimit];
    [timerButton setTitle:title forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = timerBarButton;
    [self showSolveStats:NO animated:YES];
    [puzzleView restartPuzzle];
    [nextButton setTitle:@"SKIP THIS PICSWITCH" forState:UIControlStateNormal];
}

- (void)showSolveStats:(BOOL)shouldShow animated:(BOOL)animated {
    [[self view] layoutIfNeeded];
    
    NSString *title = @"NEXT PICSWITCH";
    
    float value = -110;
    
    if ([puzzle.parseClassName isEqualToString:@"Messages"]) {
        value = -190;
        title = @"NEXT";
    }
    
    [nextButton setTitle:shouldShow? title : @"SKIP THIS PICSWITCH" forState:UIControlStateNormal];
    
    statConstraint.constant = shouldShow? 0 : value;
    
    [UIView animateWithDuration:animated? 0.2 : 0 animations:^{
        [[self view] layoutIfNeeded];
    }];
}

- (IBAction)nextPuzzle:(id)sender {
    // Invalidate timer if there is one!
    if (puzzleTimer)
        [puzzleTimer invalidate];
    
    if (puzzleSource != PuzzleSourceAdditional) {
        PFObject *next = [delegate nextPuzzle];
        
        if (next) {
            [self setPuzzle:next];
            [puzzleView toggleGrid:YES animated:YES];
            [self showSolveStats:NO animated:YES];
            self.navigationItem.rightBarButtonItem = timerBarButton;
            NSString *title = [NSString stringWithFormat:@"%i", timeLimit];
            [timerButton setTitle:title forState:UIControlStateNormal];
            
            [UIView animateWithDuration:0.2 animations:^{
                secondCaptionLabel.alpha = 0;
            }];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
            [[[PSAlertView alloc] initWithMessage:@"You finished all the puzzles!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    } else {
        // Check if we skipped this puzzle
        if (![puzzleView isComplete]) {
            // We skipped.
            NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"SkippedPuzzles"]];
            NSMutableArray *array = [NSMutableArray arrayWithArray:[temp objectForKey:puzzle.parseClassName]];
            
            if (!array) {
                array = [NSMutableArray array];
            }
            
            [array addObject:puzzle.objectId];
            [temp setObject:array forKey:puzzle.parseClassName];
            
            [[NSUserDefaults standardUserDefaults] setObject:temp forKey:@"SkippedPuzzles"];
        }
        
        PFQuery *next = [delegate nextQuery];
        firstCaptionLabel.text = @"Loading...";
        
        [next getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (!error) {
                [self setPuzzle:object];
                [puzzleView toggleGrid:YES animated:YES];
                [self showSolveStats:NO animated:YES];
                self.navigationItem.rightBarButtonItem = timerBarButton;
                NSString *title = [NSString stringWithFormat:@"%i", timeLimit];
                [timerButton setTitle:title forState:UIControlStateNormal];
                
                [UIView animateWithDuration:0.2 animations:^{
                    secondCaptionLabel.alpha = 0;
                }];
            } else {
                // Maybe we skipped in previous sections.
                NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"SkippedPuzzles"]];
                NSArray *array = [temp objectForKey:puzzle.parseClassName];
                
                if (array && array.count > 1) {
                    // clear array and try again
                    [temp setObject:@[] forKey:puzzle.parseClassName];
                    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:@"SkippedPuzzles"];
                    [self nextPuzzle:nil];
                } else if (array && array.count == 0) {
                    // User attempts to skip the last puzzle of the set!
                    [temp setObject:@[] forKey:puzzle.parseClassName];
                    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:@"SkippedPuzzles"];
                    [[[PSAlertView alloc] initWithMessage:@"This is the last puzzle of the set!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                    [[[PSAlertView alloc] initWithMessage:@"You finished all the puzzles!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }
        }];
    }
}

#pragma mark - PSPuzzleViewDelegate
- (void)userCompleted:(PSPuzzleView *)pv {
    [self playSound:@"solved" fileType:@"mp3"];
    
    [UIView animateWithDuration:0.2 animations:^{
        secondCaptionLabel.alpha = 1;
    }];
    
    // End the timer
    [puzzleTimer invalidate];
    endTime = [NSDate date];
    float time = -[startTime timeIntervalSinceNow];
    
    if (puzzleSource == PuzzleSourceFeatured) {
        if ([puzzle[@"sponsorLink"] length] > 0)
            self.navigationItem.rightBarButtonItem = moreInfoBarButton;
        else {
            self.navigationItem.rightBarButtonItem = nil;
        }
    } else {
        self.navigationItem.rightBarButtonItem = restartBarButton;
    }
    
    // Animating data slide in
    [self showSolveStats:YES animated:YES];
    
    switchesLabel.text = [NSString stringWithFormat:@"%i SWITCHES", switchCount];
    secondsLabel.text = [NSString stringWithFormat:@"%.02f SECONDS", time];
    
    // Save this data so we can retrieve later
    NSMutableDictionary *dict1 = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"SolveStats"]];
    NSMutableDictionary *dict2 = [NSMutableDictionary dictionaryWithDictionary:[dict1 objectForKey:[PFUser currentUser].objectId]];
    [dict2 setObject:@[[NSNumber numberWithInt:switchCount], [NSNumber numberWithFloat:time], [NSDate date]] forKey:puzzle.objectId];
    [dict1 setObject:dict2 forKey:[PFUser currentUser].objectId];
    [[NSUserDefaults standardUserDefaults] setObject:dict1 forKey:@"SolveStats"];
    
    // Send data to server
    switch (puzzleSource) {
        case PuzzleSourceFriend:
            [[ServiceManager sharedManager] userPuzzleCompleted:puzzle
                                                       switches:switchCount
                                                           time:time];
            break;
        case PuzzleSourceAdditional:
            [[ServiceManager sharedManager] extraPuzzleCompleted:puzzle
                                                        switches:switchCount
                                                            time:time];
            break;
        case PuzzleSourceFeatured:
            [[ServiceManager sharedManager] featuredPuzzleCompleted:puzzle
                                                           switches:switchCount
                                                               time:time];
            break;
        case PuzzleSourceSolved:
            // Do nothing as it's already solved.
            break;
    }
}

- (void)moreInfo {
    // We should link this to a link advertisers provided
    NSString *link = puzzle[@"sponsorLink"];
    NSURL *url = [NSURL URLWithString:link];
    [[UIApplication sharedApplication] openURL:url];
}

- (IBAction)reply:(id)sender {
    // Open the create camera view and save this contact as selected
    NSArray *selected = @[puzzle[@"sender"]];
    
    CameraViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CVC"];
    [viewController setRecipients:selected];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)userSwitched:(PSPuzzleView *)puzzleView {
    switchCount++;
    [self playSound:@"switch" fileType:@"mp3"];
}

- (void)puzzleDidStart:(PSPuzzleView *)puzzpleView {
    [self playSound:@"next" fileType:@"mp3"];
    switchCount = 0;
    startTime = [NSDate date];
    puzzleTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
                                                 selector:@selector(updateTime)
                                                 userInfo:nil repeats:YES];
}

#pragma mark - PSAlertViewDelegate
- (void)alertView:(PSAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        // User wants to retry
        [self restartPuzzle];
    } else {
        [self nextPuzzle:alertView];
    }
}

#pragma mark - Sound playing
- (void)playSound:(NSString *)soundName fileType:(NSString *)fileType {
    // Checking whether user muted sound
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"shouldMuteSound"] boolValue])
        return;
    
    NSError *error;
    NSString *path = [[NSBundle mainBundle] pathForResource:soundName ofType:fileType];
    NSURL *url = [NSURL URLWithString:path];
    AVAudioPlayer *p = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [p play];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)([p duration] * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [p stop];
    });
}

@end
