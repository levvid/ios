//
//  PSScrollViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 6/1/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PSScrollViewControllerProtocol

@optional

// This tells the contained controller that transition to its view just ended.
- (void)viewDidBecomeActive;

// This tells the contained controller that transition away from its view just ended.
- (void)viewDidBecomeInactive;

@end

@interface PSScrollViewController : UIViewController

// Scrolling left or right
- (void)scrollLeft;
- (void)scrollRight;

// Set active without animation
- (void)jumpToPage:(int)page;

@end
