//
//  PSAlertView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSAlertView.h"

@interface PSAlertView ()
<UIViewControllerTransitioningDelegate,
UIViewControllerAnimatedTransitioning>
{
    IBOutlet UILabel *messageLabel;
    IBOutlet UIView *buttonsView;
    IBOutlet UIView *alertView;
    
    NSArray *titlesArray;
    NSString *message;
}

@end

@implementation PSAlertView
@synthesize delegate;

- (id)initWithMessage:(NSString *)msg delegate:(id)dgt cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ... {
    self = [self initWithNibName:nil bundle:nil];
    
    if (self) {
        message = msg;
        delegate = dgt;
        titlesArray = [NSArray arrayWithObject:cancelButtonTitle];
        va_list args;
        va_start(args, otherButtonTitles);
        for (NSString *arg = otherButtonTitles; arg != nil; arg = va_arg(args, NSString *)) {
            titlesArray = [titlesArray arrayByAddingObject:arg];
        }
        va_end(args);
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.transitioningDelegate = self;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Customize appearance
    alertView.layer.borderColor = [UIColor whiteColor].CGColor;
    alertView.layer.borderWidth = 2.0;
    alertView.layer.cornerRadius = 10.0;
    
    // Create buttons after loading
    NSInteger count = titlesArray.count;
    for (int i = 0; i < count; i++) {
        CGFloat width = buttonsView.bounds.size.width / count;
        CGRect frame = CGRectMake(i * width, 0, width, buttonsView.bounds.size.height);
        UIButton *button = [[UIButton alloc] initWithFrame:frame];
        [button addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [button setTitle:[titlesArray objectAtIndex:i] forState:UIControlStateNormal];
        [buttonsView addSubview:button];
        
        UIColor *green = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
        button.backgroundColor = green;
        button.layer.borderColor = [UIColor whiteColor].CGColor;
        button.layer.borderWidth = 1.0;
        
        [[button titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Regular" size:14]];
    }
    
    // Setting text
    messageLabel.text = message;
    
    // Add fancy parallax effect
    // Set vertical effect
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(-10);
    verticalMotionEffect.maximumRelativeValue = @(10);
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(-10);
    horizontalMotionEffect.maximumRelativeValue = @(10);
    
    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect, verticalMotionEffect];
    
    // Add both effects to your view
    [alertView addMotionEffect:group];
    
    // Adding fancy blur effect if it's iOS 8
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        // Blur
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurView.frame = self.view.bounds;
        blurView.alpha = .5;
        
        [self.view insertSubview:blurView atIndex:0];
    }
}

#pragma mark - UIViewControllerAnimatedTransitioning protocol
- (id <UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self;
}

- (id <UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    return self;
}

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)context {
    return 0.2;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)context {
    UIViewController *vc1 = [context viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *vc2 = [context viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *container = [context containerView];
    UIView *v1 = vc1.view;
    UIView *v2 = vc2.view;
    
    if (vc2 == self) {
        // presenting
        [container addSubview:v2];
        v2.frame = v1.frame;
        alertView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        v2.alpha = 0;
        v1.tintAdjustmentMode = UIViewTintAdjustmentModeDimmed;
        [UIView animateWithDuration:[self transitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            v2.alpha = 1;
            alertView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            [context completeTransition:YES];
        }];
    } else {
        // dismissing
        [UIView animateWithDuration:[self transitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            v1.alpha = 0;
            alertView.transform = CGAffineTransformMakeScale(1.1, 1.1);
        } completion:^(BOOL finished) {
            v2.tintAdjustmentMode = UIViewTintAdjustmentModeAutomatic;
            [context completeTransition:YES];
        }];
    }
}

// Button index is sender's tag
- (void)buttonPressed:(UIButton *)sender {
    NSInteger index = sender.tag;
    [self dismissWithClickedButtonIndex:index animated:YES];
}

- (void)dismissWithClickedButtonIndex:(NSInteger)index animated:(BOOL)animated {
    // Play dismiss animation
    [self dismissViewControllerAnimated:animated completion:nil];
    
    // Notify delegate
    [delegate alertView:self clickedButtonAtIndex:index];
}

- (void)show {
    [[PSAlertView topMostController] presentViewController:self animated:YES completion:nil];
}

+ (UIViewController*)topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}


/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
