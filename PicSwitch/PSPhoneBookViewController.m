//
//  PSPhoneBookViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/25/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <AddressBook/AddressBook.h>
#import "PSPhoneBookViewController.h"
#import "PSContact.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>
#import "RMPhoneFormat.h"
#import "ServiceManager.h"
#import "PSLoadingView.h"
#import "PSAlertView.h"
#import "PSSelectButton.h"
#import <MessageUI/MessageUI.h>
#import "Mask.h"

typedef enum {
    SEPARATE, PICSWITCH_ONLY, ALL
} SortMode;

@interface PSPhoneBookViewController ()
<MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate,
UIScrollViewDelegate>
{
    IBOutlet UITableView *table;
    IBOutlet UITableView *searchTable;
    IBOutlet NSLayoutConstraint *sendButtonConstraint;
    IBOutlet UIView *sendButton;
    IBOutlet Mask *maskButton;
    IBOutlet UISearchBar *searchBar;
    IBOutlet NSLayoutConstraint *searchBarConstraint;
    
    // Flag for searching
    BOOL isSearching;
    
    // Should show sort button
    BOOL showSortButton;
    
    // State for the show - what kind of filtering/display mode?
    SortMode sortMode;
    
    // An array of arrays of PSContacts, sorted from A - Z and #
    NSArray *contacts;
    
    // The filtered on picswitch contacts
    NSArray *onPicswitchContacts;
    
    // The flattened array of above
    NSArray *onPicswitchContactsFlattened;
    
    // An array of arrays of PSContacts, filtered by the search bar.
    NSArray *filteredContacts;
    
    // The filtered array filtered by picswitch
    NSArray *onPicswitchFilteredContacts;
    
    NSDictionary *onPicswitch;
    NSSet *picswitchFriends;
    NSArray *indexList;
    
    // Responses from server count
    int responses;
    
    // Default country code to attach if there is none
    NSString *countryCode;
    
    PSLoadingView *loadingView;
    
    // We keep a set of phone numbers that are selected.
    NSMutableSet *selected;
    
    MFMessageComposeViewController *messageComposeViewController;
}

@end

@implementation PSPhoneBookViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sortMode = SEPARATE;
    selected = [NSMutableSet set];
    
    // Set up navigation
    [self.navigationItem setTitle:@"add friend"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Set up buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    table.sectionHeaderHeight = 0;
    table.sectionFooterHeight = 0;
    
    indexList = @[@"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J",
                  @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T",
                  @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
    
    // Set up country code
    CTTelephonyNetworkInfo *networkInfo = [CTTelephonyNetworkInfo new];
    CTCarrier *carrier = networkInfo.subscriberCellularProvider;
    
    if (carrier) {
        countryCode = carrier.isoCountryCode;
    } else {
        countryCode = [[[RMPhoneFormat alloc] init] defaultCallingCode];
    }
    
    // Set up search bar
    searchBar.alpha = 0.67;
    [searchBar setBackgroundImage:[self imageWithColor:[UIColor clearColor]]];
    
    // Blocking step
    loadingView = [[PSLoadingView alloc] init];
    [loadingView show:NO];
    
    dispatch_queue_t parseQueue = dispatch_queue_create("parse queue", DISPATCH_QUEUE_SERIAL);
    dispatch_async(parseQueue, ^{
        [self fetchContacts];
        if ([MFMessageComposeViewController canSendText]) {
            messageComposeViewController = [[MFMessageComposeViewController alloc] init];
        }
    });
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)back:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

// This fetches all the contacts from the user's address book.
// As this may take a long time, it should be performed on a separate thread.
- (void)fetchContacts {
    ABAuthorizationStatus status = ABAddressBookGetAuthorizationStatus();
    
    if (status == kABAuthorizationStatusDenied || status == kABAuthorizationStatusRestricted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [loadingView dismiss:YES];
            [self showPermissionDeniedAlert];
        });
        
        return;
    }
    
    CFErrorRef error = NULL;
    ABAddressBookRef addressBook = ABAddressBookCreateWithOptions(NULL, &error);
    
    if (!addressBook) {
        NSLog(@"ABAddressBookCreateWithOptions error: %@", CFBridgingRelease(error));
        return;
    }
    
    ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error) {
        if (error) {
            NSLog(@"ABAddressBookRequestAccessWithCompletion error: %@", CFBridgingRelease(error));
        }
        
        if (granted) {
            // if they gave you permission, then just carry on
            [self parseContacts:addressBook];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [loadingView dismiss:YES];
                [self showPermissionDeniedAlert];
            });
        }
        
        CFRelease(addressBook);
    });
}

- (void)parseContacts:(ABAddressBookRef)addressBook {
    CFRetain(addressBook);
    CFArrayRef addressBookEntries = ABAddressBookCopyArrayOfAllPeople(addressBook);
    CFIndex count = CFArrayGetCount(addressBookEntries);
    NSMutableArray *accumulator = newContactArray();
    NSMutableArray *numbers = [NSMutableArray array];
    
    for (CFIndex i = 0; i < count; i++) {
        ABRecordRef person = CFArrayGetValueAtIndex(addressBookEntries, i);
        
        if (ABRecordGetRecordType(person) != kABPersonType) {
            continue;
        }
        
        ABMultiValueRef phoneNumberProperty = ABRecordCopyValue(person, kABPersonPhoneProperty);
        NSArray *phoneNumbers = (__bridge_transfer NSArray*)ABMultiValueCopyArrayOfAllValues(phoneNumberProperty);
        CFRelease(phoneNumberProperty);
        
        if (phoneNumbers.count == 0) {
            continue;
        }
        
        NSMutableArray *nums = [NSMutableArray array];
        RMPhoneFormat *formatter = [[RMPhoneFormat alloc] init];
        for (NSInteger i = 0; i < phoneNumbers.count; i++) {
            NSString *phoneNumber = [phoneNumbers objectAtIndex:i];
            NSString *filteredText = [[phoneNumber componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                      componentsJoinedByString:@""];
            NSString *formatted = [formatter format:[NSString stringWithFormat:@"+%@", [formatter format:filteredText]]];
            NSString *numbersOnly = [[formatted componentsSeparatedByCharactersInSet:
                                      [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                      componentsJoinedByString:@""];;
            [numbers addObject:numbersOnly];
            [nums addObject:formatted];
        }
        
        PSContact *contact = [[PSContact alloc] init];
        
        NSString *firstName = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonFirstNameProperty));
        NSString *lastName  = (NSString *)CFBridgingRelease(ABRecordCopyValue(person, kABPersonLastNameProperty));
        
        // Better for sorting
        if (!lastName) {
            contact.lastName = firstName;
            contact.firstName = @"";
        } else {
            contact.firstName = firstName;
            contact.lastName = lastName;
        }
        
        contact.isFriend = [NSArray array];
        contact.onPicswitch = [NSArray array];
        contact.phoneNumbers = nums;
        
        // Determine which array we add number to.
        int i = 0;
        if (contact.lastName.length > 0) {
            char str = [[[contact.lastName substringToIndex:1] uppercaseString] characterAtIndex:0];
            
            if (str >= 65 && str <= 90) {
                i = str - 65;
            } else {
                i = 26;
            }
        } else {
            i = 26;
        }
        
        [[accumulator objectAtIndex:i] addObject:contact];
    }

    CFRelease(addressBookEntries);
    CFRelease(addressBook);
    
    // Sort the address book dictionaries
    for (int i = 0; i < 27; i++) {
        [[accumulator objectAtIndex:i] sortUsingComparator:^NSComparisonResult(PSContact *obj1, PSContact *obj2) {
            NSString *s1 = obj1.lastName;
            NSString *s2 = obj2.lastName;
            NSComparisonResult result1 = [s1 localizedCaseInsensitiveCompare:s2];
            
            if (result1 != NSOrderedSame) {
                return result1;
            } else {
                NSString *s3 = obj1.firstName;
                NSString *s4 = obj2.firstName;
                return [s3 localizedCaseInsensitiveCompare:s4];
            }
        }];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        contacts = [NSArray arrayWithArray:accumulator];
        
        // Each call to check numbers will be a portion of user's total database.
        // As there is a timeout issue, we split the number array into segments.
        NSMutableArray *numbersSetArray = [NSMutableArray array];
        NSMutableArray *currentSet = [NSMutableArray array];
        [numbersSetArray addObject:currentSet];
        
        // Send in batch of 20
        int limit = 20;
        
        for (NSString *number in numbers) {
            if (currentSet.count < limit) {
                [currentSet addObject:number];
            } else {
                currentSet = [NSMutableArray array];
                [numbersSetArray addObject:currentSet];
                [currentSet addObject:number];
            }
        }
        
        // Enable sort button if we find a picswitch contact.
        showSortButton = false;
        responses = (int)numbersSetArray.count;
        
        for (int i = 0; i < responses; i++) {
            NSArray *numbersSet = [numbersSetArray objectAtIndex:i];
            
            [[ServiceManager sharedManager] checkNumbers:numbersSet callback:^(id obj, NSError *err) {
                if (!err) {
                    NSMutableDictionary *newData = [NSMutableDictionary dictionaryWithDictionary:obj[@"others"]];
                    [newData addEntriesFromDictionary:onPicswitch];
                    onPicswitch = newData;
                    NSMutableSet *newData2 = [NSMutableSet setWithArray:[obj[@"friends"] allKeys]];
                    [newData2 unionSet:picswitchFriends];
                    picswitchFriends = newData2;
                    
                    [self filterArray];
                    // [table reloadData];
                    [table reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 27)] withRowAnimation:UITableViewRowAnimationAutomatic];
                    [searchTable reloadData];
                    
                    [self showSortButton:onPicswitch.count > 0];
                    
                    if (--responses == 0) {
                        [loadingView dismiss:YES];
                        loadingView = nil;
                    }
                }
            }];
        }
        
        [table reloadData];
        
        // We dismiss after 10 seconds! Or if all responses are back, whichever
        // is faster
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if (loadingView != nil)
                [loadingView dismiss:YES];
        });
    });
}

- (void)showSortButton:(BOOL)shouldShow {
    [[self view] layoutIfNeeded];
    searchBarConstraint.constant = shouldShow? 0 : -30;
    
    [UIView animateWithDuration:0.2 animations:^{
        [[self view] layoutIfNeeded];
    }];
}

- (void)showPermissionDeniedAlert {
    [[[PSAlertView alloc] initWithMessage:@"This app requires access to your contacts to function properly. Please visit to the \"Privacy\" section in the iPhone Settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PSPhoneBookCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PSPhoneBookCellReuseIdentifier"];
    
    // Set cell color
    [cell setColorStyleForBackground:[indexPath row] % 2 bar:([indexPath section] + 1) % 2];
    cell.delegate = self;
    
    PSContact *contact;
    
    if (indexPath.section == 0) {
        if (sortMode == SEPARATE) {
            contact = [onPicswitchContactsFlattened objectAtIndex:indexPath.row];
        }
    } else {
        NSArray *source;
        
        if (tableView == table) {
            if (sortMode == PICSWITCH_ONLY)
                source = onPicswitchContacts;
            else
                source = contacts;
        } else {
            if (sortMode == PICSWITCH_ONLY)
                source = onPicswitchFilteredContacts;
            else
                source = filteredContacts;
        }
        
        contact = [[source objectAtIndex:indexPath.section - 1] objectAtIndex:indexPath.row];
    }
    
    [cell setFirstName:contact.firstName lastName:contact.lastName];
    [cell setNumbers:contact.phoneNumbers];
    [cell setSelectedNumbers:selected];
    
    NSArray *onPic = [onPicswitch allKeys];
    [cell setOnPicswitch:onPic friend:[picswitchFriends allObjects]];
    
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 28;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            // This is the potentially picswitch people.
            if (sortMode == SEPARATE) {
                // Flatten and count
                int counter = 0;
                
                for (NSArray *sub in onPicswitchContacts) {
                    counter += sub.count;
                }
                return counter;
            } else {
                return 0;
            }
                
        default: {
            NSArray *source;
            
            if (tableView == table) {
                if (sortMode == PICSWITCH_ONLY)
                    source = onPicswitchContacts;
                else
                    source = contacts;
            } else {
                if (sortMode == PICSWITCH_ONLY)
                    source = onPicswitchFilteredContacts;
                else
                    source = filteredContacts;
            }
            
            return [[source objectAtIndex:section - 1] count];
        }
    }
}

#pragma mark - UITableViewDelegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0 && sortMode != ALL)
        return 40;
    
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return 0.1;
    
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0 && sortMode != PICSWITCH_ONLY)
        return 40;
    
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return 0.1;
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // First section - most recent or requests
    
    if (section == 0 && sortMode != ALL) {
        CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        
        label.text = @"on picswitch";
        
        label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
        label.textColor = [UIColor whiteColor];
        [container addSubview:label];
        
        return container;
    }
    
    // Deciding whether there should be a header at all!
    if ([self tableView:tableView numberOfRowsInSection:section] == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, 320, [self tableView:table heightForHeaderInSection:section])];
        view.backgroundColor = [UIColor clearColor];
        
        return view;
    }
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    UILabel *label;
    
    // Creating bar
    CGRect frame = CGRectMake(10, 0, 20, 20);
    UIView *bar = [[UIView alloc] initWithFrame:frame];
    if ((section + 1) % 2 == 0)
        [bar setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1]];
    else
        [bar setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.6 brightness:0.6 alpha:1]];
    [container addSubview:bar];
    
    // Creating label
    label = [[UILabel alloc] initWithFrame:frame];
    label.font = [UIFont fontWithName:@"Montserrat-Bold" size:14];
    label.textColor = [UIColor colorWithWhite:1 alpha:0.95];
    label.shadowColor = [UIColor colorWithWhite:0 alpha:0.3];
    label.shadowOffset = (CGSize){0, -0.5};
    label.textAlignment = NSTextAlignmentCenter;
    
    // Set text
    if (section == 27) {
        label.text = @"#";
    } else {
        // Getting the section
        int asciiA = 65;
        char c = (char)(asciiA + section - 1);
        NSString *str = [NSString stringWithFormat:@"%c", c];
        label.text = str;
    }
    
    [container addSubview:label];
    return container;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    if (section == 0 && sortMode != PICSWITCH_ONLY) {
        CGFloat height = [self tableView:tableView heightForFooterInSection:section];
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        
        label.text = @"all contacts";
            
        label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
        label.textColor = [UIColor whiteColor];
        [container addSubview:label];
        
        return container;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [self tableView:table heightForFooterInSection:section])];
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return indexList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return index + 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    PSContact *contact;
    
    if (sortMode != SEPARATE || indexPath.section > 0) {
        NSArray *source;
        
        if (tableView == table) {
            if (sortMode == PICSWITCH_ONLY)
                source = onPicswitchContacts;
            else
                source = contacts;
        } else {
            if (sortMode == PICSWITCH_ONLY)
                source = onPicswitchFilteredContacts;
            else
                source = filteredContacts;
        }
        
        NSArray *sub = [source objectAtIndex:indexPath.section - 1];
        contact = [sub objectAtIndex:indexPath.row];
    } else {
        contact = [onPicswitchContactsFlattened objectAtIndex:indexPath.row];
    }
    
    
    return 40 + 25 * [[contact phoneNumbers] count];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - PSPhoneBookCellDelegate methods
- (void)cell:(PSPhoneBookCell *)cell didSelectNumber:(NSString *)number {
    [selected addObject:number];
}

- (void)cell:(PSPhoneBookCell *)cell didUnselectNumber:(NSString *)number {
    [selected removeObject:number];
}

- (IBAction)sendRequest:(id)sender {
    if ([selected count] == 0) {
        [[[PSAlertView alloc] initWithMessage:@"Select someone to send to!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        // We sort the numbers into two groups according to on picswitch array
        NSMutableSet *registered = [NSMutableSet setWithSet:selected];
        [registered intersectSet:[NSSet setWithArray:[onPicswitch allKeys]]];
        
        NSMutableSet *unregistered = [NSMutableSet setWithSet:selected];
        [unregistered minusSet:registered];
        
        // Now we send server request for those in the set, and initiate a group
        // message for those not registered. Finally clear the selection.
        
        // First we grab all the user ID from the dictionary we got from server
        NSMutableArray *keySet = [NSMutableArray array];
        
        for (NSString *key in registered) {
            NSString *uid = [onPicswitch objectForKey:key];
            [keySet addObject:uid];
        }
        
        // Sending server request
        if (keySet.count > 0) {
            [[ServiceManager sharedManager] sendFriendRequests:keySet callback:^(id obj, NSError *err) {
                NSString *message;
                if (err) {
                    message = @"Send requests failed! Please check your internet connection!";
                } else {
                    message = @"Succesfully invited people on Picswitch!";
                    selected = [NSMutableSet set];
                    [table reloadData];
                }
                
                [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }];
        }
        
        // Sending SMS
        if (unregistered.count > 0) {
            if ([MFMessageComposeViewController canSendText]) {
                messageComposeViewController.messageComposeDelegate = self;
                messageComposeViewController.recipients = [unregistered allObjects];
                NSString *message = @"Hey, join me on picswitch at http://smartURL.it/picswitch so we can send puzzles to each other! #switchitup";
                messageComposeViewController.body = message;
                [self presentViewController:messageComposeViewController animated:YES completion:nil];
            } else {
                [[[PSAlertView alloc] initWithMessage:@"Cannot send friend requests via text message as your device is unable to send them! (Your friends already on Picswitch will receive them, though)" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)ctr didFinishWithResult:(MessageComposeResult)result {
    [ctr dismissViewControllerAnimated:YES completion:nil];
    
    if ([MFMessageComposeViewController canSendText]) {
        messageComposeViewController = [[MFMessageComposeViewController alloc] init];
    }
    
    if (result == MessageComposeResultSent) {
        selected = [NSMutableSet set];
        [table reloadData];
    }
    
}

#pragma mark - Helpful functions
int flattenedCount(NSArray *array) {
    // This returns the flattened count
    if (!array) {
        return 0;
    }
    
    int count = 0;
    
    for (NSArray *subarray in array) {
        if ([subarray isKindOfClass:[NSArray class]]) {
            count += subarray.count;
        }
    }
    
    return count;
}

NSMutableArray *newContactArray() {
    NSMutableArray *accumulator = [NSMutableArray array];
    
    for (int i = 0; i < 27; i++) {
        [accumulator addObject:[NSMutableArray array]];
    }
    
    return accumulator;
}

#pragma mark - Search bar stack
- (void)filterContentForSearchText:(NSString*)searchText
{
    // We first trim the search text for extra spaces
    searchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"fullName contains[c] %@", searchText, searchText];
    
    NSMutableArray *accumulator = [NSMutableArray array];
    
    for (int i = 0; i < contacts.count; i++) {
        NSArray *subarray = [contacts objectAtIndex:i];
        NSArray *filtered = [subarray filteredArrayUsingPredicate:resultPredicate];
        [accumulator addObject:filtered];
    }
    
    filteredContacts = accumulator;
    
    // Now we construct filtered filtered contacts
    // Do the same for filtered doubly
    onPicswitchFilteredContacts = @[];
    
    for (int i = 0; i < contacts.count; i++) {
        NSArray *filteredSub = @[];
        NSArray *sub = [filteredContacts objectAtIndex:i];
        for (int j = 0; j < sub.count; j++) {
            // This is a contact. Check if any number is on picswitch.
            PSContact *contact = [sub objectAtIndex:j];
            BOOL isOn = NO;
            BOOL isFriend = NO;
            
            for (NSString *number in contact.phoneNumbers) {
                NSString *filtered = [[number componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                      componentsJoinedByString:@""];
                isOn = isOn || [[onPicswitch allKeys] containsObject:filtered];
                isFriend = isFriend || [[picswitchFriends allObjects] containsObject:filtered];
            }
            
            if (isOn || isFriend) {
                // Add to filered sub
                filteredSub = [filteredSub arrayByAddingObject:contact];
            }
        }
        
        onPicswitchFilteredContacts = [onPicswitchFilteredContacts arrayByAddingObject:filteredSub];
    }
    
    [searchTable reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    int prev = flattenedCount(filteredContacts);
    [self filterContentForSearchText:searchText];
    int curr = flattenedCount(filteredContacts);
    
    if (prev == 0 && curr > prev) {
        [maskButton setAlpha:0];
        [searchTable setAlpha:1];
        [[self view] bringSubviewToFront:searchTable];
    } else if (curr == 0 && prev > curr){
        [maskButton setAlpha:1];
        [searchTable setAlpha:0];
        [[self view] bringSubviewToFront:table];
    }
    
    BOOL t = searchText.length != 0;
    [maskButton showText:t];
}

- (void)endEdit {
    [[self view] endEditing:YES];
    [self enableCancelButton:searchBar];
}

- (IBAction)endSearch:(id)sender {
    [self searchBarCancelButtonClicked:searchBar];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    if (bar.text.length != 0)
        return true;
    
    [self isSearching:true];
    
    [UIView animateWithDuration:0.2 animations:^{
        [bar setAlpha:1];
    }];
    
    return true;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)bar {
    [bar setShowsCancelButton:true animated:true];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)bar
{
    filteredContacts = [NSArray array];
    [bar endEditing:true];
    [bar setText:@""];
    [self searchBar:bar textDidChange:@""];
    [self isSearching:false];
    
    
    [UIView animateWithDuration:0.2 animations:^{
        [searchBar setAlpha:0.67];
    }];
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)bar {
    [bar setShowsCancelButton:false animated:true];
    return true;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)bar {
    if (bar.text.length == 0)
        [self searchBarCancelButtonClicked:bar];
}

- (void)isSearching:(BOOL)searching {
    [maskButton setUserInteractionEnabled:searching];
    
    [UIView animateWithDuration:0.2 animations:^{
        [maskButton setAlpha:searching];
        [table setAlpha:!searching];
        [searchTable setAlpha:0];
    } completion:^(BOOL finished) {
        [[self view] bringSubviewToFront:table];
    }];
    
    isSearching = searching;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self endEdit];
}

// Recursively enable the first button we find
- (void)enableCancelButton:(UIView *)view {
    for (UIView *subview in view.subviews) {
        if ([subview isKindOfClass:[UIButton class]]) {
            [(UIButton *)subview setEnabled:YES];
            
            return;
        } else {
            [self enableCancelButton:subview];
        }
    }
}

- (IBAction)filter:(id)sender {
    // We check which mode we are in and cycle through it.
    sortMode = (sortMode + 1) % 3;
    
    if (sortMode == ALL) {
        // We don't need to do anything as we are displaying everything
    } else {
        // We do what we do in ALL, except we also have all picswitch at the top
        // That is, we would display on picswitch contacts in a flattened list,
        // which means we should recalculate it here!
        
        // If it is only picswitch people, then we display it as an indexed
        // thing. Eitherway we filter.
        
        // We do filtering here
        [self filterArray];
    }
    [table reloadData];
    [searchTable reloadData];
}

- (void)filterArray {
    onPicswitchContacts = @[];
    onPicswitchContactsFlattened = @[];
    
    for (int i = 0; i < contacts.count; i++) {
        NSArray *filteredSub = @[];
        NSArray *sub = [contacts objectAtIndex:i];
        for (int j = 0; j < sub.count; j++) {
            // This is a contact. Check if any number is on picswitch.
            PSContact *contact = [sub objectAtIndex:j];
            BOOL isOn = NO;
            BOOL isFriend = NO;
            
            for (NSString *number in contact.phoneNumbers) {
                NSString *filtered = [[number componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                                      componentsJoinedByString:@""];
                isOn = isOn || [[onPicswitch allKeys] containsObject:filtered];
                isFriend = isFriend || [[picswitchFriends allObjects] containsObject:filtered];
            }
            
            if (isOn || isFriend) {
                // Add to filered sub
                filteredSub = [filteredSub arrayByAddingObject:contact];
                
                // Add to flattened
                onPicswitchContactsFlattened = [onPicswitchContactsFlattened arrayByAddingObject:contact];
            }
        }
        
        onPicswitchContacts = [onPicswitchContacts arrayByAddingObject:filteredSub];
    }
}

@end
