//
//  Mask.h
//  Big Red Ambition: 161 things to do at Cornell
//
//  Created by Jiacong Xu on 9/18/14.
//  Copyright (c) 2014 Jiacong Xu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Mask : UIButton
{
    CGFloat start;
    CGFloat end;
    CGFloat opacity;
    
    BOOL forPic;
}

- (void)setOpacity:(float)op;

- (void)showText:(BOOL)shouldShow;

- (void)adjustRatioStart:(CGFloat)start end:(CGFloat)end;

- (void)setForPicture;

@end
