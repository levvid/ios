//
//  PSContactViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSContactCell.h"
#import "PSContactViewController.h"
#import <AddressBook/AddressBook.h>
#import "PSContact.h"
#import "PSAddFriendViewController.h"
#import <Parse/Parse.h>
#import "ServiceManager.h"
#import "PSAlertView.h"
#import "PSNotificationView.h"
#import "CameraViewController.h"
#import "PSScrollViewController.h"

int const MOST_RECENT_MAX = 5;

@interface PSContactViewController ()
{
    IBOutlet NSLayoutConstraint *sendButtonConstraint;
    
    NSString *navTitle;
    
    ContactViewControllerMode mode;
    
    NSMutableArray *firstSection;
    
    IBOutlet UITableView *tableview;
    IBOutlet UIView *sendButton;
    IBOutlet UIView *requestButton;
    IBOutlet UIView *friendButton;
    
    NSArray *contacts;
    
    NSMutableSet *selected;
    
    // This is used to determine whether we want to clear out selection in view
    // mode. (To prevent actions performed on both requests and friends).
    BOOL selectingFriends;
    
    PFObject *picswitch;
    
    BOOL updatedFriends;
    BOOL updatedRequests;
    
    PSAlertView *deleteAlert;
    PSAlertView *flagAlert;
    
    NSArray *indexList;
    
    BOOL isReply;
}
@end

@implementation PSContactViewController
@synthesize recipients;

- (void)setMode:(ContactViewControllerMode)m {
    mode = m;
    [self updateUI];
}

- (void)isReply {
    isReply = YES;
}

// Called after set mode, or at init
- (void)updateUI {
    switch (mode) {
        case ContactViewControllerModeSend:
            navTitle = @"send a puzzle";
            break;
        case ContactViewControllerModeView:
            navTitle = @"friends";
            sendButtonConstraint.constant = -sendButton.frame.size.height;
            break;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [self fetchFriends];
}

- (void)awakeFromNib {
    isReply = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateUI];
    
    // Set up navigation
    [self.navigationItem setTitle:navTitle];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Set up buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addFriend:)];
    [self.navigationItem setRightBarButtonItem:right];
    
    selected = [NSMutableSet set];
    contacts = [NSArray array];
    
    // Load selected if they should be replied to
    if (mode == ContactViewControllerModeSend && recipients) {
        [selected addObjectsFromArray:recipients];
    }
    
    // Table setup
    tableview.sectionHeaderHeight = 0;
    tableview.sectionFooterHeight = 0;
    
    indexList = @[@"★", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I",
                  @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S",
                  @"T", @"U", @"V", @"W", @"X", @"Y", @"Z", @"#"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController setToolbarHidden:YES animated:YES];
}

- (IBAction)addFriend:(id)sender {
    /*
    PSAddFriendViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AddFriendViewControllerId"];
     */
    UIViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PhoneBookVC"];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (void)back:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)updateToolbar {
    // Doesn't need to do anything to toolbar when we are sending
    if (mode == ContactViewControllerModeSend)
        return;
    
    // Show/hide toolbar
    [[self view] layoutIfNeeded];
    
    sendButtonConstraint.constant = selected.count > 0? 0 : -sendButton.frame.size.height;
    
    if (selectingFriends) {
        [[self view] bringSubviewToFront:friendButton];
    } else {
        [[self view] bringSubviewToFront:requestButton];
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        [[self view] layoutIfNeeded];
    }];
}

#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            // A single "send to yourself button" if we are in send mode
            return 1 - mode;
            break;
        case 1:
            // This is the most recent section or requests section depending on which mode
            return firstSection.count;
            break;
        case 28:
            // This is the # section
            return [[self contactsWithFirstLetter:@"#"] count];
        case 29:
            // This is the no friend section if user has no friends
            return firstSection.count == 0 && contacts.count == 0;
        default: {
            // Look at the contacts array and find all people with the given name.
            int asciiA = 65;
            char c = (char)(asciiA + section - 1);
            NSString *str = [NSString stringWithFormat:@"%c", c];
            return [[self contactsWithFirstLetter:str] count];
            break;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 29) {
        return [tableView dequeueReusableCellWithIdentifier:@"NoFriendCell"];
    }
    
    PSContactCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PSContactCellReuseIdentifier"];
    
    // Set cell color
    [cell setColorStyleForBackground:[indexPath row] % 2 bar:([indexPath section] + 1) % 2];
    cell.delegate = self;
    cell.cellUser = nil;
    
    PFUser *contact;
    if (indexPath.section == 0) {
        // Return special cell
        cell.cellUser = [PFUser currentUser];
        [cell setFirstName:@"Send to yourself" lastName:@""];
        [cell setSelectionStatus:[selected containsObject:cell.cellUser.objectId]];
        return cell;
    } else if (indexPath.section == 1) {
        contact = [firstSection objectAtIndex:indexPath.row];
    } else {
        int asciiA = 65;
        char c = (char)(asciiA + indexPath.section - 1);
        NSString *str = [NSString stringWithFormat:@"%c", c];
        NSArray *sub = [self contactsWithFirstLetter:str];
        
        contact = [sub objectAtIndex:indexPath.row];
    }
   
    cell.cellUser = contact;
    
    BOOL shouldSelect = NO;
    for (PFUser *user in selected) {
        if (user.objectId == contact.objectId) {
            shouldSelect = YES;
            break;
        }
    }
    
    [cell setSelectionStatus:shouldSelect];
    
    [cell setFirstName:contact[@"firstName"] lastName:contact[@"lastName"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return indexPath.section == 29? 240 : 36;
}

- (IBAction)reportUser:(id)sender {
    // Display warning
    NSEnumerator *enumerator = [selected objectEnumerator];
    NSString *cat = [enumerator nextObject][@"firstName"];
    
    for (int i = 0; i < selected.count - 1; i++) {
        PFUser *usr = [enumerator nextObject];
        NSString *sep = i == selected.count - 2? @" and " : @", ";
        cat = [NSString stringWithFormat:@"%@%@%@", cat, sep, usr[@"firstName"]];
    }
    
    NSString *msg = [NSString stringWithFormat:@"Are you sure you want to report %@?", cat];
    flagAlert = [[PSAlertView alloc] initWithMessage:msg delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [flagAlert show];
}

- (IBAction)acceptRequest:(id)sender {
    [[ServiceManager sharedManager] respondToRequests:[selected allObjects] response:ACCEPT callback:^(id obj, NSError *err) {
        if (!err) {
            selected = [NSMutableSet set];
            [self refreshContent];
            [self updateToolbar];
            [[[PSNotificationView alloc] initWithMessage:@"Friend request(s) accepted!"] show];
        } else {
            [[[PSAlertView alloc] initWithMessage:@"Unable to contact server! Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (IBAction)declineRequest:(id)sender {
    [[ServiceManager sharedManager] respondToRequests:[selected allObjects] response:DECLINE callback:^(id obj, NSError *err) {
        if (!err) {
            selected = [NSMutableSet set];
            [self refreshContent];
            [self updateToolbar];
            [[[PSNotificationView alloc] initWithMessage:@"Friend request(s) declined!"] show];
        } else {
            [[[PSAlertView alloc] initWithMessage:@"Unable to contact server! Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        }
    }];
}

- (IBAction)newMessage:(id)sender {
    // Open the create camera view and save this contact as selected
    CameraViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"CVC"];
    [viewController setRecipients:[selected allObjects]];
    [[self navigationController] pushViewController:viewController animated:YES];
}

- (IBAction)deleteFriend:(id)sender {
    // Display warning
    NSEnumerator *enumerator = [selected objectEnumerator];
    NSString *cat = [enumerator nextObject][@"firstName"];
    
    for (int i = 0; i < selected.count - 1; i++) {
        PFUser *usr = [enumerator nextObject];
        NSString *sep = i == selected.count - 2? @" and " : @", ";
        cat = [NSString stringWithFormat:@"%@%@%@", cat, sep, usr[@"firstName"]];
    }
    
    NSString *msg = [NSString stringWithFormat:@"Are you sure you want to delete %@?", cat];
    deleteAlert = [[PSAlertView alloc] initWithMessage:msg delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [deleteAlert show];
}

#pragma mark - UITableViewDelegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return 0.1 * mode;
    
    if (section == 1)
        return 40;
    
    if (section == 29)
        return 0.1;
    
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return 0.1;
    
    return 20;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0)
        return 0.1;
    
    if (section == 1)
        return 40;
    
    if (section == 29)
        return 0.1;
    
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return 0.1;
    
    return 20;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    // First section - most recent or requests
    if (section == 0 || section == 29) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [self tableView:tableView heightForHeaderInSection:section])];
        view.backgroundColor = [UIColor clearColor];
        return view;
    } else if (section == 1) {
        CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        
        if (mode == ContactViewControllerModeSend) {
            label.text = @"recent";
        } else {
            label.text = [NSString stringWithFormat:@"friend requests (%i)", (int)firstSection.count];
        }
        
        label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
        label.textColor = [UIColor whiteColor];
        [container addSubview:label];
        
        return container;
    }
    
    // Deciding whether there should be a header at all!
    if ([self tableView:tableView numberOfRowsInSection:section] == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [self tableView:tableView heightForHeaderInSection:section])];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 20)];
    container.backgroundColor = [UIColor clearColor];
    UILabel *label;
    
    // Creating bar
    CGRect frame = CGRectMake(10, 0, 20, 20);
    UIView *bar = [[UIView alloc] initWithFrame:frame];
    if ((section + 1) % 2 == 0)
        [bar setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1]];
    else
        [bar setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.6 brightness:0.6 alpha:1]];
    [container addSubview:bar];
    
    // Creating label
    label = [[UILabel alloc] initWithFrame:frame];
    label.font = [UIFont fontWithName:@"Montserrat-Bold" size:14];
    label.textColor = [UIColor colorWithWhite:1 alpha:0.95];
    label.shadowColor = [UIColor colorWithWhite:0 alpha:0.3];
    label.shadowOffset = (CGSize){0, -0.5};
    label.textAlignment = NSTextAlignmentCenter;
    
    // Set text
    if (section == 27) {
        label.text = @"#";
    } else {
        // Getting the section
        int asciiA = 65;
        char c = (char)(asciiA + section - 1);
        NSString *str = [NSString stringWithFormat:@"%c", c];
        label.text = str;
    }
    
    [container addSubview:label];
    return container;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    // First section - showing other friends
    if (section == 0 || section == 29) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [self tableView:tableView heightForFooterInSection:section])];
        view.backgroundColor = [UIColor clearColor];
        return view;
    } else if (section == 1) {
        CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
        UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
        container.backgroundColor = [UIColor clearColor];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, 30)];
        label.text = [NSString stringWithFormat:@"friends (%i)", (int)contacts.count];
        label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
        label.textColor = [UIColor whiteColor];
        [container addSubview:label];
        
        return container;
    } else {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, [self tableView:tableView heightForFooterInSection:section])];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return indexList;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return index + 1;
}

// Attempt to get friends from server
- (void)refreshContent {
    // Get friends
    PFQuery *query = [[[PFUser currentUser] relationForKey:@"friends"] query];
    
    // Place a request to fetch on server
    [query findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *err){
        if (err) {
            NSLog(@"%@", [err localizedDescription]);
        } else {
            // Pin in background for fast fetch
            // We also save all the object ID to user defaults due to a bug
            // in Parse local database unable to fetch relations.
            [PFObject pinAllInBackground:array];
            NSArray *strArr = [NSArray array];
            for (PFUser *user in array) {
                strArr = [strArr arrayByAddingObject:user.objectId];
            }
            [[NSUserDefaults standardUserDefaults] setObject:strArr forKey:@"userFriends"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            contacts = array;
            /*
             [UIView setAnimationsEnabled:NO];
             [tableview reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(1, 26)] withRowAnimation:UITableViewRowAnimationNone];
             [UIView setAnimationsEnabled:YES];
             */
            
            [tableview reloadData];
            updatedFriends = YES;
        }
    }];
    
    if (mode == ContactViewControllerModeView) {
        [[ServiceManager sharedManager] fetchUserFriendRequests:^(NSArray *requests, NSError *err) {
            if (err) {
                NSLog(@"Unable to fetch friend requests! %@", [err userInfo][@"error"]);
            } else {
                [PFObject pinAllInBackground:requests];
                NSArray *strArr = [NSArray array];
                for (PFUser *user in requests) {
                    strArr = [strArr arrayByAddingObject:user.objectId];
                }
                [[NSUserDefaults standardUserDefaults] setObject:strArr forKey:@"incomingRequests"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                firstSection = [NSMutableArray arrayWithArray:requests];
                /*
                 [UIView setAnimationsEnabled:NO];
                 [tableview reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
                 [UIView setAnimationsEnabled:YES];
                 */
                [tableview reloadData];
                
                updatedRequests = YES;
            }
        }];
    }
}

- (void)fetchFriends {
    // Attempt to get friends locally first, but also place a call on server to
    // update friends
    
    // This indicator tells whether we already got updates from server (probably
    // impossible but for failsafe purposes)
    updatedFriends = NO;
    
    [self refreshContent];
    
    // Populate right now with data from local datastore
    NSArray *friends = [[NSUserDefaults standardUserDefaults] arrayForKey:@"userFriends"];
    PFQuery *localQuery = [PFUser query];
    [localQuery whereKey:@"objectId" containedIn:friends];
    [localQuery fromLocalDatastore];
    
    // Essentially the same code, except we check whether we got server data
    // first. In that case we won't have to do anything.
    [localQuery findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *err){
        if (err) {
            NSLog(@"%@", [err localizedDescription]);
        } else {
            // Only update if we haven't reached the server yet
            if (!updatedFriends) {
                // The array is our stored contact list.
                contacts = array;
                [tableview reloadData];
            } else {
                NSLog(@"Received server data before local fetch!");
            }
        }
    }];
    
    // Get requests or most recent
    if (mode == ContactViewControllerModeSend) {
        firstSection = [NSMutableArray array];
        
        NSArray *recentIds = [NSArray arrayWithArray:[[[NSUserDefaults standardUserDefaults] objectForKey:@"RecentContacts"] objectForKey:[PFUser currentUser].objectId]];
        PFQuery *recentQuery = [PFUser query];
        [recentQuery whereKey:@"objectId" containedIn:recentIds];
        [recentQuery fromLocalDatastore];
        
        [recentQuery findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *err){
            if (err) {
                NSLog(@"%@", [err localizedDescription]);
            } else {
                firstSection = [NSMutableArray arrayWithArray:array];
                [tableview reloadData];
            }
        }];
        
    } else {
        // Grab all requests instead
        // Again, this operates like users where we require local data first
        updatedRequests = NO;
        
        // Now fetch cached version
        NSArray *requests = [[NSUserDefaults standardUserDefaults] arrayForKey:@"incomingRequests"];
        PFQuery *localQuery = [PFUser query];
        [localQuery whereKey:@"objectId" containedIn:requests];
        [localQuery fromLocalDatastore];
        
        [localQuery findObjectsInBackgroundWithBlock:^(NSArray *array, NSError *err){
            if (err) {
                NSLog(@"%@", [err localizedDescription]);
            } else {
                // Only update if we haven't reached the server yet
                if (!updatedRequests) {
                    // The array is our requests list.
                    firstSection = [NSMutableArray arrayWithArray:array];
                    [tableview reloadData];
                } else {
                    NSLog(@"Received server data before local fetch!");
                }
            }
        }];
    }
}

- (void)updateRecentContacts:(NSSet *)users {
    // Add recipients to the recent list
    for (PFUser *user in users) {
        if (user == [PFUser currentUser])
            continue;
        NSMutableIndexSet *set = [NSMutableIndexSet indexSet];
        for (int i = 0; i < firstSection.count; i++) {
            PFUser *selectedUser = [firstSection objectAtIndex:i];
            if (selectedUser.objectId == user.objectId) {
                [set addIndex:i];
            }
        }
        [firstSection removeObjectsAtIndexes:set];
        
        [firstSection insertObject:user atIndex:0];
    }
    
    // Trim the list
    if (firstSection.count > MOST_RECENT_MAX)
        [firstSection removeObjectsInRange:NSMakeRange(MOST_RECENT_MAX, firstSection.count - MOST_RECENT_MAX)];
    
    // Save the list
    NSMutableDictionary *recent = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] objectForKey:@"RecentContacts"]];
    NSMutableArray *ids = [NSMutableArray array];
    
    for (int i = 0; i < firstSection.count; i++) {
        [ids addObject:[[firstSection objectAtIndex:i] objectId]];
    }
    
    [recent setObject:ids forKey:[PFUser currentUser].objectId];
    [[NSUserDefaults standardUserDefaults] setObject:recent forKey:@"RecentContacts"];
}

- (IBAction)sendPicswitch:(id)sender {
    // Remove system message sender from the list
    [selected removeObject:[PFUser objectWithoutDataWithObjectId:@"V5EEDV3lD7"]];
    
    if ([selected count] == 0) {
        [[[PSAlertView alloc] initWithMessage:@"Select someone to send to!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        // Save recents
        [self updateRecentContacts:selected];
        
        // Building up the picswitch to send!
        picswitch[@"sender"] = [PFUser currentUser];
        PFRelation *rps = [picswitch relationForKey:@"recipients"];
        NSMutableArray *rpIds = [NSMutableArray array];
        
        for (PFUser *user in selected) {
            [rps addObject:user];
            [rpIds addObject:user.objectId];
        }
        
        // Sending picswitch!
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sending..." message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
        [alert show];
        
        [[ServiceManager sharedManager] sendPuzzle:picswitch recipients:rpIds callback:^(id success, NSError *err) {
            [alert dismissWithClickedButtonIndex:0 animated:YES];
            
            if (!err) {
                if (isReply) {
                    UIViewController *vc = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 3];
                    [self.navigationController popToViewController:vc animated:YES];
                } else {
                    [self.navigationController popViewControllerAnimated:YES];
                    [(PSScrollViewController *)[self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count - 1] scrollLeft];
                    
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"PicswitchSent" object:self];
                [[[PSNotificationView alloc] initWithMessage:@"picswitch sent"] show];
                
            } else {
                [[[PSAlertView alloc] initWithMessage:[err localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }];
    }
}

// This parses all picswitch friends
- (void)parseFriends:(NSArray *)friends {
    NSMutableArray *temp = [NSMutableArray array];
    
    for (PFUser *friend in friends) {
        PSContact *contact = [PSContact new];
        contact.firstName = friend[@"firstName"];
        contact.lastName = friend[@"lastName"];
        [temp addObject:contact];
    }
    
    contacts = temp;
    [tableview reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 28)] withRowAnimation:UITableViewRowAnimationAutomatic];
}

// This returns all the contacts with the last name starting with letter c,
// ignoring case. If c is "#", returns the list of contacts that do not start
// with a letter.
- (NSArray *)contactsWithFirstLetter:(NSString *)c {
    NSMutableArray *array = [NSMutableArray array];
    NSMutableArray *nonAn = [NSMutableArray array];
    
    for (int i = 0; i < [contacts count]; i++) {
        PFUser *contact = [contacts objectAtIndex:i];
        NSString *str = [contact[@"lastName"] substringToIndex:1];
        
        if (str.length > 0 && [str caseInsensitiveCompare:c] == NSOrderedSame) {
            [array addObject:contact];
        }
    }
    
    if ([c isEqualToString:@"#"]) {
        return nonAn;
    } else {
        return array;
    }
}

#pragma mark - PSContactCellDelegate
- (void)contactCell:(PSContactCell *)cell changedState:(BOOL)isSelected {
    // First depending on the mode we might want to modify the selection
    PFUser *contact = cell.cellUser;
    
    // Selection needs checking, deselection doesn't.
    if (isSelected) {
        NSIndexPath *ip = [tableview indexPathForCell:cell];
        BOOL isFriend = ip.section > 1;
        
        // We remove if we are selecting something from a different section.
        if (mode == ContactViewControllerModeView && isFriend != selectingFriends) {
            [selected removeAllObjects];
            selectingFriends = isFriend;
        }
        
        [selected addObject:contact];
    } else {
        [selected removeObject:contact];
    }
    
    // UI updates
    // We want to make sure the selection goes through ALL visible cells, so
    // update all cells
    NSArray *visible = [tableview visibleCells];
    
    for (UITableViewCell *cell in visible) {
        if ([[cell class] isSubclassOfClass:[PSContactCell class]]) {
            PSContactCell *c = (PSContactCell *)cell;
            [c setSelectionStatus:[selected containsObject:c.cellUser]];
        }
    }
    
    [self updateToolbar];
}

// Call this before we reach this view controller. It should contain all the
// photo and captions
- (void)assignPicswitch:(PFObject *)p {
    picswitch = p;
}

#pragma mark - PSAlertViewDelegate
- (void)alertView:(PSAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    if (index == 1) {
        if (alertView == deleteAlert) {
            [[ServiceManager sharedManager] deleteFriends:[selected allObjects] callback:^(id obj, NSError *err) {
                if (!err) {
                    selected = [NSMutableSet set];
                    [self refreshContent];
                    [self updateToolbar];
                    [[[PSNotificationView alloc] initWithMessage:@"Removed!"] show];
                } else {
                    [[[PSAlertView alloc] initWithMessage:@"Unable to contact server! Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }];
        } else if (alertView == flagAlert) {
            [[ServiceManager sharedManager] flagUsers:[selected allObjects] callback:^(id obj, NSError *err) {
                if (!err) {
                    selected = [NSMutableSet set];
                    [self refreshContent];
                    [self updateToolbar];
                    [[[PSNotificationView alloc] initWithMessage:@"Flagged!"] show];
                } else {
                    [[[PSAlertView alloc] initWithMessage:@"Unable to contact server! Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }];
        }
    }
}

@end
