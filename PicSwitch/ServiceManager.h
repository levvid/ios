//
//  ServiceManager.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/27/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/**
 This singleton class manages the entire conversation with the Parse server. All
 network traffic should go through this class.
 */

@class PFUser, PFObject;
@interface ServiceManager : NSObject

typedef enum : NSUInteger {
    ACCEPT = 0,    // Request is accepted
    DECLINE = 1,   // Request is declined
    CANCEL = 2     // Sender cancelled request
} FRRequestAction;

+ (instancetype)sharedManager;

/**
 Fetch the new messages for the given user and return it in an array
 */
- (void)fetchNewMessages:(void (^)(NSArray *, NSError *))callback;

/**
 Fetch the new messages for the given user and return it in an array
 */
- (void)fetchFeaturedPuzzles:(void (^)(NSArray *, NSError *))callback;

/**
 Fetch additional puzzle categories
 */
- (void)fetchExtraCategoriesWithCallback:(void (^)(NSArray *, NSError *))callback;

/**
 Fetch the new puzzles for a given category index
 */
- (void)fetchExtrasForCategoryNamed:(NSString *)name callback:(void (^)(NSArray *, NSError *))callback;

/**
 Handles user login
 */
- (void)login:(NSString *)usr password:(NSString *)psd completion:(void (^)(BOOL, NSError *))block;

/**
 This handles server communication after user finishes
 */
- (void)userPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs;
- (void)extraPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs;
- (void)featuredPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs;

/**
 This is sent when user deletes a message
 */
- (void)userPuzzleDeleted:(PFObject *)message;

/**
 This grabs all the friend requests current user has.
 */
- (void)fetchUserFriendRequests:(void (^)(NSArray *, NSError *))callback;

/**
 This requests phone number validation services.
 */
- (void)requestValidation:(NSString *)number callback:(void (^)(id obj, NSError *err))block;

/**
 This sends a puzzle object to the server.
 */
- (void)sendPuzzle:(PFObject *)puzzle recipients:(NSArray *)rpIds callback:(void (^)(id obj, NSError *err))block;

/**
 This parses given list of users who sent a friend request with uniform response.
 */
- (void)respondToRequests:(NSArray *)requests response:(FRRequestAction)action callback:(void (^)(id obj, NSError *err))block;

/**
 This parses given list of users and remove them as friends.
 */
- (void)deleteFriends:(NSArray *)friends callback:(void (^)(id obj, NSError *err))block;

/**
 This parses given list of users and report them.
 */
- (void)flagUsers:(NSArray *)users callback:(void (^)(id obj, NSError *err))block;

/**
 Takes a list of numbers and check whether they are on picswitch/friends.
 */
- (void)checkNumbers:(NSArray *)numbers callback:(void (^)(id obj, NSError *err))block;

/**
 This sends a list of users friend requeusts.
 */
- (void)sendFriendRequests:(NSArray *)userIds callback:(void (^)(id obj, NSError *err))block;

/**
 Changes user's password and immediately update to server.
 */
- (void)changeUserPassword:(NSString *)newPassword callback:(void (^)(BOOL obj, NSError *err))block;

- (void)changeUserFirstName:(NSString *)firstName lastName:(NSString *)lastName callback:(void (^)(BOOL obj, NSError *err))block;

@end
