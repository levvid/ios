//
//  PSStatsViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSStatsViewController : UITableViewController

- (void)configureNavigation;

@end
