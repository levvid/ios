//
//  PSPuzzleView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/1/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 This class handles all instances of puzzle solving screen's puzzles.
 */

#import <UIKit/UIKit.h>

@class PSPuzzleView;
@protocol PSPuzzleViewDelegate

/**
 This is called when the puzzle has finished loading and is signalled to start.
 */
- (void)puzzleDidStart:(PSPuzzleView *)puzzpleView;

/**
 This is called when user makes a move. The number of moves can be accessed
 in the puzzle view.
 */
- (void)userSwitched:(PSPuzzleView *)puzzleView;

/**
 This is called when user solved a puzzle. The state transition is automatic.
 */
- (void)userCompleted:(PSPuzzleView *)puzzleView;

@end

@interface PSPuzzleView : UIControl

@property (nonatomic, weak) IBOutlet id <PSPuzzleViewDelegate> delegate;

/// The puzzle's image. This is dissected into smaller squares and presented.
@property (nonatomic, strong) UIImage *puzzleImage;

- (void)toggleGrid:(BOOL)isShowing animated:(BOOL)animated;

/**
 Let the user interact with the puzzle, with an optional fade in animation.
 */
- (void)startSolvingAnimated:(BOOL)animated;

/**
 This instantly solve the puzzle, optionally playing the solved animation.
 */
- (void)forceFinishAnimated:(BOOL)animated;

/**
 This istantly restart the puzzle.
 */
- (void)restartPuzzle;

/**
 This transforms the puzzle into a preview, allowing user to select grids but
 not swap them. This is not undoable.
 */
- (void)becomePreview;

/**
 Returns the current grid that is selected by user. If none is selected, this
 returns -1.
 */
- (int)selectedGrid;

/**
 Returns whether this puzzle has been completed.
 */
- (BOOL)isComplete;

@end
