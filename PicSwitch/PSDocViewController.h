//
//  PSDocViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/28/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This controller displays (nicely) content in a file.
 */

@interface PSDocViewController : UIViewController
{
    IBOutlet UITextView *mainDisplay;
    
    // Attributes for attributed strings in the text view.
    NSDictionary *titleAttributes;
    NSDictionary *contentAttributes;
}

/**
 Fill the screen with the given string and title.
 */
- (void)setupWithTitle:(NSString *)title content:(NSString *)string;

/**
 Returns the picswitch terms of service as a single string.
 */
+ (NSString *)terms;

/**
 Returns the picswitch privacy policy as a single string.
 */
+ (NSString *)privacy;

/**
 Returns the picswitch featured puzzle rules as a single string.
 */
+ (NSString *)featuredRules;

/**
 Returns the picswitch FAQ as a single string.
 */
+ (NSString *)faq;
@end
