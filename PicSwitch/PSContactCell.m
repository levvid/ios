//
//  PSContactCell.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSContactCell.h"

@implementation PSContactCell
@synthesize cellUser, delegate;

- (void)awakeFromNib {
    bgView.layer.cornerRadius = 8;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setColorStyleForBackground:(int)background bar:(int)bar {
    switch (background) {
        case 0:
            [bgView setBackgroundColor:[UIColor colorWithWhite:0.25 alpha:1.0]];
            break;
        case 1:
            [bgView setBackgroundColor:[UIColor clearColor]];
        default:
            break;
    }
    
    switch (bar) {
        case 0:
            [barView setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1]];
            break;
        case 1:
            [barView setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.6 brightness:0.6 alpha:1]];
        default:
            break;
    }
}

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    UIFont *font1 = [UIFont fontWithName:@"Montserrat-light" size:14.0];
    UIFont *font2 = [UIFont fontWithName:@"Montserrat-regular" size:14.0];
    NSDictionary *dict1 = @{NSFontAttributeName : font1};
    NSDictionary *dict2 = @{NSFontAttributeName : font2};
    NSMutableAttributedString *s1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", firstName] attributes:dict1];
    NSMutableAttributedString *s2 = [[NSMutableAttributedString alloc] initWithString:lastName attributes:dict2];
    [s1 appendAttributedString:s2];
    [nameLabel setAttributedText:s1];
}

- (void)selectButton:(PSSelectButton *)button stateToggled:(BOOL)isSelected {
    if (delegate) {
        [delegate contactCell:self changedState:isSelected];
    }
}

- (void)setSelectionStatus:(BOOL)isSelected {
    [selectButton setSelectionState:isSelected animated:YES];
}

@end
