//
//  ServiceManager.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/27/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "ServiceManager.h"
#import "AppDelegate.h"
#import "PSPuzzle.h"
#import <Parse/Parse.h>

@implementation ServiceManager

CGFloat const THUMBNAIL_SIZE = 72;

+ (instancetype)sharedManager {
    static ServiceManager *manager = nil;
    
    if (!manager) {
        manager = [[super allocWithZone:nil] init];
    }
    
    return manager;
}

+ (id)allocWithZone:(struct _NSZone *)zone
{
    return [self sharedManager];
}

- (void)login:(NSString *)usr password:(NSString *)psd completion:(void (^)(BOOL, NSError *))block {
    [PFUser logInWithUsernameInBackground:usr password:psd block:
     ^(PFUser *user, NSError *error) {
         block((BOOL)user, error);
         
         if (!error) {
             // execute the block as well as adding user to parse installation
             PFInstallation *installation = [PFInstallation currentInstallation];
             installation[@"username"] = [user username];
             [installation saveEventually];
         }
     }];
}

- (void)fetchNewMessages:(void (^)(NSArray *, NSError *))callback {
    PFUser *currentUser = [PFUser currentUser];
    
    if (!currentUser) {
        NSLog(@"Not logged in, unable to fetch new messages!");
        return;
    }
    
    PFQuery *query = [PFQuery queryWithClassName:@"Messages"];
    [query whereKey:@"recipients" equalTo:currentUser.objectId];
    [query includeKey:@"sender"];
    [query addAscendingOrder:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:callback];
}

- (void)fetchFeaturedPuzzles:(void (^)(NSArray *, NSError *))callback {
    PFQuery *query = [PFQuery queryWithClassName:@"FeaturedPuzzles"];
    [query addAscendingOrder:@"createdAt"];
    NSArray *solved = [PFUser currentUser][@"featuredSolved"];
    if (solved && solved.count > 0) {
        [query whereKey:@"objectId" notContainedIn:[PFUser currentUser][@"featuredSolved"]];
        [query findObjectsInBackgroundWithBlock:callback];
    } else {
        // We need to fetch user first!
        [[PFUser currentUser] fetchInBackgroundWithBlock:^(id result, NSError *err){
            if (!err) {
                NSArray *solved = [PFUser currentUser][@"featuredSolved"];
                if (solved.count > 0) {
                    [query whereKey:@"objectId" notContainedIn:[PFUser currentUser][@"featuredSolved"]];
                    [query whereKey:@"endDate" greaterThan:[NSDate date]];
                } else {
                    [query findObjectsInBackgroundWithBlock:callback];
                }
            }
        }];
    }
}

- (void)fetchExtraCategoriesWithCallback:(void (^)(NSArray *, NSError *))callback {
    PFQuery *query = [PFQuery queryWithClassName:@"AdditionalPuzzleCategories"];
    [query addAscendingOrder:@"categoryName"];
    [query findObjectsInBackgroundWithBlock:callback];
}

- (void)fetchExtrasForCategoryNamed:(NSString *)name callback:(void (^)(NSArray *, NSError *))callback {
    PFQuery *query = [PFQuery queryWithClassName:name];
    [query addDescendingOrder:@"createdAt"];
    [query findObjectsInBackgroundWithBlock:callback];
}

- (void)userPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs {
    // We need to remove user from recipients as well as updating user statistics
    PFRelation *relation = [message relationForKey:@"recipients"];
    [relation removeObject:[PFUser currentUser]];
    [message saveEventually];
    
    [message pinInBackground];
    
    // Grab the dictionary
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"userCompleted"]];
    
    // Find the current user - or create one if there's no entry
    NSString *uid = [PFUser currentUser].objectId;
    NSArray *array;
    
    if ([[dict allKeys] containsObject:uid]) {
        // Add puzzle to solved
        array = [[dict objectForKey:uid] arrayByAddingObject:message.objectId];
    } else {
        // Create new array
        array = @[message.objectId];
        
    }
    
    [dict setObject:array forKey:uid];
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"userCompleted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Update user stats here!
    NSNumber *t = (NSNumber *)(message[@"timeLimit"]);
    NSString *difficulty = @"easy";
    switch (t.intValue) {
        case 0:
            difficulty = @"noLimit";
            break;
        case 15:
            difficulty = @"veryHard";
            break;
        case 30:
            difficulty = @"hard";
            break;
        case 45:
            difficulty = @"normal";
            break;
    }
    
    NSDictionary *params = @{@"puzzleDifficulty" : difficulty,
                             @"switches" : [NSNumber numberWithInt:count],
                             @"time" : [NSNumber numberWithFloat:secs]};
    
    [PFCloud callFunctionInBackground:@"statistics" withParameters:params block:^(NSString *result, NSError *error) {
        if (error) {
            NSLog(@"Update stats failed! Error: %@", [error userInfo][@"error"]);
        }
    }];
}

- (void)extraPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs {
    // Add the object id to user's completion list
    [[PFUser currentUser] addObject:message.objectId forKey:@"puzzlesSolved"];
    [[PFUser currentUser] saveEventually];
    
    // Update user stats here!
    NSDictionary *dict = @{@"puzzleDifficulty" : @"additional",
                           @"switches" : [NSNumber numberWithInt:count],
                           @"time" : [NSNumber numberWithFloat:secs]};
    
    [PFCloud callFunctionInBackground:@"statistics"
                       withParameters:dict
                                block:^(NSString *result, NSError *error) {
                                    if (error) {
                                        NSLog(@"Update stats failed! Error: %@", [error userInfo][@"error"]);
                                    }
                                }];
}

- (void)featuredPuzzleCompleted:(PFObject *)message switches:(int)count time:(float)secs {
    [message pinInBackground];
    
    // Save puzzle
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"featuredCompleted"]];
    
    // Find the current user - or create one if there's no entry
    NSString *uid = [PFUser currentUser].objectId;
    NSArray *array;
    
    if ([[dict allKeys] containsObject:uid]) {
        // Add puzzle to solved
        array = [[dict objectForKey:uid] arrayByAddingObject:message.objectId];
    } else {
        // Create new array
        array = @[message.objectId];
    }
    
    [dict setObject:array forKey:uid];
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"featuredCompleted"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Update user stats here!
    NSDictionary *params = @{@"puzzleId" : message.objectId,
                             @"steps" : [NSNumber numberWithInt:count],
                             @"time" : [NSNumber numberWithFloat:secs]};
    
    [PFCloud callFunctionInBackground:@"featuredStatistics"
                       withParameters:params
                                block:^(NSString *result, NSError *error) {
                                    if (error) {
                                        NSLog(@"Update stats failed! Error: %@", [error userInfo][@"error"]);
                                    }
                                }];
}

- (void)userPuzzleDeleted:(PFObject *)message {
    // We just need to remove user from recipients.
    PFRelation *relation = [message relationForKey:@"recipients"];
    [relation removeObject:[PFUser currentUser]];
    [message saveEventually];
}

- (void)fetchUserFriendRequests:(void (^)(NSArray *, NSError *))callback {
    PFRelation *requests = [PFUser currentUser][@"friendRequests"];
    [[requests query] findObjectsInBackgroundWithBlock:callback];
}

- (void)requestValidation:(NSString *)number callback:(void (^)(id, NSError *))block {
    // Workout user's locale and send approriate language parameter
    NSString *language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *locale = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    NSString *lang = [NSString stringWithFormat:@"%@-%@",language, locale];
    NSString *type = @"sms";
    
    // Build request
    NSDictionary *params = @{@"phoneNumber": number, @"language": lang, @"type": type};
    
    [PFCloud callFunctionInBackground:@"PVRequest" withParameters:params block:block];
}

- (void)sendPuzzle:(PFObject *)puzzle recipients:(NSArray *)rpIds callback:(void (^)(id success, NSError *))block {
    NSString *encoded = [[puzzle[@"photo"] getData] base64EncodedStringWithOptions:0];
    
    NSDictionary *params = @{@"recipientIds": rpIds,
                             @"photo": encoded,
                             @"caption1": puzzle[@"firstCaption"],
                             @"caption2": puzzle[@"secondCaption"],
                             @"previewIndex": puzzle[@"previewIndex"],
                             @"timeLimit": puzzle[@"timeLimit"],
                             @"puzzleSize": puzzle[@"puzzleSize"]};
    
    [PFCloud callFunctionInBackground:@"MSSend" withParameters:params block:block];
}

- (void)respondToRequests:(NSArray *)requests response:(FRRequestAction)action callback:(void (^)(id, NSError *))block {
    for (PFUser *user in requests) {
        NSDictionary *params = @{@"otherUserId":user.objectId, @"action":[NSNumber numberWithInt:action]};
        
        [PFCloud callFunctionInBackground:@"FRRequestHandle" withParameters:params block:block];
    }
}

- (void)deleteFriends:(NSArray *)friends callback:(void (^)(id, NSError *))block {
    for (PFUser *user in friends) {
        NSDictionary *params = @{@"otherUserId":user.objectId};
        
        [PFCloud callFunctionInBackground:@"FRRemoveFriend" withParameters:params block:block];
    }
}

- (void)flagUsers:(NSArray *)users callback:(void (^)(id, NSError *))block {
    for (PFUser *user in users) {
        NSDictionary *params = @{@"otherUserId":user.objectId};
        
        [PFCloud callFunctionInBackground:@"FRFlagFriend" withParameters:params block:block];
    }
}

- (void)checkNumbers:(NSArray *)numbers callback:(void (^)(id obj, NSError *err))block {
    NSDictionary *params = @{@"numbers": numbers};
    [PFCloud callFunctionInBackground:@"FRCheckContacts" withParameters:params block:block];
}

- (void)sendFriendRequests:(NSArray *)userIds callback:(void (^)(id, NSError *))block {
    BOOL first = YES;
    for (NSString *userId in userIds) {
        NSDictionary *params = @{@"recipients": @[userId]};
        
        if (first) {
            [PFCloud callFunctionInBackground:@"FRRequestSend" withParameters:params block:block];
            first = NO;
        } else {
            [PFCloud callFunctionInBackground:@"FRRequestSend" withParameters:params block:nil];
        }
        
    }
    
}

- (void)changeUserPassword:(NSString *)newPassword callback:(void (^)(BOOL, NSError *))block {
    [PFUser currentUser].password = newPassword;
    
    [[PFUser currentUser] saveInBackgroundWithBlock:block];
}

- (void)changeUserFirstName:(NSString *)firstName lastName:(NSString *)lastName callback:(void (^)(BOOL, NSError *))block {
    [PFUser currentUser][@"firstName"] = firstName;
    [PFUser currentUser][@"lastName"] = lastName;
    [PFUser currentUser][@"nameChangeCount"] = [NSNumber numberWithInt:[[PFUser currentUser][@"nameChangeCount"] intValue] + 1];
    [[PFUser currentUser] saveInBackgroundWithBlock:block];
}

@end
