//
//  PSAlertView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSLoadingView;

@interface PSLoadingView : UIViewController

/**
 Default initializer
 */
- (id)initWithTitle:(NSString *)title;

/**
 Dismiss the alert view with given index and option for animation.
 */
- (void)dismiss:(BOOL)isAnimated;
- (void)dismiss:(BOOL)animated completion:(void (^)(void))completion;

/**
 Present the alert within the current view controller.
 */
- (void)show:(BOOL)isAnimated;

@end
