//
//  PSSelectButton.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PSSelectButton;
@protocol PSSelectButtonDelegate

- (void)selectButton:(PSSelectButton *)button stateToggled:(BOOL)isSelected;

@end

@interface PSSelectButton : UIView
{
    UIView *gleam;
    UIView *fill;
    UIView *border;
    
    BOOL selectionState;
    
    // This is the foreground image we display, if any
    UIImageView *imageView;
}

/**
 Determining whether button is selected.
 */
- (void)setSelectionState:(BOOL)isSelected animated:(BOOL)shouldAnimate;

/**
 Get the selection state.
 */
- (BOOL)getSelectionState;

/**
 Change the foreground image of the select button.
 */
- (void)setImage:(UIImage *)image;

/**
 Controls whether border is visible
 */
- (void)showBorder:(BOOL)shouldShow;

@property (weak, nonatomic) IBOutlet id <PSSelectButtonDelegate> delegate;

@end
