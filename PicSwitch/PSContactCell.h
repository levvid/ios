//
//  PSContactCell。h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PSSelectButton.h"

@class PSContactCell;
@protocol PSContactCellDelegate

- (void)contactCell:(PSContactCell *)cell changedState:(BOOL)isSelected;

@end

@class PFUser;
@interface PSContactCell : UITableViewCell
<PSSelectButtonDelegate>
{
    IBOutlet UIView *bgView;
    IBOutlet UIView *barView;
    
    IBOutlet UILabel *nameLabel;
    
    IBOutlet PSSelectButton *selectButton;
}

/**
 This sets the cell to one of the predefined color styles. The only valid values
 for style are 0 (dark) and 1 (light).
 */
- (void)setColorStyleForBackground:(int)background bar:(int)bar;

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName;

- (void)setSelectionStatus:(BOOL)isSelected;

// The user this cell represents
@property (weak, nonatomic) PFUser *cellUser;
@property (weak, nonatomic) id <PSContactCellDelegate> delegate;



@end