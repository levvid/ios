//
//  PSChangePasswordViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 6/16/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSChangePasswordViewController.h"
#import <Parse/Parse.h>
#import "PSAlertView.h"
#import "ServiceManager.h"
#import "PSNotificationView.h"
#import "PSLoadingView.h"

@interface PSChangePasswordViewController ()
{
    IBOutlet UITextField *oldPassword;
    IBOutlet UITextField *newPassword;
    IBOutlet UITextField *confirmPassword;
}
@end

@implementation PSChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationItem setTitle:@"change password"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Set up buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self nextField:textField];
    
    return YES;
}

// iOS 7 workaround
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        UIFont *fontMain = [UIFont fontWithName:@"Montserrat-Bold" size:14];
        UIFont *fontPh = [UIFont fontWithName:@"Quicksand-Italic" size:14];
        
        if ([[[textField text] stringByReplacingCharactersInRange:range withString:string] isEqualToString:@""]) {
            [textField setFont:fontPh];
        } else {
            [textField setFont:fontMain];
        }
    }
    
    return YES;
}

- (void)nextField:(id)sender {
    if (sender == oldPassword) {
        [newPassword becomeFirstResponder];
    } else if (sender == newPassword) {
        [confirmPassword becomeFirstResponder];
    } else if (sender == confirmPassword) {
        [self confirmChange:self];
    }
}

- (IBAction)dismissKeyboard:(id)sender {
    [[self view] endEditing:YES];
}


- (IBAction)confirmChange:(id)sender {
    NSString *message = @"";
    if (oldPassword.text.length < 6) {
        message = @"Wrong password! Please re-enter your current password.";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if (![newPassword.text isEqualToString:confirmPassword.text]) {
        message = @"The new passwords don't match!";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else if (newPassword.text.length < 6) {
        message = @"The new password is too short!";
        [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    } else {
        PSLoadingView *loadingView = [[PSLoadingView alloc] init];
        [loadingView show:YES];
        
        // Login again to confirm password
        [PFUser logInWithUsernameInBackground:[PFUser currentUser].username password:oldPassword.text block:^(PFUser *user, NSError *err){
            if (user) {
                [[ServiceManager sharedManager] changeUserPassword:newPassword.text callback:^(BOOL success, NSError *err) {
                    [loadingView dismiss:YES completion:^{
                        NSString *message = @"";
                        if (success) {
                            [self.navigationController popViewControllerAnimated:YES];
                            
                            message = @"Password changed successfully!";
                            [[[PSNotificationView alloc] initWithMessage:message] show];
                        } else {
                            message = @"Network error! Please try again.";
                            [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                        }
                    }];
                }];
            } else {
                [loadingView dismiss:YES completion:^{
                    NSString *message = @"Wrong password! Please re-enter your current password.";
                    [[[PSAlertView alloc] initWithMessage:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }];
            }
        }];        
    }
}


/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
