//
//  PSTransparentView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/11/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 This is a transparent view in the sense it allows all user interactions to be
 passed down to the views below.
 */
@interface PSTransparentView : UIView

@end
