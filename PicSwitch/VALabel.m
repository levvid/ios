//
//  VALabel.m
//  Big Red Ambition: 161 things to do at Cornell
//
//  Created by Jiacong Xu on 9/12/14.
//  Copyright (c) 2014 Jiacong Xu. All rights reserved.
//

#import "VALabel.h"

@implementation VALabel

@synthesize verticalAlignment;

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        verticalAlignment = UIControlContentVerticalAlignmentTop;
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        verticalAlignment = UIControlContentVerticalAlignmentTop;
    }
    
    return self;
}


- (void)drawTextInRect:(CGRect)rect
{
	if (verticalAlignment == UIControlContentVerticalAlignmentTop ||
	   verticalAlignment == UIControlContentVerticalAlignmentBottom) {
		//	If one line, we can just use the lineHeight, faster than querying sizeThatFits
		const CGFloat height = ((self.numberOfLines == 1) ? ceilf(self.font.lineHeight) : [self sizeThatFits:self.frame.size].height);
		
		if (height < self.frame.size.height)
			rect.origin.y = ((self.frame.size.height - height) / 2.0f) * ((verticalAlignment == UIControlContentVerticalAlignmentTop) ? -1.0f : 1.0f);
	}
    
	[super drawTextInRect:rect];
}

- (void)setVerticalAlignment:(UIControlContentVerticalAlignment)va {
	verticalAlignment = va;
	[self setNeedsDisplay];
}

@end
