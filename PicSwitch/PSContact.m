//
//  PSContact.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/3/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//


#import "PSContact.h"

@implementation PSContact

@synthesize firstName, lastName, phoneNumbers, isFriend, onPicswitch, fullName;

- (NSString *)fullName {
    return [NSString stringWithFormat:@"%@ %@", firstName, lastName];
}

@end