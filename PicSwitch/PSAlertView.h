//
//  PSAlertView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 4/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSAlertView;
@protocol PSAlertViewDelegate

- (void)alertView:(PSAlertView *)alertView clickedButtonAtIndex:(NSInteger)index;

@end

@interface PSAlertView : UIViewController

@property (weak, nonatomic) id <PSAlertViewDelegate> delegate;

/**
 Default initializer.
 */
- (instancetype)initWithMessage:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles, ...;

/**
 Dismiss the alert view with given index and option for animation.
 */
- (void)dismissWithClickedButtonIndex:(NSInteger)index animated:(BOOL)isAnimated;

/**
 Present the alert within the current view controller.
 */
- (void)show;

@end
