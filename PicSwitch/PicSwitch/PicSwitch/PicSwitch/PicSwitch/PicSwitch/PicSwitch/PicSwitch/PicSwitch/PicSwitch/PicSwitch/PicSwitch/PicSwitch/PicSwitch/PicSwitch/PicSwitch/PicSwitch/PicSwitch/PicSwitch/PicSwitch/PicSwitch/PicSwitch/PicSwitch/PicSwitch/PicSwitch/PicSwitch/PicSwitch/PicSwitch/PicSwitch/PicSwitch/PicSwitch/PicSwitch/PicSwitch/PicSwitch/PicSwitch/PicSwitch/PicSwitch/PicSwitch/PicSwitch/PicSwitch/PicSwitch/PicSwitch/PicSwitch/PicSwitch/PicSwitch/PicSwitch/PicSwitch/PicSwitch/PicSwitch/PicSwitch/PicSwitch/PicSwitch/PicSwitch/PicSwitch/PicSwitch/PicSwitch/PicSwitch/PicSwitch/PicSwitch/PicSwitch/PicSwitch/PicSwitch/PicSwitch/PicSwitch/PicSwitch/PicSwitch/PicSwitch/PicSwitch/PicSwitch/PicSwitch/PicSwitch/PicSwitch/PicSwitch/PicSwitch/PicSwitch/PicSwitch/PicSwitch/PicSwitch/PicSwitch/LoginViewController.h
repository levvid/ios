//
//  ViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 The first screen of the app, this handles the login screen.
 */

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
{
    // Facebook Disclaimer label
    IBOutlet UILabel *fbDisclaimerLabel;
}

@end

