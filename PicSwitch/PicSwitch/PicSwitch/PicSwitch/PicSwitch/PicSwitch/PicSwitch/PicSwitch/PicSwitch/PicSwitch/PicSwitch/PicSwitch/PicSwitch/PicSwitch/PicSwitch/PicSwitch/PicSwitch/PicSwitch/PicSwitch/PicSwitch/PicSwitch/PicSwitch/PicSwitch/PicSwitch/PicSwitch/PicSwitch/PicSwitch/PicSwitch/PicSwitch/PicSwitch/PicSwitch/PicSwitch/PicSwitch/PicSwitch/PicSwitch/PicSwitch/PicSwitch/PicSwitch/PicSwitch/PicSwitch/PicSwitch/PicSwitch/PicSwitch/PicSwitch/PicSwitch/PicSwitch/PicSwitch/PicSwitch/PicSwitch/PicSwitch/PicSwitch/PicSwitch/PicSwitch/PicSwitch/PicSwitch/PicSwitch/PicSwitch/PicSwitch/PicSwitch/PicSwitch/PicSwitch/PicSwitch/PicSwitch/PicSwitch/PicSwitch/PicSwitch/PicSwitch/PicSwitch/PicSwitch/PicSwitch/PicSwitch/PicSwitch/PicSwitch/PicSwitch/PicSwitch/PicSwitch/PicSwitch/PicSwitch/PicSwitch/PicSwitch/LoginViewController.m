//
//  LoginViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Hides Facebook disclaimer if we don't have enough screen space.
    if ([UIScreen mainScreen].bounds.size.height < 568) {
        fbDisclaimerLabel.hidden = YES;
    }
}


#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
