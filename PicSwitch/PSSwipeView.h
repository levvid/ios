//
//  PSSwipeView.h
//  PicSwitch
//
//  Created by Jiacong Xu on 2/7/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSSwipeView;
@protocol PSSwipeViewDelegate <NSObject>

/**
 This is called when user completed the swipe.
 */
- (void)swipeComplete:(PSSwipeView *)swipeView;

@end

@interface PSSwipeView : UIView

@property (nonatomic, weak) IBOutlet id <PSSwipeViewDelegate> delegate;

@end
