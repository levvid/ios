//
//  PSDiscreteProgressControl.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/22/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

/**
 This class represents an instance of a small progress control, where progress
 is depicted by circles filling up. Add this to a view as a subview and set the
 initial number of total circles to get started. This will automatically resize
 its frame according to the size of the circles.
 */

#import <UIKit/UIKit.h>

@interface PSDiscreteProgressControl : UIView

@property (nonatomic) CGFloat circleSize;
@property (nonatomic) CGFloat circleSpacing;
@property (nonatomic) NSInteger totalNumber;
@property (nonatomic) NSInteger progress;
@property (strong, nonatomic) UIColor *lineColor;
@property (strong, nonatomic) UIColor *fillColor;
@property (strong, nonatomic) UIColor *highlightColor;

- (void)setProgress:(NSInteger)progress animated:(BOOL)isAnimated;

@end
