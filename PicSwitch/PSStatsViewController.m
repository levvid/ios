//
//  PSStatsViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 4/26/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSStatsViewController.h"
#import <Parse/Parse.h>

@interface PSStatsViewController ()
{
    IBOutlet UILabel *friendCountLabel;
    IBOutlet UILabel *firstPlaceLabel;
    IBOutlet UILabel *secondPlaceLabel;
    IBOutlet UILabel *thirdPlaceLabel;
    IBOutlet UILabel *ownNameLabel;
    IBOutlet UILabel *ownPlaceLabel;
    
    IBOutlet NSLayoutConstraint *noLimitConstraint;
    IBOutlet NSLayoutConstraint *easyConstraint;
    IBOutlet NSLayoutConstraint *normalConstraint;
    IBOutlet NSLayoutConstraint *hardConstraint;
    IBOutlet NSLayoutConstraint *veryHardConstraint;
    IBOutlet UILabel *noLimitLabel;
    IBOutlet UILabel *easyLabel;
    IBOutlet UILabel *normalLabel;
    IBOutlet UILabel *hardLabel;
    IBOutlet UILabel *veryHardLabel;
    IBOutlet UILabel *totalPtsLabel;
    
    IBOutlet UILabel *avgSwitches;
    IBOutlet UILabel *avgTime;
    IBOutlet UILabel *additionalSolved;
    IBOutlet UILabel *totalAdditionalPtsLabel;
    
    IBOutlet UILabel *avgSwitchesFeatured;
    IBOutlet UILabel *avgTimeFeatured;
    IBOutlet UILabel *featuredSolved;
    IBOutlet UILabel *totalFeaturedPtsLabel;
}
@end

@implementation PSStatsViewController

- (void)awakeFromNib {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self fetchLeaderboard];
}

NSInteger comparator(PFUser *a, PFUser *b, void *context) {
    int pt1 = [a[@"stats"][@"points"] intValue];
    int pt2 = [b[@"stats"][@"points"] intValue];
    
    if (pt1 == pt2) {
        return NSOrderedSame;
    } else if (pt1 > pt2) {
        return NSOrderedAscending;
    } else {
        return NSOrderedDescending;
    }
}

- (void)fetchLeaderboard {
    // First retrieve user points
    [(PFObject *)([PFUser currentUser][@"stats"]) fetchInBackgroundWithBlock:^(id result, NSError *err){
        if (!err && result != nil) {
            // Personal stats
            
            NSNumber *pts = [PFUser currentUser][@"stats"][@"points"];
            ownNameLabel.text = [NSString stringWithFormat:@"You - %i pts", pts.intValue];
            
            // Now we populate numbers section. We mark 0 as width 5 and max as width 175
            [[self view] layoutIfNeeded];
            
            // First we need to find the maximum out of the whole set.
            int noLimit = [[PFUser currentUser][@"stats"][@"noLimit"] intValue];
            int easy = [[PFUser currentUser][@"stats"][@"easy"] intValue];
            int normal = [[PFUser currentUser][@"stats"][@"normal"] intValue];
            int hard = [[PFUser currentUser][@"stats"][@"hard"] intValue];
            int veryHard = [[PFUser currentUser][@"stats"][@"veryHard"] intValue];
            int friendPt = noLimit * 1 + easy * 2 + normal * 3 + hard * 4 + veryHard * 5;
            int max = MAX(MAX(MAX(MAX(noLimit, easy), normal), hard), veryHard);
            
            noLimitConstraint.constant = 5 + 170 * noLimit / (float)max;
            easyConstraint.constant = 5 + 170 * easy / (float)max;
            normalConstraint.constant = 5 + 170 * normal / (float)max;
            hardConstraint.constant = 5 + 170 * hard / (float)max;
            veryHardConstraint.constant = 5 + 170 * veryHard / (float)max;
            
            noLimitLabel.text = [NSString stringWithFormat:@"%i", noLimit];
            easyLabel.text = [NSString stringWithFormat:@"%i", easy];
            normalLabel.text = [NSString stringWithFormat:@"%i", normal];
            hardLabel.text = [NSString stringWithFormat:@"%i", hard];
            veryHardLabel.text = [NSString stringWithFormat:@"%i", veryHard];
            
            totalPtsLabel.text = [NSString stringWithFormat:@"total = %i pt", friendPt];
            
            [UIView animateWithDuration:0.8 delay:0 usingSpringWithDamping:0.3 initialSpringVelocity:0.2 options:UIViewAnimationOptionCurveLinear animations:^{
                [[self view] layoutIfNeeded];
            } completion:nil];
            
            // Now comes featured
            int featured = [[PFUser currentUser][@"stats"][@"numberOfFeatured"] intValue];
            featuredSolved.text = [NSString stringWithFormat:@"%i", featured];
            float fat = [[PFUser currentUser][@"stats"][@"featuredAverageTime"] floatValue];
            avgTimeFeatured.text = [NSString stringWithFormat:@"%.1f", fat];
            float fas = [[PFUser currentUser][@"stats"][@"featuredAverageSteps"] floatValue];
            avgSwitchesFeatured.text = [NSString stringWithFormat:@"%.1f", fas];
            totalFeaturedPtsLabel.text = [NSString stringWithFormat:@"total = %i pt", featured * 5];
            
            // Finally the additional puzzle section
            int add = [[PFUser currentUser][@"stats"][@"numberOfAdditional"] intValue];
            additionalSolved.text = [NSString stringWithFormat:@"%i", add];
            float at = [[PFUser currentUser][@"stats"][@"averageTime"] floatValue];
            avgTime.text = [NSString stringWithFormat:@"%.1f", at];
            float as = [[PFUser currentUser][@"stats"][@"averageSteps"] floatValue];
            avgSwitches.text = [NSString stringWithFormat:@"%.1f", as];
            totalAdditionalPtsLabel.text = [NSString stringWithFormat:@"total = %i pt", add];
            
            // Fetch all user stats
            PFQuery *query = [[[PFUser currentUser] relationForKey:@"friends"] query];
            
            [query includeKey:@"stats"];
            
            [query findObjectsInBackgroundWithBlock:^(id obj, NSError *err){
                if (!err) {
                    // Retrieve points from users
                    NSArray *users = (NSArray *)obj;
                    users = [users arrayByAddingObject:[PFUser currentUser]];
                    
                    // Sort and display
                    users = [users sortedArrayUsingFunction:&comparator context:nil];
                    
                    PFObject *first = [users objectAtIndex:0];
                    firstPlaceLabel.text = [NSString stringWithFormat:@"%@ %@ - %i pts", first[@"firstName"], first[@"lastName"], [first[@"stats"][@"points"] intValue]];
                    
                    if (users.count > 1) {
                        PFObject *second = [users objectAtIndex:1];
                        secondPlaceLabel.text = [NSString stringWithFormat:@"%@ %@ - %i pts", second[@"firstName"], second[@"lastName"], [second[@"stats"][@"points"] intValue]];
                    }
                    
                    if (users.count > 2) {
                        PFObject *third = [users objectAtIndex:2];
                        thirdPlaceLabel.text = [NSString stringWithFormat:@"%@ %@ - %i pts", third[@"firstName"], third[@"lastName"], [third[@"stats"][@"points"] intValue]];
                    }
                    
                    friendCountLabel.text = [NSString stringWithFormat:@"highest ranking friends (of %lu)", (unsigned long)users.count];
                    ownPlaceLabel.text = [NSString stringWithFormat:@"%lu", (unsigned long)[users indexOfObject:[PFUser currentUser]] + 1];
                } else {
                    NSLog(@"Network error!");
                }
            }];
        } else {
            NSLog(@"Network error!");
        }
    }];
}

- (void)configureNavigation {
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationItem setTitle:@"Statistics"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
}

- (UINavigationController *)navigationController {
    UINavigationController *nav = self.parentViewController.navigationController;
    return nav;
}

- (UINavigationItem *)navigationItem {
    return [self parentViewController].navigationItem;
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return nil;
    
    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, height - 10)];
    
    switch (section) {
        case 0:
            label.text = @"how do you stack up?";
            break;
        case 1:
            label.text = @"numbers don't lie";
            break;
        default:
            break;
    }
    
    label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
    label.textColor = [UIColor whiteColor];
    
    [container addSubview:label];
    
    return container;
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
