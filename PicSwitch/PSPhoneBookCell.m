//
//  PSPhoneBookCell.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/2/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSPhoneBookCell.h"
#import "PSSelectButton.h"

@implementation PSPhoneBookCell
@synthesize delegate;

- (void)awakeFromNib {
    bgView.layer.cornerRadius = 8;
    inviteButtons = @[];
    // isExpanded = NO;
}

- (void)prepareForReuse {
    createdUI = NO;
    inviteButtons = @[];
}

- (void)setColorStyleForBackground:(int)background bar:(int)bar {
    switch (background) {
        case 0:
            [bgView setBackgroundColor:[UIColor colorWithWhite:0.25 alpha:1.0]];
            break;
        case 1:
            [bgView setBackgroundColor:[UIColor clearColor]];
        default:
            break;
    }
    
    switch (bar) {
        case 0:
            [barView setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1]];
            break;
        case 1:
            [barView setBackgroundColor:[UIColor colorWithHue:0.5 saturation:0.6 brightness:0.6 alpha:1]];
        default:
            break;
    }
}

- (void)setFirstName:(NSString *)firstName lastName:(NSString *)lastName {
    UIFont *font1 = [UIFont fontWithName:@"Montserrat-light" size:14.0];
    UIFont *font2 = [UIFont fontWithName:@"Montserrat-regular" size:14.0];
    NSDictionary *dict1 = @{NSFontAttributeName : font1};
    NSDictionary *dict2 = @{NSFontAttributeName : font2};
    NSMutableAttributedString *s1 = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", firstName? firstName : @"<no first name>"] attributes:dict1];
    NSMutableAttributedString *s2 = [[NSMutableAttributedString alloc] initWithString:lastName? lastName : @"<no last name>" attributes:dict2];
    [s1 appendAttributedString:s2];
    [nameLabel setAttributedText:s1];
}

- (IBAction)expandButtonPressed:(UIButton *)sender {
    // [self shouldExpand:!isExpanded animated:YES];
    // [[NSNotificationCenter defaultCenter] postNotificationName:@"PSPhoneBookCellHeightChanged" object:self];
}

- (void)shouldExpand:(BOOL)expand animated:(BOOL)animated {
    /*
    if (expand == isExpanded)
        return;
    
    isExpanded = expand;
    
    // Fade out icons
    [UIView animateWithDuration:animated? 0.3 : 0 animations:^{
        mainIsFriendIcon.alpha = !expand;
        mainOnlineIcon.alpha = !expand;
    }];
     */
}

// This sets the numbers
- (void)setNumbers:(NSArray *)numbers {
    numbersArray = numbers;
    
    [self createUI];
}

// This sets selected states. Call this after setNumbers
- (void)setSelectedNumbers:(NSSet *)selected {
    for (int i = 0; i < numbersArray.count; i++) {
        NSString *number = [numbersArray objectAtIndex:i];
        NSString *filtered = [[number componentsSeparatedByCharactersInSet:
                               [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                              componentsJoinedByString:@""];
        
        BOOL shouldSelect = [selected containsObject:filtered];
        [[inviteButtons objectAtIndex:i] setSelectionState:shouldSelect animated:YES];
    }
}

- (void)setOnPicswitch:(NSArray *)picswitch friend:(NSArray *)friends {
    for (int i = 0; i < numbersArray.count; i++) {
        NSString *number = [numbersArray objectAtIndex:i];
        NSString *filtered = [[number componentsSeparatedByCharactersInSet:
                               [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                              componentsJoinedByString:@""];
        
        BOOL isFriend = [friends containsObject:filtered];
        BOOL onPicswitch = [picswitch containsObject:filtered];
        
        [[inviteButtons objectAtIndex:i] setUserInteractionEnabled:!isFriend];
        [[inviteButtons objectAtIndex:i] showBorder:!isFriend];
        
        if (onPicswitch) {
            [(PSSelectButton *)[inviteButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"AddButton"]];
        } else if (isFriend) {
            [(PSSelectButton *)[inviteButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"FriendsIcon"]];
        } else {
            [(PSSelectButton *)[inviteButtons objectAtIndex:i] setImage:[UIImage imageNamed:@"MailButtonWhite"]];
        }
    }
}

// Using the numbers, create extra UI.
// Only call once!
- (void)createUI {
    if (createdUI)
        return;
    
    createdUI = YES;
    
    for (UIView *sbv in numViewArray) {
        [sbv removeFromSuperview];
    }
    
    numViewArray = [NSMutableArray array];
    inviteButtons = [NSMutableArray array];
    // For each number, we create a button, an icon to indicate friendship, and
    // an icon to indicate whether on picswitch or not.
    CGFloat height = 36;
    CGFloat width = bgView.bounds.size.width;
    CGFloat numHeight = 25;
    
    for (int i = 0; i < numbersArray.count; i++) {
        UIView *current = [[UIView alloc] init];
        NSString *number = [numbersArray objectAtIndex:i];
        UIButton *numberButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, width, numHeight)];
        [[numberButton titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Light" size:15]];
        [numberButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [numberButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [numberButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
        
        // Format number and set as title
        NSString *formatted = number;
        
        [numberButton setTitle:formatted forState:UIControlStateNormal];
        
        [current addSubview:numberButton];
        
        // Add icons to indicate friendship, etc.
        // If we are friends, then we display friend icon. Default is to invite
        // by text (then it get updated from database)
        PSSelectButton *inviteButton = [[PSSelectButton alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        inviteButtons = [inviteButtons arrayByAddingObject:inviteButton];
        inviteButton.delegate = self;
        [inviteButton setImage:[UIImage imageNamed:@"MailButtonWhite"]];
        [current addSubview:inviteButton];
        
        // Set up auto constraints
        inviteButton.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSDictionary *iconsDictionary = @{@"invite":inviteButton};
        NSArray *inviteH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[invite(32)]-0-|"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:iconsDictionary];
        NSArray *inviteV = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[invite(32)]"
                                                                            options:0
                                                                            metrics:nil
                                                                              views:iconsDictionary];
        NSLayoutConstraint *inviteY = [NSLayoutConstraint constraintWithItem:inviteButton
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:0
                                                                      toItem:current
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1
                                                                    constant:0];
        [current addConstraints:inviteH];
        [current addConstraints:inviteV];
        [current addConstraint:inviteY];
        
        // Inserting view
        [bgView addSubview:current];
        
        // Set up auto constraints
        current.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *viewsDictionary = @{@"self":current};
        NSArray *constraintV = [NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|-%f-[self(%f)]", height + numHeight * i, numHeight]
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
        NSArray *constraintH = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[self]-0-|"
                                                                       options:0
                                                                       metrics:nil
                                                                         views:viewsDictionary];
        [bgView addConstraints:constraintV];
        [bgView addConstraints:constraintH];
        
        [numViewArray addObject:current];
    }
}

- (NSInteger)numbersCount {
    return numbersArray.count;
}

- (BOOL)isExpanded {
    return isExpanded;
}

- (void)selectButton:(PSSelectButton *)button stateToggled:(BOOL)isSelected {
    // Find the number associated with the state
    NSString *number = [numbersArray objectAtIndex:[inviteButtons indexOfObject:button]];
    NSString *filtered = [[number componentsSeparatedByCharactersInSet:
                               [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                              componentsJoinedByString:@""];
    
    if (isSelected) {
        [delegate cell:self didSelectNumber:filtered];
    } else {
        [delegate cell:self didUnselectNumber:filtered];
    }
}

@end
