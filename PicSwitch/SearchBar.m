//
//  SearchBar.m
//  Big Red Ambition: 161 things to do at Cornell
//
//  Created by Jiacong Xu on 9/18/14.
//  Copyright (c) 2014 Jiacong Xu. All rights reserved.
//

#import "SearchBar.h"

@implementation SearchBar

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        for(UIView *subView in self.subviews) {
            if([subView conformsToProtocol:@protocol(UITextInputTraits)]) {
                [(UITextField *)subView setReturnKeyType: UIReturnKeyDone];
            } else {
                for(UIView *subSubView in [subView subviews]) {
                    if([subSubView conformsToProtocol:@protocol(UITextInputTraits)]) {
                        [(UITextField *)subSubView setReturnKeyType: UIReturnKeyDone];
                    }
                }      
            }
        }
        
        NSDictionary *attributes =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [UIColor whiteColor], NSForegroundColorAttributeName,
         nil];
        [[UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil]
         setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
    
    return self;
}

@end
