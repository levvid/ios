//
//  CameraViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 2/14/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "PSAlertView.h"

@class PSPuzzleView;

@interface CameraViewController : UIViewController
<UITextFieldDelegate,
UINavigationControllerDelegate,
UIImagePickerControllerDelegate>
{
    // Handles all accessory views
    IBOutlet UIImageView *camOverlay;
    IBOutlet UITextField *caption1;
    IBOutlet UITextField *caption2;
    IBOutlet NSLayoutConstraint *timerConstraint;
    
    IBOutlet UIButton *timerButton1;
    IBOutlet UIButton *timerButton2;
    IBOutlet UIButton *timerButton3;
    IBOutlet UIButton *timerButton4;
    IBOutlet UIButton *timerButton5;
    
    IBOutlet UIView *takePictureView;
    IBOutlet UIScrollView *postPictureView;
    IBOutlet PSPuzzleView *previewImage;
    IBOutlet NSLayoutConstraint *previewConstraint;
    IBOutlet NSLayoutConstraint *scrollConstraint;
    
    IBOutlet UIView *sendButton;
    
    /**
     States: handling the individual states of the camera view
     0: no picture taken yet
     1: user took a picture
     */
    int state;
    
    BOOL isBackCamera;
    
    BOOL timerOpen;
    
    int timerOption;
    
    BOOL isEditingCaption2;
}

/// Recipients that get send to the contact view controller to be pre-selected.
@property (strong, nonatomic) NSArray *recipients;

- (IBAction)capturePic:(id)sender;
- (IBAction)changeTimer:(id)sender;
- (IBAction)retakePicture:(id)sender;
- (IBAction)chooseContacts:(id)sender;
- (IBAction)toggleFlash:(UIButton *)sender;

@end
