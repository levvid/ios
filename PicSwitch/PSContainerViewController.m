//
//  PSContainerViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 6/16/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSContainerViewController.h"

@interface PSContainerViewController ()

@end

@implementation PSContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    for (UIViewController *controller in self.childViewControllers) {
        if ([controller respondsToSelector:@selector(configureNavigation)]) {
            [controller performSelector:@selector(configureNavigation)];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
