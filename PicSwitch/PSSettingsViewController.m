//
//  PSSettingsViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/24/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSSettingsViewController.h"
#import <Parse/Parse.h>
#import "PSAlertView.h"
#import "PSDocViewController.h"

@interface PSSettingsViewController ()
{
    IBOutlet UISwitch *soundSwitch;
    IBOutlet UISwitch *savePicSwitch;
}
@end

@implementation PSSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Setting up navigation appearance
    
    // Set up sound icon
    NSNumber *on = [[NSUserDefaults standardUserDefaults] objectForKey:@"shouldMuteSound"];
    [soundSwitch setOn:![on boolValue]];
    
    on = [[NSUserDefaults standardUserDefaults] objectForKey:@"shouldSaveOutgoing"];
    [savePicSwitch setOn:[on boolValue]];
}

- (void)configureNavigation {
    [self.navigationItem setTitle:@"settings"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    // Set up buttons
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
}

- (UINavigationController *)navigationController {
    UINavigationController *nav = self.parentViewController.navigationController;
    return nav;
}

- (UINavigationItem *)navigationItem {
    return [self parentViewController].navigationItem;
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([self tableView:tableView numberOfRowsInSection:section] == 0)
        return nil;
    
    CGFloat height = [self tableView:tableView heightForHeaderInSection:section];
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 300, height - 10)];
    
    switch (section) {
        case 0:
            label.text = @"account";
            break;
        case 1:
            label.text = @"preferences";
            break;
        case 2:
            label.text = @"others";
            break;
        default:
            break;
    }
    
    label.font = [UIFont fontWithName:@"Montserrat-Regular" size:18];
    label.textColor = [UIColor whiteColor];
    
    [container addSubview:label];
    
    return container;
}

- (void)logout {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [PFUser logOut];
}

- (IBAction)soundSwitched:(UISwitch *)sender {
    NSNumber *on = [NSNumber numberWithBool:![sender isOn]];
    [[NSUserDefaults standardUserDefaults] setObject:on forKey:@"shouldMuteSound"];
}

- (IBAction)savePicSwitched:(UISwitch *)sender {
    NSNumber *on = [NSNumber numberWithBool:[sender isOn]];
    [[NSUserDefaults standardUserDefaults] setObject:on forKey:@"shouldSaveOutgoing"];
}

- (void)openFAQ {
    NSURL *url = [NSURL URLWithString:@"http://www.picswitchapp.com/faqs"];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)openTerms {
    NSURL *url = [NSURL URLWithString:@"http://www.picswitchapp.com/terms"];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)openPrivacy {
    NSURL *url = [NSURL URLWithString:@"http://www.picswitchapp.com/privacy"];
    [[UIApplication sharedApplication] openURL:url];
}

- (void)openContestRules {
    /*
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FeaturedRules" ofType:@"txt"];
    NSString *string = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    [[[PSAlertView alloc] initWithMessage:string delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
     */
    
    PSDocViewController *docController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"DVC"];
    [docController setupWithTitle:@"Contest Rules" content:[PSDocViewController featuredRules]];
    [self.navigationController pushViewController:docController animated:YES];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2 && indexPath.row == 0) {
        // Open FAQ
        [self openFAQ];
    } else if (indexPath.section == 2 && indexPath.row == 1) {
        // Open terms
        [self openTerms];
    } else if (indexPath.section == 2 && indexPath.row == 2) {
        // Open privacy
        [self openPrivacy];
    } else if (indexPath.section == 2 && indexPath.row == 3) {
        // Open contest rules
        [self openContestRules];
    } else if (indexPath.section == 2 && indexPath.row == 4) {
        [self logout];
    }
}


/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
