//
//  PSSwipeView.m
//  PicSwitch
//
//  Created by Jiacong Xu on 2/7/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSSwipeView.h"

@implementation PSSwipeView
{
    UIButton *knob;
    UIView *fill;
    UIView *gleam;
}

@synthesize delegate;

- (void)awakeFromNib {
    [self setup];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

- (void)setup {
    // Set up the slider
    knob = [[UIButton alloc] initWithFrame:CGRectMake(1, 1, self.bounds.size.height - 2, self.bounds.size.height - 2)];
    [knob addTarget:self action:@selector(dragged:withEvent:) forControlEvents:UIControlEventTouchDragInside];
    [knob addTarget:self action:@selector(released:withEvent:) forControlEvents:UIControlEventTouchDragOutside];
    [knob addTarget:self action:@selector(released:withEvent:) forControlEvents:UIControlEventTouchUpInside];
    [knob addTarget:self action:@selector(released:withEvent:) forControlEvents:UIControlEventTouchUpOutside];
    knob.backgroundColor = [UIColor colorWithWhite:0.25 alpha:1.0];
    knob.layer.cornerRadius = 5.0;
    knob.layer.borderColor = [[UIColor colorWithWhite:0.6 alpha:1.0] CGColor];
    knob.layer.borderWidth = 3.0;
    
    gleam = [[UIView alloc] initWithFrame:CGRectMake(0, 0, knob.frame.size.width, 5)];
    gleam.layer.cornerRadius = 5.0;
    gleam.backgroundColor = [UIColor colorWithHue:0.5 saturation:0.4 brightness:0.6 alpha:1.0];
    
    fill = [[UIView alloc] initWithFrame:CGRectMake(0, knob.frame.size.height, knob.frame.size.width, 0)];
    fill.layer.cornerRadius = 5.0;
    fill.backgroundColor = [UIColor colorWithHue:0.5 saturation:0.9 brightness:0.4 alpha:1.0];
    fill.clipsToBounds = YES;
    [fill addSubview:gleam];
    [knob addSubview:fill];
    [self addSubview:knob];
}

// Handling button dragging
- (void)dragged:(id)sender withEvent:(UIEvent *)event {
    [self setNeedsDisplay];
    CGPoint curr = [[[event allTouches] anyObject] locationInView:self];
    CGPoint prev = [[[event allTouches] anyObject] previousLocationInView:self];
    CGPoint diff = CGPointMake(curr.x - prev.x, 0);
    CGFloat w = self.bounds.size.width;
    CGFloat h = self.bounds.size.height;
    CGFloat percentage = knob.frame.origin.x / (w - knob.frame.size.width);
    
    knob.center = CGPointMake(MAX(h / 2, MIN(w - h / 2, knob.center.x + diff.x)), knob.center.y);
    fill.frame = CGRectMake(0, knob.frame.size.height * (1 - percentage), knob.frame.size.width, knob.frame.size.height * percentage);
}

- (void)released:(id)sender withEvent:(UIEvent *)event {
    CGFloat h = self.bounds.size.height;
    
    if (knob.center.x > self.bounds.size.width * 0.85) {
        [UIView animateWithDuration:0.3 animations:^{
            self.alpha = 0.0;
        } completion:^(BOOL finished){
            [delegate swipeComplete:self];
            self.alpha = 1.0;
            knob.center = CGPointMake(h/2, h/2);
            fill.frame = CGRectMake(0, knob.frame.size.height, knob.frame.size.width, 0);
        }];
    } else {
        [UIView animateWithDuration:0.3 delay:0.0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            knob.center = CGPointMake(h/2, h/2);
            fill.frame = CGRectMake(0, knob.frame.size.height, knob.frame.size.width, 0);
        } completion:nil];
    }
}

// Custom draw rect
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    // Count how many lines we can draw (remember the triangle at the end)
    int lines = 10;
    CGFloat separation = self.bounds.size.width / (lines + 2);
    
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineJoin(context, kCGLineJoinRound);
    CGContextSetLineWidth(context, 3.0);
    
    for (int i = 1; i <= lines; i++) {
        CGContextMoveToPoint(context, separation * i, 7);
        CGContextAddLineToPoint(context, separation * i, self.bounds.size.height - 7);
        [[UIColor colorWithWhite:1.0 alpha:0.2 + 0.1 * i / lines] set];
        CGContextDrawPath(context, kCGPathStroke);
    }
    
    // triangle
    CGContextMoveToPoint(context, separation * (lines + 1), 5);
    CGContextAddLineToPoint(context, separation * (lines + 1), self.bounds.size.height - 5);
    CGContextAddLineToPoint(context, separation * (lines + 1.5), self.bounds.size.height / 2);
    CGContextClosePath(context);
    [[UIColor colorWithWhite:1.0 alpha:0.3] set];
    CGContextDrawPath(context, kCGPathStroke);
    
    
}

@end
