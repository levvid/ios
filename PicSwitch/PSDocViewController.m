//
//  PSDocViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 1/28/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSDocViewController.h"

@interface PSDocViewController ()
{
    NSString *navTitle;
    NSString *docContent;
}

@end

@implementation PSDocViewController

+ (NSString *)terms {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Terms" ofType:@"txt"];
    NSString *terms = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    return terms;
}

+ (NSString *)privacy {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Privacy" ofType:@"txt"];
    NSString *privacy = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    return privacy;
}

+ (NSString *)featuredRules {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"FeaturedRules" ofType:@"txt"];
    NSString *rules = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    
    return rules;
}


+ (NSString *)faq {
    return @"Picswitch FAQ - ask Robbie!";
}

- (void)awakeFromNib {
    // Set up the title and content text attributes here
    UIColor *color = [UIColor whiteColor];
    UIFont *titleFont = [UIFont fontWithName:@"Quicksand-Italic" size:16];
    UIFont *contentFont = [UIFont fontWithName:@"Quicksand-Italic" size:14];
    NSMutableParagraphStyle *center = [[NSMutableParagraphStyle alloc] init];
    [center setAlignment:NSTextAlignmentCenter];
    
    titleAttributes = @{NSForegroundColorAttributeName:color, NSFontAttributeName:titleFont, NSParagraphStyleAttributeName:center};
    contentAttributes = @{NSForegroundColorAttributeName:color, NSFontAttributeName:contentFont};
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationItem setTitle:navTitle];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    // Load content
    mainDisplay.text = docContent;
}

- (void)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupWithTitle:(NSString *)title content:(NSString *)string {
    navTitle = title;
    docContent = string;
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
