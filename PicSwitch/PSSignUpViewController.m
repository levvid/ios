//
//  PSSignUpViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 3/22/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSSignUpViewController.h"
#import "PSDiscreteProgressControl.h"
#import <Parse/Parse.h>
#import "PSTextField.h"
#import "PSDatePickerView.h"
#import "PSGenderPickerView.h"
#import "PSAlertView.h"
#import "PSPhoneNumberField.h"
#import "ServiceManager.h"
#import "PSScrollViewController.h"
#import "PSLoadingView.h"
#import "PSTutorialViewController.h"

int const RESEND_WAIT_INTERVAL = 300;

typedef NS_ENUM(NSInteger, Gender)  {
    Male = 1,
    Female,
    Undisclosed
};

@interface PSSignUpViewController ()
<UITextFieldDelegate,
PSDatePickerViewDelegate,
PSGenderPickerViewDelegate,
PSPhoneNumberFieldDelegate,
PSAlertViewDelegate>
{
    IBOutlet UIView *page1;
    IBOutlet UIView *page2;
    IBOutlet UIView *page3;
    IBOutlet UIView *page4;
    
    IBOutlet NSLayoutConstraint *page1Left;
    IBOutlet NSLayoutConstraint *page2Left;
    IBOutlet NSLayoutConstraint *page3Left;
    IBOutlet NSLayoutConstraint *page4Left;
    
    IBOutlet PSTextField *email;
    IBOutlet PSTextField *password;
    IBOutlet PSTextField *password2;
    IBOutlet PSTextField *firstName;
    IBOutlet PSTextField *lastName;
    IBOutlet UIButton *birthday;
    IBOutlet UIButton *gender;
    IBOutlet PSDatePickerView *datePicker;
    IBOutlet PSGenderPickerView *genderPicker;
    IBOutlet PSPhoneNumberField *phoneNumberField;
    IBOutlet PSTextField *codeTextfield;
    IBOutlet UIButton *resendButton;
    
    IBOutlet PSDiscreteProgressControl *progressControl;
    
    int stage;
    
    // Storing user's input to relay to the picker interfaces.
    int userBirthYear;
    Gender userGender;
    
    // Stores timer for resend button
    NSTimer *resendTimer;
    NSDate *startTime;
    
    NSString *validationRequest;
    
    // Storing validated number
    NSString *validingNumber;
    
    // PSAlertView
    PSAlertView *cancelAlert;
    PSAlertView *welcomeAlert;
}

@end

@implementation PSSignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Navigation view controller set up
    [self.navigationController.navigationBar setTitleTextAttributes:
     [NSDictionary dictionaryWithObjectsAndKeys:
      [UIFont fontWithName:@"Montserrat-Regular" size:22],
      NSFontAttributeName, nil]];
    [self.navigationItem setTitle:@"signing up"];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                 forBarPosition:UIBarPositionAny
                                                     barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    UIView *back = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(-10, 0, 40, 40)];
    [backButton setImage:[UIImage imageNamed:@"BackButton"] forState:UIControlStateNormal];
    [back addSubview:backButton];
    [backButton addTarget:self action:@selector(back:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *left = [[UIBarButtonItem alloc] initWithCustomView:back];
    [self.navigationItem setLeftBarButtonItem:left];
    
    [self moveToStage:1];
    [progressControl setProgress:stage animated:NO];
    
    UIColor *white = [UIColor colorWithWhite:1 alpha:0.5];
    email.layer.borderColor = white.CGColor;
    password.layer.borderColor = white.CGColor;
    password2.layer.borderColor = white.CGColor;
    firstName.layer.borderColor = white.CGColor;
    lastName.layer.borderColor = white.CGColor;
    birthday.layer.borderColor = white.CGColor;
    gender.layer.borderColor = white.CGColor;
    phoneNumberField.layer.borderColor = white.CGColor;
    codeTextfield.layer.borderColor = white.CGColor;
    
    // Set up date picker
    [datePicker setHidden:YES animated:NO];
    [datePicker setDelegate:self];
    userBirthYear = 0;
    
    // Set up gender picker
    [genderPicker setHidden:YES animated:NO];
    [genderPicker setDelegate:self];
    
    // Set up phone number field
    phoneNumberField.delegate = phoneNumberField;
    phoneNumberField.controlDelegate = self;
    
    // Set up toolbar for last field
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:[self view] action:@selector(endEditing:)],
                           nil];
    [numberToolbar sizeToFit];
    codeTextfield.inputAccessoryView = numberToolbar;
    
    // Set up alerts
    cancelAlert = [[PSAlertView alloc] initWithMessage:@"Are you sure you want to stop signing up? The provided information so far will be discarded." delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    welcomeAlert = [[PSAlertView alloc] initWithMessage:@"Registration Successful! Welcome to picswitch!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [datePicker setHidden:YES animated:YES];
    [genderPicker setHidden:YES animated:YES];
}

/**
 Goes to the next field.
 */
- (void)nextField:(id)sender {
    if (sender == email) {
        [password becomeFirstResponder];
    } else if (sender == password) {
        [password2 becomeFirstResponder];
    } else if (sender == firstName) {
        [lastName becomeFirstResponder];
    } else if (sender == lastName) {
        [[self view] endEditing:YES];
        [self chooseBirthday:sender];
    } else if (sender == datePicker) {
        [self chooseGender:sender];
    } else {
        [self nextStep:self];
    }
}

- (IBAction)chooseBirthday:(id)sender {
    [[self view] endEditing:YES];
    [datePicker setHidden:NO animated:YES];
    [genderPicker setHidden:YES animated:YES];
}

- (IBAction)chooseGender:(id)sender {
    [[self view] endEditing:YES];
    [genderPicker setHidden:NO animated:YES];
    [datePicker setHidden:YES animated:YES];
}

- (void)back:(id)sender {
    [cancelAlert show];
}

- (IBAction)nextStep:(id)sender {
    [[self view] endEditing:YES];
    
    switch (stage) {
        case 1:
            [self checkFirstStep];
            break;
        case 2:
            [self checkSecondStep];
            break;
        case 3:
            [self checkThirdStep];
            break;
        case 4:
            [self checkFourthStep];
            break;
        default:
            break;
    }
}

- (void)checkFirstStep {
    // Reset UI
    email.layer.borderWidth = 0;
    password.layer.borderWidth = 0;
    password2.layer.borderWidth = 0;
    
    // First, check empty fields and password matching
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:&error];
    
    BOOL isEmailValid = [regex numberOfMatchesInString:email.text options:0 range:NSMakeRange(0, email.text.length)];
    
    BOOL isPasswordValid = password.text.length >= 6;
    BOOL isPasswordSame = [password.text isEqualToString:password2.text];
    
    NSString *errMsg = @"Please correct the following:";
    
    if (!isEmailValid) {
        email.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - invalid email", errMsg];
    }
    
    if (!isPasswordValid) {
        password.layer.borderWidth = .5;
        password2.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - invalid password", errMsg];
    }
    
    if (!isPasswordSame) {
        password.layer.borderWidth = .5;
        password2.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - passwords don't match", errMsg];
    }
    
    if (isEmailValid && isPasswordValid && isPasswordSame) {
        // Load
        PSLoadingView *loading = [[PSLoadingView alloc] initWithTitle:@"Checking..."];
        [loading show:YES];
        
        // Next, we check with server whether email is taken
        PFQuery *query = [PFUser query];
        [query whereKey:@"email" equalTo:email.text];
        
        [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [loading dismiss:YES completion:^{
                if (object) {
                    // Already taken
                    NSString *errMsg = @"This email is already taken! Please try to login or recover your password!";
                    
                    PSAlertView *alert = [[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                } else {
                    // Success! Load next step
                    [self moveToStage:2];
                }
            }];
        }];
    } else {
        PSAlertView *alert = [[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)checkSecondStep {
    firstName.layer.borderWidth = 0;
    lastName.layer.borderWidth = 0;
    birthday.layer.borderWidth = 0;
    gender.layer.borderWidth = 0;
    
    NSString *errMsg = @"Please correct the following:";
    
    // Just checking name length
    BOOL isFirstNameValid = firstName.text.length > 0;
    BOOL isLastNameValid = lastName.text.length > 0;
    BOOL hasDate = userBirthYear != 0;
    BOOL hasGender = userGender;
    
    if (!isFirstNameValid) {
        firstName.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - type a first name", errMsg];
    }
    
    if (!isLastNameValid) {
        lastName.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - type a last name", errMsg];
    }
    
    if (!hasDate) {
        birthday.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - choose birthday", errMsg];
    }
    
    if (!hasGender) {
        gender.layer.borderWidth = .5;
        errMsg = [NSString stringWithFormat:@"%@\n - choose gender", errMsg];
    }
    
    if (isFirstNameValid && isLastNameValid && hasDate && hasGender) {
        [self moveToStage:3];
    } else {
        PSAlertView *alert = [[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)checkThirdStep {
    firstName.layer.borderWidth = 0;
    
    // Checks whether phone number is valid
    BOOL isValid = [phoneNumberField isValid];
    
    if (isValid) {
        [self moveToStage:4];
    } else {
        phoneNumberField.layer.borderWidth = .5;
        NSString *errMsg = @"Phone number is invalid!";
        PSAlertView *alert = [[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (void)checkFourthStep {
    codeTextfield.layer.borderWidth = 0;
    
    // Checks whether field is empty
    BOOL isEmpty = codeTextfield.text.length == 0;
    
    if (isEmpty) {
        phoneNumberField.layer.borderWidth = .5;
        NSString *errMsg = @"Please enter verification code!";
        PSAlertView *alert = [[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    } else {
        // Loading
        PSLoadingView *loading = [[PSLoadingView alloc] initWithTitle:@"Verifying..."];
        [loading show:YES];
        
        NSDictionary *params = @{@"request": validationRequest? validationRequest : @"",
                                 @"pin": codeTextfield.text};
        [PFCloud callFunctionInBackground:@"PVValidate" withParameters:params block:^(id result, NSError *error) {
            [loading dismiss:YES completion:^{
                if (!error) {
                    if ([result boolValue]) {
                        [self createAccount];
                    } else {
                        phoneNumberField.layer.borderWidth = .5;
                        NSString *errMsg = @"Incorrect verification code! Please double check or request a new code.";
                        [[[PSAlertView alloc] initWithMessage:errMsg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                    }
                } else {
                    NSLog(@"Request failed with message: %@", [error userInfo][@"error"]);
                }
            }];
        }];
    }
}

- (void)createAccount {
    PFUser *user = [PFUser user];
    user.username = email.text;
    user.password = password.text;
    user.email = email.text;
    user[@"firstName"] = firstName.text;
    user[@"lastName"] = lastName.text;
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    int i2  = [[formatter stringFromDate:[NSDate date]] intValue];
    user[@"age"] = [NSNumber numberWithInt:i2 - datePicker.year];
    user[@"gender"] = genderPicker.gender;
    user[@"phoneNumber"] = [phoneNumberField phoneNumber];
    
    [[NSUserDefaults standardUserDefaults] setObject:email.text forKey:@"LoginUsername"];
    
    // Put up loading screen
    PSLoadingView *loading = [[PSLoadingView alloc] initWithTitle:@"Creating..."];
    [loading show:YES];
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [loading dismiss:YES completion:^{
            if (!error) {
                // Hooray! Let them use the app now.
                
                // First perform login stuff
                [[NSUserDefaults standardUserDefaults] setObject:email.text forKey:@"LoginUsername"];
                // Pin user
                [[PFUser currentUser] pinInBackground];
                
                // Reset local data
                [[NSUserDefaults standardUserDefaults] setObject:@{} forKey:@"SkippedPuzzles"];
                [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"userFriends"];
                [[NSUserDefaults standardUserDefaults] setObject:@[] forKey:@"incomingRequests"];
                
                // Save installation to server
                PFInstallation *installation = [PFInstallation currentInstallation];
                installation[@"username"] = [user username];
                [installation saveEventually];
                
                PSScrollViewController *viewController = [[PSScrollViewController alloc] init];
                [self.navigationController pushViewController:viewController animated:YES];
                [welcomeAlert show];
                
                // Send first message to
                [PFCloud callFunctionInBackground:@"tutorialMessage" withParameters:nil];
            } else {
                NSString *errorString = [error userInfo][@"error"];
                NSLog(@"%@", errorString);
                // Show the errorString somewhere and let the user try again.
                [[[PSAlertView alloc] initWithMessage:@"Something went wrong! Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }];
    }];
}

- (void)moveToStage:(int)s {
    stage = s;
    [progressControl setProgress:stage animated:YES];
    
    switch (s) {
        case 1:
            break;
        case 2: {
            [page2 setHidden:NO];
            float width = self.view.bounds.size.width;
            page2Left.constant = width;
            [[self view] layoutIfNeeded];
            
            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                page1Left.constant = -width / 3;
                page2Left.constant = 0;
                [[self view] layoutIfNeeded];
            } completion:nil];
            
            break;
        } case 3: {
            [page3 setHidden:NO];
            float width = self.view.bounds.size.width;
            page3Left.constant = width;
            [[self view] layoutIfNeeded];
            
            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                page2Left.constant = -width / 3;
                page3Left.constant = 0;
                [[self view] layoutIfNeeded];
            } completion:nil];
            break;
        } case 4: {
            [page4 setHidden:NO];
            float width = self.view.bounds.size.width;
            page4Left.constant = width;
            [[self view] layoutIfNeeded];
            
            [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                page3Left.constant = -width / 3;
                page4Left.constant = 0;
                [[self view] layoutIfNeeded];
            } completion:nil];
            
            // Start the timer for resend enabling
            [self requestValidation];
            resendTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateResendButtonTitle:) userInfo:nil repeats:YES];
            [self updateResendButtonTitle:self];
            startTime = [NSDate date];
            
            break;
        } default:
            break;
    }
}

- (void)updateResendButtonTitle:(id)sender {
    if (-[startTime timeIntervalSinceNow] > RESEND_WAIT_INTERVAL) {
        // Invalidate timer and enable button
        [resendTimer invalidate];
        resendButton.enabled = YES;
        UIColor *green = [UIColor colorWithHue:0.5 saturation:0.66 brightness:0.53 alpha:1.0];
        resendButton.backgroundColor = green;
    } else {
        // Update duration
        int min = (RESEND_WAIT_INTERVAL + [startTime timeIntervalSinceNow]) / 60;
        int sec = (int)(RESEND_WAIT_INTERVAL + [startTime timeIntervalSinceNow]) % 60;
        NSString *title = [NSString stringWithFormat:@"%02d:%02d", min, sec];
        
        // Bypassing weird bug of skipping a second
        if (min == 5) {
            title = @"04:59";
        }
        
        [resendButton setTitle:title forState:UIControlStateDisabled];
        resendButton.enabled = NO;
        resendButton.backgroundColor = [UIColor grayColor];
    }
}

#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self nextField:textField];
    
    return YES;
}

// iOS 7 workaround
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        UIFont *fontMain = [UIFont fontWithName:@"Montserrat-Bold" size:14];
        UIFont *fontPh = [UIFont fontWithName:@"Quicksand-Italic" size:14];
        
        if ([[[textField text] stringByReplacingCharactersInRange:range withString:string] isEqualToString:@""]) {
            [textField setFont:fontPh];
        } else {
            [textField setFont:fontMain];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [datePicker setHidden:YES animated:YES];
    [genderPicker setHidden:YES animated:YES];
    
    return YES;
}

#pragma mark - PSDatePickerView Delegate
- (void)nextPressed:(PSDatePickerView *)pickerView {
    [self updateBirthday];
    [pickerView setHidden:YES animated:YES];
    [self nextField:pickerView];
}

- (void)donePressed:(PSDatePickerView *)pickerView {
    [self updateBirthday];
    [pickerView setHidden:YES animated:YES];
}

- (void)donePressedGender:(PSGenderPickerView *)picker {
    [self updateGender];
    [picker setHidden:YES animated:YES];
}

- (void)updateBirthday {
    userBirthYear = [datePicker year];
    NSString *dateString = [NSString stringWithFormat:@"%i", userBirthYear];
    [birthday setTitle:dateString forState:UIControlStateNormal];
    
    // In sync with animation
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [birthday setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [[birthday titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Regular" size:14]];
    });
}

- (void)updateGender {
    [gender setTitle:[genderPicker gender] forState:UIControlStateNormal];
    userGender = [genderPicker genderIndex];
    
    // In sync with animation
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [gender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [[gender titleLabel] setFont:[UIFont fontWithName:@"Montserrat-Regular" size:14]];
    });
}

// Request another text message to be sent to user
- (IBAction)resendValidationRequest:(id)sender {
    [self requestValidation];
    resendTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateResendButtonTitle:) userInfo:nil repeats:YES];
    startTime = [NSDate date];
    [self updateResendButtonTitle:self];
}

// Contacts the server to generate a text message for user
- (void)requestValidation {
    [[ServiceManager sharedManager] requestValidation:[phoneNumberField phoneNumber] callback:^(id obj, NSError *err) {
        if (!err) {
            validationRequest = obj;
            NSLog(@"%@", validationRequest);
        } else {
            NSLog(@"Error with message: %@", [err userInfo][@"error"]);
        }
    }];
}

- (void)showTutorial {
    PSTutorialViewController *tute = [[PSTutorialViewController alloc] initWithNibName:nil bundle:nil];
    NSArray *tuteImages;
    
    if ([UIScreen mainScreen].bounds.size.height != 480) {
        tuteImages = @[[UIImage imageNamed:@"Tutorial1"], [UIImage imageNamed:@"Tutorial2"], [UIImage imageNamed:@"Tutorial3"], [UIImage imageNamed:@"Tutorial4"], [UIImage imageNamed:@"Tutorial5"]];
    } else {
        tuteImages = @[[UIImage imageNamed:@"Tutorial1@3.5"], [UIImage imageNamed:@"Tutorial2@3.5"], [UIImage imageNamed:@"Tutorial3@3.5"], [UIImage imageNamed:@"Tutorial4@3.5"], [UIImage imageNamed:@"Tutorial5@3.5"]];
    }
    
    [tute setupImages:tuteImages];
    
    [self presentViewController:tute animated:YES completion:nil];
}


#pragma mark - PSPhoneNumberFieldControlDelegate
- (void)donePressedPhoneNumberField:(PSPhoneNumberField *)field {
    [field endEditing:YES];
}

- (void)nextPressedPhoneNumberField:(PSPhoneNumberField *)field {
    [self nextField:field];
}

#pragma mark - PSAlertViewDelegate
- (void)alertView:(PSAlertView *)alertView clickedButtonAtIndex:(NSInteger)index {
    if ([alertView isEqual:cancelAlert] && index == 1)
        [[self navigationController] popViewControllerAnimated:YES];
    else if ([alertView isEqual:welcomeAlert]) {
        [self showTutorial];
    }
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
