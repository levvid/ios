//
//  PSAddFriendViewController.h
//  PicSwitch
//
//  Created by Jiacong Xu on 3/7/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSAddFriendViewController : UIViewController
<UITextFieldDelegate>
{
    IBOutlet UITextField *friendIdField;
}

- (IBAction)addFriend:(id)sender;

@end
