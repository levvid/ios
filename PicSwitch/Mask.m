//
//  Mask.m
//  Big Red Ambition: 161 things to do at Cornell
//
//  Created by Jiacong Xu on 9/18/14.
//  Copyright (c) 2014 Jiacong Xu. All rights reserved.
//

#import "Mask.h"

@implementation Mask

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self showText:false];
        [self adjustRatioStart:3.2 end:1.2];
        opacity = 0.75;
        forPic = false;
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self showText:false];
        [self adjustRatioStart:2.2 end:1];
        opacity = 0.36;
    }
    
    return self;
}

- (void)adjustRatioStart:(CGFloat)s end:(CGFloat)e {
    start = s;
    end = e;
    [self setNeedsDisplay];
}

- (void)setOpacity:(float)op {
    opacity = op;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    CGFloat startRad = self.bounds.size.height / start;
    CGFloat endRad = self.bounds.size.height / end;
    
    CGPoint c = CGPointMake(self.center.x, 0);
    CGFloat components[8];
    
    if (forPic) {
        c = CGPointMake(self.center.x, startRad / 1.5);
        components[0] = 0.2;
        components[1] = 0.2;
        components[2] = 0.2;
        components[3] = 0.8;
        components[4] = 0.05;
        components[5] = 0.05;
        components[6] = 0.05;
        components[7] = 0.8;
    } else {
        components[0] = 0.0;
        components[1] = 0.0;
        components[2] = 0.0;
        components[3] = 0.0;
        components[4] = 0.0;
        components[5] = 0.0;
        components[6] = 0.0;
        components[7] = opacity;
    }
    
    CGFloat locations[2] = {0.0, 1.0};
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef gradient = CGGradientCreateWithColorComponents(space, components, locations, 2);
    
    CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), gradient, c, startRad, c, endRad, kCGGradientDrawsAfterEndLocation | kCGGradientDrawsBeforeStartLocation);
    
    CGColorSpaceRelease(space);
    CGGradientRelease(gradient);
}

- (void)setForPicture {
    forPic = true;
}

- (void)showText:(BOOL)shouldShow
{
    self.titleLabel.alpha = shouldShow;
}

@end
