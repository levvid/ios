//
//  PSTutorialViewController.m
//  PicSwitch
//
//  Created by Jiacong Xu on 7/21/15.
//  Copyright (c) 2015 Myriad Games. All rights reserved.
//

#import "PSTutorialViewController.h"

@interface PSTutorialViewController ()
<UIScrollViewDelegate>
{
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIScrollView *scrollView;
    
    NSArray *images;
}
@end

@implementation PSTutorialViewController

// For now, we want to display a set number of pictures.

- (void)setupImages:(NSArray *)imageArray {
    images = @[];
    
    for (int i = 0; i < imageArray.count; i++) {
        UIImage *img = imageArray[i];
        if ([img isKindOfClass:[UIImage class]])
            images = [images arrayByAddingObject:img];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setupScrollView];
}

- (void)setupScrollView {
    // Set up paging
    pageControl.numberOfPages = images.count;
    
    // Set up the size of the scroll view
    CGFloat width = scrollView.frame.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    scrollView.contentSize = CGSizeMake(width * images.count, height);
    
    for (int i = 0; i < images.count; i++) {
        UIImage *image = images[i];
        UIImageView *imgView = [[UIImageView alloc] initWithImage:image];
        imgView.frame = CGRectMake(i * width, 0, width, height);
        [scrollView addSubview:imgView];
    }
}

- (IBAction)contentTapped:(id)sender {
    // Exit the tutorial if last page
    if (pageControl.currentPage == images.count - 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        CGRect frame = scrollView.frame;
        frame.origin.x = frame.size.width * (pageControl.currentPage + 1);
        frame.origin.y = 0;
        [scrollView scrollRectToVisible:frame animated:YES];
    }
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)sv {
    int pg = floor((sv.contentOffset.x - sv.frame.size.width / 2) / sv.frame.size.width) + 1;
    [pageControl setCurrentPage:pg];
}

#pragma mark - Miscellaneous Methods
/**
 Makes status bar white instead of black.
 */
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
